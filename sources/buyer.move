module marketplace::buyer {
    use std::error;
    use std::signer;
    use std::vector;
    use std::string::String;
    use aptos_framework::timestamp;
    use aptos_std::table::{Self, Table};
    use aptos_token::token::{Self, TokenId};

    // Store offering records on the maker's account. The maker is everyone, who offer the price of a NFT
    struct OfferRecords has key {
        all_records: Table<String, Offer>,
        all_offer_ids: vector<String>,
        offered_token_ids: vector<TokenId>,
        offer_ids_by_token_id: Table<TokenId, vector<String>>
    }

    struct Offer has drop, store {
        offer_id: String,
        token_id: TokenId,
        buyer: address,
        bought_amount: u64,
        quantity: u64,
        price: u64,
        start_time: u64,
        end_time: u64,
        is_active: bool,
        is_claim: bool,
        accepted_by_lists: vector<AcceptedByListing>
    }

    struct AcceptedByListing has drop, store {
        listing_id: u64,
        seller: address,
        quantity: u64,
        price: u64,
        timestamp: u64
    }

    /// Start time should be less than end time
    const ESTART_TIME_LARGER_THAN_END_TIME: u64 = 0;

    /// Token ID is not published
    const EOFFERING_TOKEN_ID_NOT_PUBLISH: u64 = 1;

    /// Offer quantity must be non-zero
    const EOFFERING_QUANTITY_ZERO: u64 = 2;

    /// Offering price cannot equal zero
    const EOFFERING_PRICE_ZERO: u64 = 3;

    /// The offer id is existed
    const EOFFERING_ID_IS_EXISTED: u64 = 4;

    /// The offer id is not found
    const EOFFER_ID_NOT_FOUND: u64 = 5;

    /// The offer is already archived
    const EOFFER_IS_ARCHIVED: u64 = 6;

    /// The offer is expired
    const EOFFER_IS_EXPIRED: u64 = 7;

    /// The token id is not existed
    const ETOKEN_ID_IS_NOT_EXISTED: u64 = 8;

    /// The token id is existed
    const ETOKEN_ID_IS_EXISTED: u64 = 9;

    /// The amount of offer is not enough
    const EOFFER_QUANTITY_NOT_ENOUGH: u64 = 10;

    /// The offer is already claimed
    const EOFFER_ALREADY_CLAIMED: u64 = 11;

    public fun initialize_offer_records(account: &signer) {
        if(!exists<OfferRecords>(signer::address_of(account))) {
            move_to(
                account,
                OfferRecords {
                    all_records: table::new(),
                    all_offer_ids: vector::empty<String>(),
                    offered_token_ids: vector::empty<TokenId>(),
                    offer_ids_by_token_id: table::new()
                }
            );
        }
    }

    public fun create_offer(
        buyer: &signer,
        offer_id: String,
        token_id: TokenId,
        quantity: u64,
        price: u64,
        start_time: u64,
        end_time: u64
    ): Offer {
        let buyer_addr = signer::address_of(buyer);
        let (creator, collection, name, property_version) = token::get_token_id_fields(&token_id);
        let token_data_id = token::create_token_data_id(creator, collection, name);
        assert!(start_time < end_time, error::invalid_argument(ESTART_TIME_LARGER_THAN_END_TIME));
        assert!(quantity > 0, error::invalid_argument(EOFFERING_QUANTITY_ZERO));
        assert!(price > 0, error::invalid_argument(EOFFERING_PRICE_ZERO));
        if (token::check_tokendata_exists(creator, collection, name)) {
            assert!(
                property_version <= token::get_tokendata_largest_property_version(creator, token_data_id),
                error::invalid_argument(EOFFERING_TOKEN_ID_NOT_PUBLISH)
            );
        };
        Offer {
            offer_id,
            token_id,
            buyer: buyer_addr,
            bought_amount: 0,
            quantity,
            price,
            start_time,
            end_time,
            is_active: true,
            is_claim: false,
            accepted_by_lists: vector::empty<AcceptedByListing>()
        }
    }

    public fun create_offer_under_user_account(
        buyer: &signer,
        offer_id: String,
        token_id: TokenId,
        quantity: u64,
        price: u64,
        start_time: u64,
        end_time: u64
    ) acquires OfferRecords {
        let buyer_addr = signer::address_of(buyer);
        let offer = create_offer(
            buyer,
            offer_id,
            token_id,
            quantity,
            price,
            start_time,
            end_time
        );
        initialize_offer_records(buyer);
        let offer_records = borrow_global_mut<OfferRecords>(buyer_addr);
        assert!(!table::contains(&offer_records.all_records, offer_id), error::invalid_argument(EOFFERING_ID_IS_EXISTED));
        table::add(&mut offer_records.all_records, offer_id, offer);
        vector::push_back(&mut offer_records.all_offer_ids, offer_id);
        if (vector::contains(&offer_records.offered_token_ids, &token_id)) {
            assert!(table::contains(&offer_records.offer_ids_by_token_id, token_id), error::invalid_argument(ETOKEN_ID_IS_NOT_EXISTED));
            assert!(!vector::contains(table::borrow(&offer_records.offer_ids_by_token_id, token_id), &offer_id), error::invalid_argument(EOFFERING_ID_IS_EXISTED));
            let offer_ids = table::borrow_mut(&mut offer_records.offer_ids_by_token_id, token_id);
            vector::push_back(offer_ids, offer_id);
        } else {
            assert!(!table::contains(&offer_records.offer_ids_by_token_id, token_id), error::invalid_argument(ETOKEN_ID_IS_EXISTED));
            vector::push_back(&mut offer_records.offered_token_ids, token_id);
            table::add(&mut offer_records.offer_ids_by_token_id, token_id, vector::singleton(offer_id));
        };
    }

    public fun cancel_offer(buyer_addr: address, offer_id: String): (TokenId, u64) acquires OfferRecords {
        let offer_records = borrow_global_mut<OfferRecords>(buyer_addr);
        assert!(table::contains(&offer_records.all_records, offer_id), error::invalid_argument(EOFFER_ID_NOT_FOUND));
        remove_offer_from_token_infos(offer_records, offer_id);
        let offer = table::borrow_mut(&mut offer_records.all_records, offer_id);
        assert!(offer.is_active, error::unavailable(EOFFER_IS_ARCHIVED));
        let now = timestamp::now_seconds();
        assert!(offer.start_time < now && now < offer.end_time, error::unavailable(EOFFER_IS_EXPIRED));
        offer.is_active = false;
        (offer.token_id, offer.price * (offer.quantity - offer.bought_amount))
    }

    public fun cancel_all_offer(buyer_addr: address): (u64, vector<TokenId>, vector<String>) acquires OfferRecords {
        let offer_records = borrow_global_mut<OfferRecords>(buyer_addr);
        let now = timestamp::now_seconds();
        let total_remain_coin = 0;
        let token_ids = vector::empty<TokenId>();
        let canceled_offer_ids = vector::empty<String>();
        let idx = 0;
        while (idx < vector::length(&offer_records.all_offer_ids)) {
            let offer = table::borrow_mut(
                &mut offer_records.all_records,
                *vector::borrow(&offer_records.all_offer_ids, idx)
            );
            if (offer.is_active && offer.start_time < now && now < offer.end_time) {
                offer.is_active = false;
                total_remain_coin = total_remain_coin + offer.price * (offer.quantity - offer.bought_amount);
                vector::push_back(&mut token_ids, offer.token_id);
                vector::push_back(&mut canceled_offer_ids, offer.offer_id);
                remove_offer_from_token_infos(offer_records, offer.offer_id);
            };
            idx = idx + 1;
        };
        (total_remain_coin, token_ids, canceled_offer_ids)
    }

    public fun claim_offer(buyer_addr: address, offer_id: String): (TokenId, u64) acquires OfferRecords {
        let offer_records = borrow_global_mut<OfferRecords>(buyer_addr);
        assert!(table::contains(&offer_records.all_records, offer_id), error::invalid_argument(EOFFER_ID_NOT_FOUND));
        remove_offer_from_token_infos(offer_records, offer_id);
        let offer = table::borrow_mut(&mut offer_records.all_records, offer_id);
        assert!(offer.is_active, error::unavailable(EOFFER_IS_ARCHIVED));
        assert!(!offer.is_claim, error::unavailable(EOFFER_ALREADY_CLAIMED));
        let now = timestamp::now_seconds();
        assert!(now > offer.end_time, error::unavailable(EOFFER_IS_EXPIRED));
        offer.is_claim = true;
        (offer.token_id, offer.price * (offer.quantity - offer.bought_amount))
    }

    public fun claim_all_offer(buyer_addr: address): (u64, vector<TokenId>, vector<String>) acquires OfferRecords {
        let offer_records = borrow_global_mut<OfferRecords>(buyer_addr);
        let now = timestamp::now_seconds();
        let total_remain_coin = 0;
        let token_ids = vector::empty<TokenId>();
        let claimed_offer_ids = vector::empty<String>();
        let idx = 0;
        while (idx < vector::length(&offer_records.all_offer_ids)) {
            let offer = table::borrow_mut(
                &mut offer_records.all_records,
                *vector::borrow(&offer_records.all_offer_ids, idx)
            );
            if (offer.is_active && !offer.is_claim && now > offer.end_time) {
                offer.is_claim = true;
                total_remain_coin = total_remain_coin + offer.price * (offer.quantity - offer.bought_amount);
                vector::push_back(&mut token_ids, offer.token_id);
                vector::push_back(&mut claimed_offer_ids, offer.offer_id);
                remove_offer_from_token_infos(offer_records, offer.offer_id);
            };
            idx = idx + 1;
        };
        (total_remain_coin, token_ids, claimed_offer_ids)
    }

    public fun match_offer_id_with_listing_ids(
        buyer_addr: address,
        offer_id: String,
        amount: u64
    ) acquires OfferRecords {
        assert!(amount > 0, error::invalid_argument(EOFFERING_QUANTITY_ZERO));
        let offer_records = borrow_global_mut<OfferRecords>(buyer_addr);
        let now = timestamp::now_seconds();
        assert!(vector::contains(&offer_records.all_offer_ids, &offer_id), error::invalid_argument(EOFFER_ID_NOT_FOUND));
        assert!(table::contains(&offer_records.all_records, offer_id), error::invalid_argument(EOFFER_ID_NOT_FOUND));
        let offer = table::borrow_mut(&mut offer_records.all_records, offer_id);
        assert!(offer.start_time < now && now < offer.end_time, error::unavailable(EOFFER_IS_EXPIRED));
        assert!(offer.is_active, error::unavailable(EOFFER_IS_ARCHIVED));
        assert!(amount <= offer.quantity - offer.bought_amount, error::invalid_argument(EOFFER_QUANTITY_NOT_ENOUGH));
        offer.bought_amount = offer.bought_amount + amount;
        if (offer.bought_amount == offer.quantity) {
            remove_offer_from_token_infos(offer_records, offer.offer_id);
        };
    }

    public fun get_offer_id_fields(buyer_addr: address, offer_id: String): (TokenId, u64, u64) acquires OfferRecords {
        let offer_records = borrow_global<OfferRecords>(buyer_addr);
        assert!(vector::contains(&offer_records.all_offer_ids, &offer_id), error::invalid_argument(EOFFER_ID_NOT_FOUND));
        assert!(table::contains(&offer_records.all_records, offer_id), error::invalid_argument(EOFFER_ID_NOT_FOUND));
        let offer = table::borrow(&offer_records.all_records, offer_id);
        let amount_available = offer.quantity - offer.bought_amount;
        (offer.token_id, amount_available, offer.price)
    }

    fun remove_offer_from_token_infos(
        offer_records: &mut OfferRecords,
        offer_id: String
    ) {
        let token_id = table::borrow(&offer_records.all_records, offer_id).token_id;
        let (is_found, token_idx) = vector::index_of(&offer_records.offered_token_ids, &token_id);
        assert!(is_found, error::invalid_argument(ETOKEN_ID_IS_NOT_EXISTED));
        let (is_found, offer_idx) = vector::index_of(table::borrow(&offer_records.offer_ids_by_token_id, token_id), &offer_id);
        let offer_ids = table::borrow_mut(&mut offer_records.offer_ids_by_token_id, token_id);
        assert!(is_found, error::invalid_argument(EOFFER_ID_NOT_FOUND));
        vector::remove(offer_ids, offer_idx);
        if (vector::length(table::borrow(&offer_records.offer_ids_by_token_id, token_id)) == 0) {
            table::remove(&mut offer_records.offer_ids_by_token_id, token_id);
            vector::remove(&mut offer_records.offered_token_ids, token_idx);
        };
    }
}
