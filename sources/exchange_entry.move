module marketplace::exchange_entry {
    use std::error;
    use std::vector;
    use std::signer;
    use std::string::String;
    use std::option::{Self, Option};
    use aptos_std::math64;
    use aptos_std::type_info::{Self, TypeInfo};
    use aptos_std::ed25519::{Self, ValidatedPublicKey};
    use aptos_token::token::{Self, TokenId};
    use aptos_framework::coin::{Self, Coin};
    use aptos_framework::timestamp;
    use aptos_framework::resource_account;
    use aptos_framework::event::{Self, EventHandle};
    use aptos_framework::aptos_coin::AptosCoin;
    use aptos_framework::account::{Self, SignerCapability};
    use marketplace::seller as seller_utils;
    use marketplace::buyer as buyer_utils;
    use marketplace::nft as nft_utils;
    use marketplace::utils::{
        Self as utils,
        CreateCollectionEvent,
        CreateTokenEvent,
        ListingEvent,
        EditPriceListingEvent,
        CancelListingEvent,
        CancelAllListingEvent,
        OfferEvent,
        CancelOfferEvent,
        CancelAllOfferEvent,
        TransferEvent,
        BuyNowEvent,
        AcceptOfferEvent,
        ClaimListingEvent,
        ClaimAllListingEvent,
        ClaimOfferEvent,
        ClaimAllOfferEvent,
        CreateCollectionProof,
        CreateTokenWithCreatedCollectionProof,
        CreateTokenWithoutCreatedCollectionProof,
        TransferWithCreatedTokenProof,
        TransferWithoutCreatedCollectionProof,
        TransferWithCreatedCollectionProof,
        ListingTokenFixedPriceProof,
        ListingTokenForBidProof,
        EditPriceListingProof,
        CancelListingProof,
        CancelAllListingProof,
        BuyNowTokenProof,
        OfferTokenProof,
        CancelOfferProof,
        CancelAllOfferProof,
        AcceptOffersWithCreatedTokenAndListingProof,
        AcceptOffersWithoutCreatedCollectionAndListingProof,
        AcceptOffersWithoutCreatedTokenAndListingProof,
        AcceptOffersWithCreatedTokenAndWithoutListingProof,
        ClaimListingProof,
        ClaimAllListingProof,
        ClaimOfferingProof,
        ClaimAllOfferingProof
    };

    struct Config has key {
        fee_numerator: u64,
        fee_denominator: u64,
        fee_address: address,
        signer_cap: SignerCapability,
        payment_coin: TypeInfo,
        verifier_pk: Option<ValidatedPublicKey>,
        paused: bool,
        operators: vector<address>
    }

    struct Exchange has key {
        create_collection_event: EventHandle<CreateCollectionEvent>,
        create_token_event: EventHandle<CreateTokenEvent>,
        listing_event: EventHandle<ListingEvent>,
        edit_price_listing_event: EventHandle<EditPriceListingEvent>,
        cancel_listing_event: EventHandle<CancelListingEvent>,
        cancel_all_listing_event: EventHandle<CancelAllListingEvent>,
        offer_event: EventHandle<OfferEvent>,
        cancel_offer_event: EventHandle<CancelOfferEvent>,
        cancel_all_offer_event: EventHandle<CancelAllOfferEvent>,
        transfer_event: EventHandle<TransferEvent>,
        buy_now_event: EventHandle<BuyNowEvent>,
        accept_offer_event: EventHandle<AcceptOfferEvent>,
        claim_listing_event: EventHandle<ClaimListingEvent>,
        claim_all_listing_event: EventHandle<ClaimAllListingEvent>,
        claim_offer_event: EventHandle<ClaimOfferEvent>,
        claim_all_offer_event: EventHandle<ClaimAllOfferEvent>
    }

    /// Action not authorized because the signer is not the admin of this module
    const ENOT_AUTHORIZED: u64 = 1;

    /// Fee invalid if the numerator is larger than the denominator
    const EINVALID_FEE_NUMERATOR_DENOMINATOR: u64 = 2;

    /// Contract is paused
    const ECONTRACT_PAUSING: u64 = 3;

    /// CoinType arguments is invalid if the CoinType is not AptosCoin
    const EINVALID_COIN_TYPE_ARGUMENT: u64 = 4;

    /// The lazy minting is expired
    const ELAZY_MINTING_EXPIRED: u64 = 5;

    /// The element of vector is existed
    const EELEMENT_IS_EXISTED: u64 = 6;

    /// The element of vector is existed
    const EELEMENT_IS_NOT_EXISTED: u64 = 7;

    /// The argument or signature is wrong
    const EINVALID_SIGNATURE: u64 = 8;

    /// The module is paused
    const EIS_PAUSED: u64 = 9;

    /// The arguments for listing token is invalid
    const EINVALID_LISTING_ARGUMENTS: u64 = 10;

    /// The token id between listing and offering is not matching
    const ETOKEN_ID_NOT_MATCH: u64 = 11;

    /// Accept offers: Invalid arguments
    const EACCEPT_OFFER_INVALID_ARGUMENTS: u64 = 12;

    /// The timestamp of listing is invalid
    const ELISTING_TIME_IS_INVALID: u64 = 13;

    /// The quantities of listing records is invalid
    const ELISTING_QUANTITIES_INVALID: u64 = 14;

    fun init_module(resource_account: &signer) {
        initialize_exchange(resource_account);
    }

    public entry fun init_exchange_config(
        caller: &signer,
        fee_numerator: u64,
        fee_denominator: u64,
        fee_address: address,
        verifier_pk: vector<u8>
    ) acquires Config {
        let config = borrow_global_mut<Config>(@marketplace);
        let caller_address = signer::address_of(caller);
        assert!(vector::contains(&config.operators, &caller_address), error::permission_denied(ENOT_AUTHORIZED));
        assert!(fee_numerator > 0, error::invalid_argument(EINVALID_FEE_NUMERATOR_DENOMINATOR));
        assert!(fee_numerator <= fee_denominator, error::invalid_argument(EINVALID_FEE_NUMERATOR_DENOMINATOR));

        config.fee_numerator = fee_numerator;
        config.fee_denominator = fee_denominator;
        config.fee_address = fee_address;
        config.verifier_pk = option::some(option::extract(&mut ed25519::new_validated_public_key_from_bytes(verifier_pk)));
    }

    public entry fun add_operator(caller: &signer, operator: address) acquires Config {
        let config = borrow_global_mut<Config>(@marketplace);
        let caller_address = signer::address_of(caller);
        assert!(caller_address == @admin_addr, error::permission_denied(ENOT_AUTHORIZED));
        assert!(!vector::contains(&config.operators, &operator), error::already_exists(EELEMENT_IS_EXISTED));
        vector::push_back(&mut config.operators, operator);
    }

    public entry fun remove_operator(caller: &signer, operator: address) acquires Config {
        let config = borrow_global_mut<Config>(@marketplace);
        let caller_address = signer::address_of(caller);
        assert!(caller_address == @admin_addr, error::permission_denied(ENOT_AUTHORIZED));
        let (found, index) = vector::index_of(&config.operators, &operator);
        assert!(found, error::invalid_argument(EELEMENT_IS_NOT_EXISTED));
        vector::remove(&mut config.operators, index);
    }

    public entry fun set_fee(caller: &signer, fee_numerator: u64, fee_denominator: u64, fee_address: address) acquires Config {
        let config = borrow_global_mut<Config>(@marketplace);
        let caller_address = signer::address_of(caller);
        assert!(vector::contains(&config.operators, &caller_address), error::permission_denied(ENOT_AUTHORIZED));
        assert!(fee_numerator > 0, error::invalid_argument(EINVALID_FEE_NUMERATOR_DENOMINATOR));
        assert!(fee_numerator <= fee_denominator, error::invalid_argument(EINVALID_FEE_NUMERATOR_DENOMINATOR));

        config.fee_numerator = fee_numerator;
        config.fee_numerator = fee_numerator;
        config.fee_denominator = fee_denominator;
        config.fee_address = fee_address;
    }

    public entry fun set_payment_coin<CoinType>(caller: &signer) acquires Config {
        let config = borrow_global_mut<Config>(@marketplace);
        let caller_address = signer::address_of(caller);
        assert!(vector::contains(&config.operators, &caller_address), error::permission_denied(ENOT_AUTHORIZED));
        config.payment_coin = type_info::type_of<CoinType>();

        let resource_signer = account::create_signer_with_capability(&config.signer_cap);
        if (!coin::is_account_registered<CoinType>(@marketplace)) {
            coin::register<CoinType>(&resource_signer);
        };
    }

    public entry fun set_verifier_pk(caller: &signer, verifier_pk: vector<u8>) acquires Config {
        let config = borrow_global_mut<Config>(@marketplace);
        let caller_address = signer::address_of(caller);
        assert!(vector::contains(&config.operators, &caller_address), error::permission_denied(ENOT_AUTHORIZED));
        config.verifier_pk = option::some(option::extract(&mut ed25519::new_validated_public_key_from_bytes(verifier_pk)));
    }

    public entry fun set_paused(caller: &signer, is_paused: bool) acquires Config {
        let config = borrow_global_mut<Config>(@marketplace);
        let caller_address = signer::address_of(caller);
        assert!(vector::contains(&config.operators, &caller_address), error::permission_denied(ENOT_AUTHORIZED));
        config.paused = is_paused;
    }

    public entry fun create_collection(
        caller: &signer,
        name: String,
        description: String,
        uri: String,
        maximum: u64,
        mutate_setting: vector<bool>,
        signature: vector<u8>
    ) acquires Config, Exchange {
        checking_when_not_paused();
        let caller_address = signer::address_of(caller);
        let data = utils::create_new_collection_proof(
            caller_address,
            name,
            description,
            uri,
            maximum,
            mutate_setting
        );
        verify_signature<CreateCollectionProof>(signature, data);
        create_collection_raw(caller, name, description, uri, maximum, mutate_setting);
    }

    public entry fun create_token_with_created_collection(
        caller: &signer,
        collection_name: String,
        name: String,
        description: String,
        uri: String,
        quantity: u64,
        maximum: u64,
        royalty_payee_address: address,
        royalty_points_numerator: u64,
        royalty_points_denominator: u64,
        mutate_setting: vector<bool>,
        property_keys: vector<String>,
        property_values: vector<vector<u8>>,
        property_types: vector<String>,
        signature: vector<u8>
    ) acquires Config, Exchange {
        checking_when_not_paused();
        let caller_address = signer::address_of(caller);
        let data = utils::create_new_token_with_created_collection_proof(
            caller_address,
            collection_name,
            name,
            description,
            uri,
            quantity,
            maximum,
            royalty_payee_address,
            royalty_points_numerator,
            royalty_points_denominator,
            mutate_setting,
            property_keys,
            property_values,
            property_types
        );
        verify_signature<CreateTokenWithCreatedCollectionProof>(signature, data);
        create_token_raw(
            caller,
            collection_name,
            name,
            description,
            uri,
            quantity,
            maximum,
            royalty_payee_address,
            royalty_points_numerator,
            royalty_points_denominator,
            mutate_setting,
            property_keys,
            property_values,
            property_types
        );
    }

    public entry fun create_token_without_created_collection(
        caller: &signer,
        collection_name: String,
        collection_description: String,
        collection_uri: String,
        collection_maximum: u64,
        collection_mutate_setting: vector<bool>,
        token_name: String,
        token_description: String,
        token_uri: String,
        token_quantity: u64,
        token_maximum: u64,
        royalty_payee_address: address,
        royalty_points_numerator: u64,
        royalty_points_denominator: u64,
        token_mutate_setting: vector<bool>,
        property_keys: vector<String>,
        property_values: vector<vector<u8>>,
        property_types: vector<String>,
        signature: vector<u8>
    ) acquires Config, Exchange {
        checking_when_not_paused();
        let caller_address = signer::address_of(caller);
        let data = utils::create_new_token_without_created_collection_proof(
            caller_address,
            collection_name,
            collection_description,
            collection_uri,
            collection_maximum,
            collection_mutate_setting,
            token_name,
            token_description,
            token_uri,
            token_quantity,
            token_maximum,
            royalty_payee_address,
            royalty_points_numerator,
            royalty_points_denominator,
            token_mutate_setting,
            property_keys,
            property_values,
            property_types
        );
        verify_signature<CreateTokenWithoutCreatedCollectionProof>(signature, data);
        create_collection_raw(
            caller,
            collection_name,
            collection_description,
            collection_uri,
            collection_maximum,
            collection_mutate_setting
        );
        create_token_raw(
            caller,
            collection_name,
            token_name,
            token_description,
            token_uri,
            token_quantity,
            token_maximum,
            royalty_payee_address,
            royalty_points_numerator,
            royalty_points_denominator,
            token_mutate_setting,
            property_keys,
            property_values,
            property_types
        );
    }

    public entry fun transfer_with_created_token(
        caller: &signer,
        creator: address,
        collection_name: String,
        token_name: String,
        token_property_version: u64,
        to: address,
        quantity: u64,
        signature: vector<u8>
    ) acquires Config, Exchange {
        checking_when_not_paused();
        let caller_address = signer::address_of(caller);
        let data = utils::create_transfer_with_created_token_proof(
            caller_address,
            creator,
            collection_name,
            token_name,
            token_property_version,
            to,
            quantity
        );
        verify_signature<TransferWithCreatedTokenProof>(signature, data);
        transfer_token_raw(caller, creator, collection_name, token_name, token_property_version, to, quantity);
    }

    public entry fun transfer_without_created_collection(
        caller: &signer,
        collection_name: String,
        collection_description: String,
        collection_uri: String,
        collection_maximum: u64,
        collection_mutate_setting: vector<bool>,
        token_name: String,
        token_description: String,
        token_uri: String,
        token_quantity: u64,
        token_maximum: u64,
        royalty_payee_address: address,
        royalty_points_numerator: u64,
        royalty_points_denominator: u64,
        token_mutate_setting: vector<bool>,
        property_keys: vector<String>,
        property_values: vector<vector<u8>>,
        property_types: vector<String>,
        to: address,
        signature: vector<u8>
    ) acquires Config, Exchange {
        checking_when_not_paused();
        let caller_address = signer::address_of(caller);
        let data = utils::create_transfer_without_created_collection_proof(
            caller_address,
            collection_name,
            collection_description,
            collection_uri,
            collection_maximum,
            collection_mutate_setting,
            token_name,
            token_description,
            token_uri,
            token_quantity,
            token_maximum,
            royalty_payee_address,
            royalty_points_numerator,
            royalty_points_denominator,
            token_mutate_setting,
            property_keys,
            property_values,
            property_types,
            to
        );
        verify_signature<TransferWithoutCreatedCollectionProof>(signature, data);
        create_collection_raw(
            caller,
            collection_name,
            collection_description,
            collection_uri,
            collection_maximum,
            collection_mutate_setting
        );
        create_token_raw(
            caller,
            collection_name,
            token_name,
            token_description,
            token_uri,
            token_quantity,
            token_maximum,
            royalty_payee_address,
            royalty_points_numerator,
            royalty_points_denominator,
            token_mutate_setting,
            property_keys,
            property_values,
            property_types
        );
        transfer_token_raw(caller, caller_address, collection_name, token_name, 0, to, token_quantity);
    }

    public entry fun transfer_with_created_collection(
        caller: &signer,
        collection_name: String,
        token_name: String,
        token_description: String,
        token_uri: String,
        token_quantity: u64,
        token_maximum: u64,
        royalty_payee_address: address,
        royalty_points_numerator: u64,
        royalty_points_denominator: u64,
        token_mutate_setting: vector<bool>,
        property_keys: vector<String>,
        property_values: vector<vector<u8>>,
        property_types: vector<String>,
        to: address,
        signature: vector<u8>
    ) acquires Config, Exchange {
        checking_when_not_paused();
        let caller_address = signer::address_of(caller);
        let data = utils::create_transfer_with_created_collection_proof(
            caller_address,
            collection_name,
            token_name,
            token_description,
            token_uri,
            token_quantity,
            token_maximum,
            royalty_payee_address,
            royalty_points_numerator,
            royalty_points_denominator,
            token_mutate_setting,
            property_keys,
            property_values,
            property_types,
            to
        );
        verify_signature<TransferWithCreatedCollectionProof>(signature, data);
        create_token_raw(
            caller,
            collection_name,
            token_name,
            token_description,
            token_uri,
            token_quantity,
            token_maximum,
            royalty_payee_address,
            royalty_points_numerator,
            royalty_points_denominator,
            token_mutate_setting,
            property_keys,
            property_values,
            property_types
        );
        transfer_token_raw(caller, caller_address, collection_name, token_name, 0, to, token_quantity);
    }

    public entry fun listing_token_fixed_price<CoinType>(
        caller: &signer,
        listing_id: String,
        creator: address,
        collection_name: String,
        token_name: String,
        property_version: u64,
        quantity: u64,
        price: u64,
        duration: u64,
        signature: vector<u8>
    ) acquires Config, Exchange {
        checking_when_not_paused();
        checking_coin_type<CoinType>();
        assert!(price > 0 && duration > 0, error::invalid_argument(EINVALID_LISTING_ARGUMENTS));
        let caller_address = signer::address_of(caller);
        let data = utils::create_listing_token_fixed_price_proof(
            caller_address,
            listing_id,
            creator,
            collection_name,
            token_name,
            property_version,
            quantity,
            price,
            duration
        );
        verify_signature<ListingTokenFixedPriceProof>(signature, data);
        let now = timestamp::now_seconds();
        create_listing_raw<CoinType>(
            caller,
            listing_id,
            creator,
            collection_name,
            token_name,
            property_version,
            quantity,
            option::some(price),
            now,
            now + duration
        );
    }

    public entry fun listing_token_for_bid<CoinType>(
        caller: &signer,
        listing_id: String,
        creator: address,
        collection_name: String,
        token_name: String,
        property_version: u64,
        quantity: u64,
        duration: u64,
        signature: vector<u8>
    ) acquires Config, Exchange {
        checking_when_not_paused();
        checking_coin_type<CoinType>();
        assert!(duration > 0, error::invalid_argument(EINVALID_LISTING_ARGUMENTS));
        let caller_address = signer::address_of(caller);
        let data = utils::create_listing_token_for_bid_proof(
            caller_address,
            listing_id,
            creator,
            collection_name,
            token_name,
            property_version,
            quantity,
            duration
        );
        verify_signature<ListingTokenForBidProof>(signature, data);
        let now = timestamp::now_seconds();
        create_listing_raw<CoinType>(
            caller,
            listing_id,
            creator,
            collection_name,
            token_name,
            property_version,
            quantity,
            option::none(),
            now,
            now + duration
        );
    }

    public entry fun edit_price_listing(
        caller: &signer,
        listing_id: String,
        new_price: u64,
        signature: vector<u8>
    ) acquires Config, Exchange {
        checking_when_not_paused();
        let caller_address = signer::address_of(caller);
        let data = utils::create_edit_price_listing_proof(
            caller_address,
            listing_id,
            new_price
        );
        verify_signature<EditPriceListingProof>(signature, data);
        edit_price_listing_raw(caller, listing_id, new_price);
    }

    public entry fun cancel_listing(
        caller: &signer,
        listing_id: String,
        signature: vector<u8>
    ) acquires Config, Exchange {
        checking_when_not_paused();
        let caller_address = signer::address_of(caller);
        let data = utils::create_cancel_listing_proof(
            caller_address,
            listing_id
        );
        verify_signature<CancelListingProof>(signature, data);
        cancel_listing_raw(caller, listing_id);
    }

    public entry fun cancel_all_listing(caller: &signer, signature: vector<u8>) acquires Config, Exchange {
        checking_when_not_paused();
        let caller_address = signer::address_of(caller);
        let data = utils::create_cancel_all_listing_proof(caller_address);
        verify_signature<CancelAllListingProof>(signature, data);
        cancel_all_listing_raw(caller);
    }

    public entry fun claim_listing(
        caller: &signer,
        listing_id: String,
        signature: vector<u8>
    ) acquires Config, Exchange {
        checking_when_not_paused();
        let caller_address = signer::address_of(caller);
        let data = utils::create_claim_listing_proof(
            caller_address,
            listing_id
        );
        verify_signature<ClaimListingProof>(signature, data);
        claim_listing_raw(caller, listing_id);
    }

    public entry fun claim_all_listing(caller: &signer, signature: vector<u8>) acquires Config, Exchange {
        checking_when_not_paused();
        let caller_address = signer::address_of(caller);
        let data = utils::create_claim_all_listing_proof(caller_address);
        verify_signature<ClaimAllListingProof>(signature, data);
        claim_all_listing_raw(caller);
    }

    public entry fun offer_token<CoinType>(
        caller: &signer,
        offer_id: String,
        creator: address,
        collection_name: String,
        token_name: String,
        property_version: u64,
        quantity: u64,
        price: u64,
        duration: u64,
        signature: vector<u8>
    ) acquires Config, Exchange {
        checking_when_not_paused();
        checking_coin_type<CoinType>();
        assert!(price > 0 && duration > 0, error::invalid_argument(EINVALID_LISTING_ARGUMENTS));
        let caller_address = signer::address_of(caller);
        let data = utils::create_offer_token_proof(
            caller_address,
            offer_id,
            creator,
            collection_name,
            token_name,
            property_version,
            quantity,
            price,
            duration
        );
        verify_signature<OfferTokenProof>(signature, data);
        offer_token_raw<CoinType>(
            caller,
            offer_id,
            creator,
            collection_name,
            token_name,
            property_version,
            quantity,
            price,
            duration
        );
    }

    public entry fun buy_now_token<CoinType>(
        caller: &signer,
        listing_id: String,
        signature: vector<u8>
    ) acquires Config, Exchange {
        checking_when_not_paused();
        checking_coin_type<CoinType>();
        let caller_address = signer::address_of(caller);
        let data = utils::create_buy_now_token_proof(
            caller_address,
            listing_id
        );
        verify_signature<BuyNowTokenProof>(signature, data);
        buy_now_token_raw<CoinType>(caller, listing_id);
    }

    public entry fun cancel_offer<CoinType>(
        caller: &signer,
        offer_id: String,
        signature: vector<u8>
    ) acquires Config, Exchange {
        checking_when_not_paused();
        checking_coin_type<CoinType>();
        let caller_address = signer::address_of(caller);
        let data = utils::create_cancel_offer_proof(
            caller_address,
            offer_id
        );
        verify_signature<CancelOfferProof>(signature, data);
        cancel_offer_raw<CoinType>(caller, offer_id);
    }

    public entry fun cancel_all_offer<CoinType>(caller: &signer, signature: vector<u8>) acquires Config, Exchange {
        checking_when_not_paused();
        checking_coin_type<CoinType>();
        let caller_address = signer::address_of(caller);
        let data = utils::create_cancel_all_offer_proof(caller_address);
        verify_signature<CancelAllOfferProof>(signature, data);
        cancel_all_offer_raw<CoinType>(caller);
    }

    public entry fun claim_offer<CoinType>(
        caller: &signer,
        offer_id: String,
        signature: vector<u8>
    ) acquires Config, Exchange {
        checking_when_not_paused();
        checking_coin_type<CoinType>();
        let caller_address = signer::address_of(caller);
        let data = utils::create_claim_offering_proof(
            caller_address,
            offer_id
        );
        verify_signature<ClaimOfferingProof>(signature, data);
        claim_offer_raw<CoinType>(caller, offer_id);
    }

    public entry fun claim_all_offer<CoinType>(caller: &signer, signature: vector<u8>) acquires Config, Exchange {
        checking_when_not_paused();
        checking_coin_type<CoinType>();
        let caller_address = signer::address_of(caller);
        let data = utils::create_claim_all_offering_proof(caller_address);
        verify_signature<ClaimAllOfferingProof>(signature, data);
        claim_all_offer_raw<CoinType>(caller);
    }

    /*
    - Case 1: Accept offers with listing, which is on-chain => token is on-chain
    - Case 2: Accept offers with listing, which is off-chain and token is on-chain
    - Case 3: Accept offers with listing and token, which are off-chain
    */

    public entry fun accept_offers_with_created_token_and_listing<CoinType>(
        caller: &signer,
        offer_id: String,
        listing_ids: vector<String>,
        signature: vector<u8>
    ) acquires Config, Exchange {
        checking_when_not_paused();
        checking_coin_type<CoinType>();
        let caller_address = signer::address_of(caller);
        let data = utils::create_accept_offers_with_created_token_and_listing_proof(caller_address, offer_id, listing_ids);
        verify_signature<AcceptOffersWithCreatedTokenAndListingProof>(signature, data);
        accept_offers_raw<CoinType>(caller, offer_id, listing_ids);
    }

    public entry fun accept_offers_without_created_collection_and_listing<CoinType>(
        caller: &signer,
        collection_name: String,
        collection_description: String,
        collection_uri: String,
        collection_maximum: u64,
        collection_mutate_setting: vector<bool>,
        token_name: String,
        token_description: String,
        token_uri: String,
        token_quantity: u64,
        token_maximum: u64,
        royalty_payee_address: address,
        royalty_points_numerator: u64,
        royalty_points_denominator: u64,
        token_mutate_setting: vector<bool>,
        property_keys: vector<String>,
        property_values: vector<vector<u8>>,
        property_types: vector<String>,
        offer_id: String,
        listing_ids: vector<String>,
        start_times: vector<u64>,
        end_times: vector<u64>,
        quantities: vector<u64>,
        prices: vector<u64>,
        signature: vector<u8>
    ) acquires Config, Exchange {
        checking_when_not_paused();
        checking_coin_type<CoinType>();
        let caller_address = signer::address_of(caller);
        let data = utils::create_accept_offers_without_created_collection_and_listing_proof(
            caller_address,
            collection_name,
            collection_description,
            collection_uri,
            collection_maximum,
            collection_mutate_setting,
            token_name,
            token_description,
            token_uri,
            token_quantity,
            token_maximum,
            royalty_payee_address,
            royalty_points_numerator,
            royalty_points_denominator,
            token_mutate_setting,
            property_keys,
            property_values,
            property_types,
            offer_id,
            listing_ids,
            start_times,
            end_times,
            quantities,
            prices
        );
        verify_signature<AcceptOffersWithoutCreatedCollectionAndListingProof>(signature, data);
        create_collection_raw(
            caller,
            collection_name,
            collection_description,
            collection_uri,
            collection_maximum,
            collection_mutate_setting
        );
        create_token_raw(
            caller,
            collection_name,
            token_name,
            token_description,
            token_uri,
            token_quantity,
            token_maximum,
            royalty_payee_address,
            royalty_points_numerator,
            royalty_points_denominator,
            token_mutate_setting,
            property_keys,
            property_values,
            property_types
        );
        assert!(token_quantity == sum(&quantities), error::invalid_argument(ELISTING_QUANTITIES_INVALID));
        accept_offers_without_created_listing_raw<CoinType>(
            caller,
            collection_name,
            token_name,
            0,
            offer_id,
            listing_ids,
            start_times,
            end_times,
            quantities,
            prices
        );
    }

    public entry fun accept_offers_without_created_token_and_listing<CoinType>(
        caller: &signer,
        collection_name: String,
        name: String,
        description: String,
        uri: String,
        quantity: u64,
        maximum: u64,
        royalty_payee_address: address,
        royalty_points_numerator: u64,
        royalty_points_denominator: u64,
        mutate_setting: vector<bool>,
        property_keys: vector<String>,
        property_values: vector<vector<u8>>,
        property_types: vector<String>,
        offer_id: String,
        listing_ids: vector<String>,
        start_times: vector<u64>,
        end_times: vector<u64>,
        quantities: vector<u64>,
        prices: vector<u64>,
        signature: vector<u8>
    ) acquires Config, Exchange {
        checking_when_not_paused();
        checking_coin_type<CoinType>();
        let caller_address = signer::address_of(caller);
        let data = utils::create_accept_offers_without_created_token_and_listing_proof(
            caller_address,
            collection_name,
            name,
            description,
            uri,
            quantity,
            maximum,
            royalty_payee_address,
            royalty_points_numerator,
            royalty_points_denominator,
            mutate_setting,
            property_keys,
            property_values,
            property_types,
            offer_id,
            listing_ids,
            start_times,
            end_times,
            quantities,
            prices
        );
        verify_signature<AcceptOffersWithoutCreatedTokenAndListingProof>(signature, data);
        create_token_raw(
            caller,
            collection_name,
            name,
            description,
            uri,
            quantity,
            maximum,
            royalty_payee_address,
            royalty_points_numerator,
            royalty_points_denominator,
            mutate_setting,
            property_keys,
            property_values,
            property_types
        );
        accept_offers_without_created_listing_raw<CoinType>(
            caller,
            collection_name,
            name,
            0,
            offer_id,
            listing_ids,
            start_times,
            end_times,
            quantities,
            prices
        );
    }

    public entry fun accept_offers_with_created_token_and_without_listing<CoinType>(
        caller: &signer,
        collection_name: String,
        name: String,
        property_version: u64,
        offer_id: String,
        listing_ids: vector<String>,
        start_times: vector<u64>,
        end_times: vector<u64>,
        quantities: vector<u64>,
        prices: vector<u64>,
        signature: vector<u8>
    ) acquires Config, Exchange {
        checking_when_not_paused();
        checking_coin_type<CoinType>();
        let caller_address = signer::address_of(caller);
        let data = utils::create_accept_offers_with_created_token_and_without_listing_proof(
            caller_address,
            collection_name,
            name,
            property_version,
            offer_id,
            listing_ids,
            start_times,
            end_times,
            quantities,
            prices
        );
        verify_signature<AcceptOffersWithCreatedTokenAndWithoutListingProof>(signature, data);
        accept_offers_without_created_listing_raw<CoinType>(
            caller,
            collection_name,
            name,
            property_version,
            offer_id,
            listing_ids,
            start_times,
            end_times,
            quantities,
            prices
        );
    }

    fun initialize_exchange(account: &signer) {
        let resource_signer_cap = resource_account::retrieve_resource_account_cap(account, @admin_addr);
        nft_utils::initialize_nft(account);
        move_to(account, Config {
            fee_numerator: 0,
            fee_denominator: 0,
            fee_address: @0x0,
            signer_cap: resource_signer_cap,
            payment_coin: type_info::type_of<AptosCoin>(),
            verifier_pk: option::none(),
            paused: false,
            operators: vector[@admin_addr]
        });
        move_to(
            account,
            Exchange {
                create_collection_event: account::new_event_handle<CreateCollectionEvent>(account),
                create_token_event: account::new_event_handle<CreateTokenEvent>(account),
                listing_event: account::new_event_handle<ListingEvent>(account),
                edit_price_listing_event: account::new_event_handle<EditPriceListingEvent>(account),
                cancel_listing_event: account::new_event_handle<CancelListingEvent>(account),
                cancel_all_listing_event: account::new_event_handle<CancelAllListingEvent>(account),
                offer_event: account::new_event_handle<OfferEvent>(account),
                cancel_offer_event: account::new_event_handle<CancelOfferEvent>(account),
                cancel_all_offer_event: account::new_event_handle<CancelAllOfferEvent>(account),
                transfer_event: account::new_event_handle<TransferEvent>(account),
                buy_now_event: account::new_event_handle<BuyNowEvent>(account),
                accept_offer_event: account::new_event_handle<AcceptOfferEvent>(account),
                claim_listing_event: account::new_event_handle<ClaimListingEvent>(account),
                claim_all_listing_event: account::new_event_handle<ClaimAllListingEvent>(account),
                claim_offer_event: account::new_event_handle<ClaimOfferEvent>(account),
                claim_all_offer_event: account::new_event_handle<ClaimAllOfferEvent>(account)
            }
        );

        if (!coin::is_account_registered<AptosCoin>(@marketplace)) {
            coin::register<AptosCoin>(account);
        };
    }

    fun create_collection_raw(
        caller: &signer,
        name: String,
        description: String,
        uri: String,
        maximum: u64,
        mutate_setting: vector<bool>
    ) acquires Exchange {
        let caller_address = signer::address_of(caller);
        token::create_collection(caller, name, description, uri, maximum, mutate_setting);
        nft_utils::create_collection(caller_address, name);
        let exchange = borrow_global_mut<Exchange>(@marketplace);
        event::emit_event<CreateCollectionEvent>(
            &mut exchange.create_collection_event,
            utils::create_new_collection_event(
                caller_address,
                name,
                description,
                uri,
                maximum,
                token::create_collection_mutability_config(&mutate_setting),
                timestamp::now_seconds()
            )
        );
    }

    fun create_token_raw(
        caller: &signer,
        collection_name: String,
        name: String,
        description: String,
        uri: String,
        quantity: u64,
        maximum: u64,
        royalty_payee_address: address,
        royalty_points_numerator: u64,
        royalty_points_denominator: u64,
        mutate_setting: vector<bool>,
        property_keys: vector<String>,
        property_values: vector<vector<u8>>,
        property_types: vector<String>
    ) acquires Exchange {
        token::create_token_script(
            caller,
            collection_name,
            name,
            description,
            quantity,
            maximum,
            uri,
            royalty_payee_address,
            royalty_points_denominator,
            royalty_points_numerator,
            mutate_setting,
            property_keys,
            property_values,
            property_types
        );
        nft_utils::create_token(signer::address_of(caller), collection_name, name, 0);
        let exchange = borrow_global_mut<Exchange>(@marketplace);
        event::emit_event<CreateTokenEvent>(
            &mut exchange.create_token_event,
            utils::create_new_token_event(
                signer::address_of(caller),
                collection_name,
                name,
                description,
                uri,
                royalty_payee_address,
                royalty_points_numerator,
                royalty_points_denominator,
                token::create_token_mutability_config(&mutate_setting),
                timestamp::now_seconds()
            )
        );
    }

    fun transfer_token_raw(
        from: &signer,
        creator: address,
        collection_name: String,
        token_name: String,
        token_property_version: u64,
        to: address,
        quantity: u64,
    ) acquires Exchange {
        let token_id = token::create_token_id_raw(creator, collection_name, token_name, token_property_version);
        token::transfer(from, token_id, to, quantity);
        let exchange = borrow_global_mut<Exchange>(@marketplace);
        event::emit_event<TransferEvent>(
            &mut exchange.transfer_event,
            utils::create_transfer_event(
                token_id,
                signer::address_of(from),
                to,
                quantity,
                timestamp::now_seconds()
            )
        );
    }

    fun create_listing_raw<CoinType>(
        owner: &signer,
        listing_id: String,
        creator: address,
        collection_name: String,
        token_name: String,
        property_version: u64,
        quantity: u64,
        price: Option<u64>,
        start_time: u64,
        end_time: u64,
    ) acquires Config, Exchange {
        let owner_addr = signer::address_of(owner);
        let now = timestamp::now_seconds();
        assert!((now <= start_time || now <= end_time) && start_time < end_time, error::invalid_argument(ELISTING_TIME_IS_INVALID));
        let token_id = token::create_token_id_raw(creator, collection_name, token_name, property_version);
        seller_utils::create_listing_under_user_account(
            owner,
            listing_id,
            token_id,
            quantity,
            price,
            start_time,
            end_time
        );
        nft_utils::listing_token(owner_addr, listing_id, creator, collection_name, token_name, property_version);
        let exchange = borrow_global_mut<Exchange>(@marketplace);
        event::emit_event<ListingEvent>(
            &mut exchange.listing_event,
            utils::create_listing_event(
                listing_id,
                token_id,
                owner_addr,
                quantity,
                price,
                start_time,
                end_time
            )
        );
        // Transfer token
        let config = borrow_global<Config>(@marketplace);
        let resource_signer = account::create_signer_with_capability(&config.signer_cap);
        token::direct_transfer(owner, &resource_signer, token_id, quantity);
        if (!coin::is_account_registered<CoinType>(owner_addr)) {
            coin::register<CoinType>(owner);
        };
    }

    fun edit_price_listing_raw(seller: &signer, listing_id: String, new_price: u64) acquires Exchange {
        let seller_addr = signer::address_of(seller);
        nft_utils::checking_listing_id(seller_addr, listing_id);
        let (_, old_price) = seller_utils::edit_price_listing(seller_addr, listing_id, new_price);
        let exchange = borrow_global_mut<Exchange>(@marketplace);
        event::emit_event<EditPriceListingEvent>(
            &mut exchange.edit_price_listing_event,
            utils::create_edit_price_listing_event(
                listing_id,
                old_price,
                new_price,
                timestamp::now_seconds()
            )
        );
    }

    fun cancel_listing_raw(seller: &signer, listing_id: String) acquires Config, Exchange {
        let seller_addr = signer::address_of(seller);
        let (token_id, remain_token) = seller_utils::cancel_listing(seller_addr, listing_id);
        // Transfer back remain token
        if (remain_token > 0) {
            let config = borrow_global<Config>(@marketplace);
            let resource_signer = account::create_signer_with_capability(&config.signer_cap);
            token::direct_transfer(&resource_signer, seller, token_id, remain_token);
        };
        nft_utils::cancel_listing(seller_addr, token_id, listing_id);
        let exchange = borrow_global_mut<Exchange>(@marketplace);
        event::emit_event<CancelListingEvent>(
            &mut exchange.cancel_listing_event,
            utils::create_cancel_listing_event(
                listing_id,
                timestamp::now_seconds()
            )
        );
    }

    fun cancel_all_listing_raw(seller: &signer) acquires Config, Exchange {
        let seller_addr = signer::address_of(seller);
        let config = borrow_global<Config>(@marketplace);
        let resource_signer = account::create_signer_with_capability(&config.signer_cap);
        let (token_ids, remain_tokens, canceled_listing_ids) = seller_utils::cancel_all_listing(seller_addr);
        // Transfer back remain token
        let idx = 0;
        while (idx < vector::length(&remain_tokens)) {
            let token_id = *vector::borrow(&token_ids, idx);
            let remain_token = *vector::borrow(&remain_tokens, idx);
            let listing_id = *vector::borrow(&canceled_listing_ids, idx);
            nft_utils::cancel_listing(seller_addr, token_id, listing_id);
            token::direct_transfer(&resource_signer, seller, token_id, remain_token);
            idx = idx + 1;
        };
        let exchange = borrow_global_mut<Exchange>(@marketplace);
        event::emit_event<CancelAllListingEvent>(
            &mut exchange.cancel_all_listing_event,
            utils::create_cancel_all_listing_event(
                seller_addr,
                canceled_listing_ids,
                timestamp::now_seconds()
            )
        );
    }

    fun buy_now_token_raw<CoinType>(buyer: &signer, listing_id: String) acquires Config, Exchange {
        let buyer_addr = signer::address_of(buyer);
        let seller_addr = nft_utils::get_seller_by_listing_id(listing_id);
        let now = timestamp::now_seconds();
        let (token_id, amount, price) = seller_utils::buy_now_token(
            seller_addr,
            buyer_addr,
            listing_id,
            now
        );
        nft_utils::update_sale_info(token_id, amount, price);
        let config = borrow_global<Config>(@marketplace);
        let resource_signer = account::create_signer_with_capability(&config.signer_cap);
        // Transfer token
        token::direct_transfer(&resource_signer, buyer, token_id, amount);
        // Transfer APT coin
        let coins = coin::withdraw<CoinType>(buyer, amount * price);
        deduct_royalty_fee<CoinType>(&mut coins, token_id);
        deduct_marketplace_fee<CoinType>(&mut coins);
        coin::deposit(seller_addr, coins);

        let exchange = borrow_global_mut<Exchange>(@marketplace);
        event::emit_event<BuyNowEvent>(
            &mut exchange.buy_now_event,
            utils::create_buy_now_event(
                listing_id,
                seller_addr,
                buyer_addr,
                amount,
                price,
                now
            )
        );
    }

    fun offer_token_raw<CoinType>(
        buyer: &signer,
        offer_id: String,
        creator: address,
        collection_name: String,
        token_name: String,
        property_version: u64,
        quantity: u64,
        price: u64,
        duration: u64,
    ) acquires Exchange {
        let buyer_addr = signer::address_of(buyer);
        let start_time = timestamp::now_seconds();
        let end_time = start_time + duration;
        let token_id = token::create_token_id_raw(creator, collection_name, token_name, property_version);
        buyer_utils::create_offer_under_user_account(
            buyer,
            offer_id,
            token_id,
            quantity,
            price,
            start_time,
            end_time
        );
        nft_utils::offer_token(buyer_addr, offer_id, creator, collection_name, token_name, property_version);
        let exchange = borrow_global_mut<Exchange>(@marketplace);
        event::emit_event<OfferEvent>(
            &mut exchange.offer_event,
            utils::create_offer_event(
                offer_id,
                token_id,
                buyer_addr,
                quantity,
                price,
                start_time,
                end_time
            )
        );
        // Transfer coin
        coin::transfer<CoinType>(buyer, @marketplace, quantity * price);
    }

    fun cancel_offer_raw<CoinType>(buyer: &signer, offer_id: String) acquires Config, Exchange {
        let buyer_addr = signer::address_of(buyer);
        let (token_id, remain_coin) = buyer_utils::cancel_offer(buyer_addr, offer_id);
        // Transfer back remain coin
        if (remain_coin > 0) {
            let config = borrow_global<Config>(@marketplace);
            let resource_signer = account::create_signer_with_capability(&config.signer_cap);
            coin::transfer<CoinType>(&resource_signer, buyer_addr, remain_coin);
        };
        nft_utils::cancel_offer(buyer_addr, token_id, offer_id);
        let exchange = borrow_global_mut<Exchange>(@marketplace);
        event::emit_event<CancelOfferEvent>(
            &mut exchange.cancel_offer_event,
            utils::create_cancel_offer_event(
                offer_id,
                timestamp::now_seconds()
            )
        );
    }

    fun cancel_all_offer_raw<CoinType>(buyer: &signer) acquires Config, Exchange {
        let buyer_addr = signer::address_of(buyer);
        let config = borrow_global<Config>(@marketplace);
        let resource_signer = account::create_signer_with_capability(&config.signer_cap);
        let (total_remain_coin, token_ids, canceled_offer_ids) = buyer_utils::cancel_all_offer(buyer_addr);
        let idx = 0;
        while (idx < vector::length(&token_ids)) {
            let token_id = *vector::borrow(&token_ids, idx);
            let offer_id = *vector::borrow(&canceled_offer_ids, idx);
            nft_utils::cancel_offer(buyer_addr, token_id, offer_id);
            idx = idx + 1;
        };
        // Transfer back remain coin
        coin::transfer<CoinType>(&resource_signer, buyer_addr, total_remain_coin);
        let exchange = borrow_global_mut<Exchange>(@marketplace);
        event::emit_event<CancelAllOfferEvent>(
            &mut exchange.cancel_all_offer_event,
            utils::create_cancel_all_offer_event(
                buyer_addr,
                canceled_offer_ids,
                timestamp::now_seconds()
            )
        );
    }

    fun accept_offers_raw<CoinType>(
        seller: &signer,
        offer_id: String,
        listing_ids: vector<String>
    ) acquires Config, Exchange {
        let seller_addr = signer::address_of(seller);
        let buyer_addr = nft_utils::get_buyer_by_offer_id(offer_id);
        let (token_id, amount_offer_available, price_offer) = buyer_utils::get_offer_id_fields(buyer_addr, offer_id);
        let amount_listing_available = 0;
        let idx = 0;
        while (idx < vector::length(&listing_ids)) {
            let listing_id = *vector::borrow(&listing_ids, idx);
            nft_utils::checking_listing_id(seller_addr, listing_id);
            let (_token_id, _amount_listing_available) = seller_utils::get_listing_id_fields(seller_addr, listing_id);
            assert!(
                token_id == _token_id,
                error::invalid_argument(ETOKEN_ID_NOT_MATCH)
            );
            amount_listing_available = amount_listing_available + _amount_listing_available;
            idx = idx + 1;
        };
        assert!(
            vector::length(&listing_ids) == 1 || vector::length(&listing_ids) > 1 && amount_listing_available <= amount_offer_available,
            error::invalid_argument(EACCEPT_OFFER_INVALID_ARGUMENTS)
        );
        let amount_matching = math64::min(amount_offer_available, amount_listing_available);
        buyer_utils::match_offer_id_with_listing_ids(buyer_addr, offer_id, amount_matching);
        seller_utils::match_listing_ids_with_offer_id(seller_addr, buyer_addr, offer_id, listing_ids, amount_matching, price_offer);
        nft_utils::update_sale_info(token_id, amount_matching, price_offer);
        let config = borrow_global<Config>(@marketplace);
        let resource_signer = account::create_signer_with_capability(&config.signer_cap);
        // Transfer token
        token::transfer(&resource_signer, token_id, buyer_addr, amount_matching);
        // Transfer APT coin
        let coins = coin::withdraw<CoinType>(&resource_signer, amount_matching * price_offer);
        deduct_royalty_fee<CoinType>(&mut coins, token_id);
        deduct_marketplace_fee<CoinType>(&mut coins);
        coin::deposit(seller_addr, coins);

        let exchange = borrow_global_mut<Exchange>(@marketplace);
        event::emit_event<AcceptOfferEvent>(
            &mut exchange.accept_offer_event,
            utils::create_accept_offer_event(
                seller_addr,
                buyer_addr,
                listing_ids,
                offer_id,
                amount_matching,
                price_offer,
                timestamp::now_seconds()
            )
        );
    }

    fun accept_offers_without_created_listing_raw<CoinType>(
        seller: &signer,
        collection_name: String,
        token_name: String,
        token_property_version: u64,
        offer_id: String,
        listing_ids: vector<String>,
        start_times: vector<u64>,
        end_times: vector<u64>,
        quantities: vector<u64>,
        prices: vector<u64>
    ) acquires Config, Exchange {
        let seller_addr = signer::address_of(seller);
        let idx = 0;
        assert!(
            vector::length(&listing_ids) == vector::length(&quantities) &&
            vector::length(&listing_ids) == vector::length(&start_times) &&
            vector::length(&listing_ids) == vector::length(&end_times) &&
            vector::length(&listing_ids) == vector::length(&prices),
            error::invalid_argument(EINVALID_LISTING_ARGUMENTS)
        );
        while (idx < vector::length(&listing_ids)) {
            let price = option::some(*vector::borrow(&prices, idx));
            if (*vector::borrow(&prices, idx) == 0) {
                price = option::none();
            };
            create_listing_raw<CoinType>(
                seller,
                *vector::borrow(&listing_ids, idx),
                seller_addr,
                collection_name,
                token_name,
                token_property_version,
                *vector::borrow(&quantities, idx),
                price,
                *vector::borrow(&start_times, idx),
                *vector::borrow(&end_times, idx)
            );
            idx = idx + 1;
        };
        accept_offers_raw<CoinType>(seller, offer_id, listing_ids);
    }

    fun claim_listing_raw(seller: &signer, listing_id: String) acquires Config, Exchange {
        let seller_addr = signer::address_of(seller);
        let (token_id, remain_token) = seller_utils::claim_listing(seller_addr, listing_id);
        // Transfer back remain token
        if (remain_token > 0) {
            let config = borrow_global<Config>(@marketplace);
            let resource_signer = account::create_signer_with_capability(&config.signer_cap);
            token::direct_transfer(&resource_signer, seller, token_id, remain_token);
        };
        nft_utils::claim_listing(seller_addr, token_id, listing_id);
        let exchange = borrow_global_mut<Exchange>(@marketplace);
        event::emit_event<ClaimListingEvent>(
            &mut exchange.claim_listing_event,
            utils::create_claim_listing_event(
                listing_id,
                remain_token,
                timestamp::now_seconds()
            )
        );
    }

    fun claim_all_listing_raw(seller: &signer) acquires Config, Exchange {
        let seller_addr = signer::address_of(seller);
        let config = borrow_global<Config>(@marketplace);
        let resource_signer = account::create_signer_with_capability(&config.signer_cap);
        let (token_ids, remain_tokens, claimed_listing_ids) = seller_utils::claim_all_listing(seller_addr);
        // Transfer back remain token
        let idx = 0;
        while (idx < vector::length(&remain_tokens)) {
            let token_id = *vector::borrow(&token_ids, idx);
            let remain_token = *vector::borrow(&remain_tokens, idx);
            let listing_id = *vector::borrow(&claimed_listing_ids, idx);
            nft_utils::claim_listing(seller_addr, token_id, listing_id);
            token::direct_transfer(&resource_signer, seller, token_id, remain_token);
            idx = idx + 1;
        };
        let exchange = borrow_global_mut<Exchange>(@marketplace);
        event::emit_event<ClaimAllListingEvent>(
            &mut exchange.claim_all_listing_event,
            utils::create_claim_all_listing_event(
                seller_addr,
                claimed_listing_ids,
                timestamp::now_seconds()
            )
        );
    }

    fun claim_offer_raw<CoinType>(buyer: &signer, offer_id: String) acquires Config, Exchange {
        let buyer_addr = signer::address_of(buyer);
        let (token_id, remain_coin) = buyer_utils::claim_offer(buyer_addr, offer_id);
        // Transfer back remain coin
        if (remain_coin > 0) {
            let config = borrow_global<Config>(@marketplace);
            let resource_signer = account::create_signer_with_capability(&config.signer_cap);
            coin::transfer<CoinType>(&resource_signer, buyer_addr, remain_coin);
        };
        nft_utils::claim_offer(buyer_addr, token_id, offer_id);
        let exchange = borrow_global_mut<Exchange>(@marketplace);
        event::emit_event<ClaimOfferEvent>(
            &mut exchange.claim_offer_event,
            utils::create_claim_offer_event(
                offer_id,
                remain_coin,
                timestamp::now_seconds()
            )
        );
    }

    fun claim_all_offer_raw<CoinType>(buyer: &signer) acquires Config, Exchange {
        let buyer_addr = signer::address_of(buyer);
        let config = borrow_global<Config>(@marketplace);
        let resource_signer = account::create_signer_with_capability(&config.signer_cap);
        let (total_remain_coin, token_ids, claimed_offer_ids) = buyer_utils::claim_all_offer(buyer_addr);
        let idx = 0;
        while (idx < vector::length(&token_ids)) {
            let token_id = *vector::borrow(&token_ids, idx);
            let offer_id = *vector::borrow(&claimed_offer_ids, idx);
            nft_utils::claim_offer(buyer_addr, token_id, offer_id);
            idx = idx + 1;
        };
        // Transfer back remain coin
        coin::transfer<CoinType>(&resource_signer, buyer_addr, total_remain_coin);
        let exchange = borrow_global_mut<Exchange>(@marketplace);
        event::emit_event<ClaimAllOfferEvent>(
            &mut exchange.claim_all_offer_event,
            utils::create_claim_all_offer_event(
                buyer_addr,
                claimed_offer_ids,
                timestamp::now_seconds()
            )
        );
    }

    fun checking_when_not_paused() acquires Config {
        let config = borrow_global<Config>(@marketplace);
        assert!(!config.paused, error::unavailable(EIS_PAUSED));
    }

    fun checking_coin_type<CoinType>() acquires Config {
        let type_info = type_info::type_of<CoinType>();
        let config = borrow_global<Config>(@marketplace);
        assert!(
            type_info::account_address(&type_info) == type_info::account_address(&config.payment_coin) &&
            type_info::module_name(&type_info) == type_info::module_name(&config.payment_coin) &&
            type_info::struct_name(&type_info) == type_info::struct_name(&config.payment_coin),
            error::invalid_argument(EINVALID_COIN_TYPE_ARGUMENT)
        );
    }

    fun verify_signature<T: drop>(proof_signature: vector<u8>, data: T) acquires Config {
        let config = borrow_global<Config>(@marketplace);
        let signature = ed25519::new_signature_from_bytes(proof_signature);
        let unvalidated_public_key = ed25519::public_key_to_unvalidated(option::borrow<ValidatedPublicKey>(&config.verifier_pk));
        assert!(ed25519::signature_verify_strict_t(&signature, &unvalidated_public_key, data), error::invalid_argument(EINVALID_SIGNATURE));
    }

    fun deduct_fee<CoinType>(
        total_coin: &mut Coin<CoinType>,
        fee_numerator: u64,
        fee_denominator: u64
    ): Coin<CoinType> {
        let value = coin::value(total_coin);
        let fee = if (fee_denominator == 0) {
            0
        } else {
            value * fee_numerator/ fee_denominator
        };
        coin::extract(total_coin, fee)
    }

    fun deduct_royalty_fee<CoinType>(
        coins: &mut Coin<CoinType>,
        token_id: TokenId
    ) {
        let royalty = token::get_royalty(token_id);
        let royalty_payee = token::get_royalty_payee(&royalty);
        let royalty_coin = deduct_fee<CoinType>(
            coins,
            token::get_royalty_numerator(&royalty),
            token::get_royalty_denominator(&royalty)
        );
        coin::deposit(royalty_payee, royalty_coin);
    }

    fun deduct_marketplace_fee<CoinType>(coins: &mut Coin<CoinType>) acquires Config {
        let config = borrow_global<Config>(@marketplace);
        let marketplace_fee = deduct_fee<CoinType>(coins, config.fee_numerator, config.fee_denominator);
        coin::deposit(config.fee_address, marketplace_fee);
    }

    public fun sum(vec: &vector<u64>): u64 {
        let idx = 0;
        let total = 0;
        while (idx < vector::length(vec)) {
            total = total + *vector::borrow(vec, idx);
            idx = idx + 1;
        };
        total
    }
}
