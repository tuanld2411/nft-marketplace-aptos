module marketplace::utils {
    use std::string::String;
    use std::option::Option;
    use aptos_token::token::{
        TokenId,
        CollectionMutabilityConfig,
        TokenMutabilityConfig
    };

    struct CreateCollectionEvent has copy, drop, store {
        creator: address,
        name: String,
        description: String,
        uri: String,
        maximum: u64,
        mutability_config: CollectionMutabilityConfig,
        timestamp: u64
    }

    struct CreateTokenEvent has copy, drop, store {
        creator: address,
        collection_name: String,
        name: String,
        description: String,
        uri: String,
        royalty_payee_address: address,
        royalty_points_numerator: u64,
        royalty_points_denominator: u64,
        mutability_config: TokenMutabilityConfig,
        timestamp: u64
    }

    struct ListingEvent has copy, drop, store {
        listing_id: String,
        token_id: TokenId,
        seller: address,
        quantity: u64,
        price: Option<u64>,
        start_time: u64,
        end_time: u64
    }

    struct EditPriceListingEvent has copy, drop, store {
        listing_id: String,
        old_price: u64,
        new_price: u64,
        timestamp: u64
    }

    struct CancelListingEvent has copy, drop, store {
        listing_id: String,
        timestamp: u64
    }

    struct CancelAllListingEvent has copy, drop, store {
        seller: address,
        listing_ids: vector<String>,
        timestamp: u64
    }

    struct OfferEvent has copy, drop, store {
        offer_id: String,
        token_id: TokenId,
        buyer: address,
        quantity: u64,
        price: u64,
        start_time: u64,
        end_time: u64
    }

    struct CancelOfferEvent has copy, drop, store {
        offer_id: String,
        timestamp: u64
    }

    struct CancelAllOfferEvent has copy, drop, store {
        buyer: address,
        offer_ids: vector<String>,
        timestamp: u64
    }

    struct TransferEvent has copy, drop, store {
        token_id: TokenId,
        from: address,
        to: address,
        quantity: u64,
        timestamp: u64
    }

    struct BuyNowEvent has copy, drop, store {
        listing_id: String,
        seller: address,
        buyer: address,
        quantity: u64,
        price: u64,
        timestamp: u64
    }

    struct AcceptOfferEvent has copy, drop, store {
        seller: address,
        buyer: address,
        listing_ids: vector<String>,
        offer_id: String,
        quantity: u64,
        price: u64,
        timestamp: u64
    }

    struct ClaimListingEvent has copy, drop, store {
        listing_id: String,
        quantity: u64,
        timestamp: u64
    }

    struct ClaimAllListingEvent has copy, drop, store {
        seller: address,
        listing_ids: vector<String>,
        timestamp: u64
    }

    struct ClaimOfferEvent has copy, drop, store {
        offer_id: String,
        quantity: u64,
        timestamp: u64
    }

    struct ClaimAllOfferEvent has copy, drop, store {
        buyer: address,
        offer_ids: vector<String>,
        timestamp: u64
    }

    struct CreateCollectionProof has drop {
        creator: address,
        name: String,
        description: String,
        uri: String,
        maximum: u64,
        mutate_setting: vector<bool>
    }

    struct CreateTokenWithCreatedCollectionProof has drop {
        creator: address,
        collection_name: String,
        name: String,
        description: String,
        uri: String,
        quantity: u64,
        maximum: u64,
        royalty_payee_address: address,
        royalty_points_numerator: u64,
        royalty_points_denominator: u64,
        mutate_setting: vector<bool>,
        property_keys: vector<String>,
        property_values: vector<vector<u8>>,
        property_types: vector<String>
    }

    struct CreateTokenWithoutCreatedCollectionProof has drop {
        creator: address,
        collection_name: String,
        collection_description: String,
        collection_uri: String,
        collection_maximum: u64,
        collection_mutate_setting: vector<bool>,
        token_name: String,
        token_description: String,
        token_uri: String,
        token_quantity: u64,
        token_maximum: u64,
        royalty_payee_address: address,
        royalty_points_numerator: u64,
        royalty_points_denominator: u64,
        token_mutate_setting: vector<bool>,
        property_keys: vector<String>,
        property_values: vector<vector<u8>>,
        property_types: vector<String>
    }

    struct TransferWithCreatedTokenProof has drop {
        from: address,
        creator: address,
        collection_name: String,
        token_name: String,
        token_property_version: u64,
        to: address,
        quantity: u64
    }

    struct TransferWithoutCreatedCollectionProof has drop {
        creator: address,
        collection_name: String,
        collection_description: String,
        collection_uri: String,
        collection_maximum: u64,
        collection_mutate_setting: vector<bool>,
        token_name: String,
        token_description: String,
        token_uri: String,
        token_quantity: u64,
        token_maximum: u64,
        royalty_payee_address: address,
        royalty_points_numerator: u64,
        royalty_points_denominator: u64,
        token_mutate_setting: vector<bool>,
        property_keys: vector<String>,
        property_values: vector<vector<u8>>,
        property_types: vector<String>,
        to: address
    }

    struct TransferWithCreatedCollectionProof has drop {
        creator: address,
        collection_name: String,
        token_name: String,
        token_description: String,
        token_uri: String,
        token_quantity: u64,
        token_maximum: u64,
        royalty_payee_address: address,
        royalty_points_numerator: u64,
        royalty_points_denominator: u64,
        token_mutate_setting: vector<bool>,
        property_keys: vector<String>,
        property_values: vector<vector<u8>>,
        property_types: vector<String>,
        to: address
    }

    struct ListingTokenFixedPriceProof has drop {
        owner: address,
        listing_id: String,
        creator: address,
        collection_name: String,
        token_name: String,
        property_version: u64,
        quantity: u64,
        price: u64,
        duration: u64
    }

    struct ListingTokenForBidProof has drop {
        owner: address,
        listing_id: String,
        creator: address,
        collection_name: String,
        token_name: String,
        property_version: u64,
        quantity: u64,
        duration: u64
    }

    struct EditPriceListingProof has drop {
        seller: address,
        listing_id: String,
        new_price: u64
    }

    struct CancelListingProof has drop {
        seller: address,
        listing_id: String
    }

    struct CancelAllListingProof has drop {
        seller: address
    }

    struct BuyNowTokenProof has drop {
        buyer: address,
        listing_id: String
    }

    struct OfferTokenProof has drop {
        buyer: address,
        offer_id: String,
        creator: address,
        collection_name: String,
        token_name: String,
        property_version: u64,
        quantity: u64,
        price: u64,
        duration: u64
    }

    struct CancelOfferProof has drop {
        buyer: address,
        offer_id: String
    }

    struct CancelAllOfferProof has drop {
        buyer: address
    }

    struct AcceptOffersWithCreatedTokenAndListingProof has drop {
        seller: address,
        offer_id: String,
        listing_ids: vector<String>
    }

    struct AcceptOffersWithoutCreatedCollectionAndListingProof has drop {
        seller: address,
        collection_name: String,
        collection_description: String,
        collection_uri: String,
        collection_maximum: u64,
        collection_mutate_setting: vector<bool>,
        token_name: String,
        token_description: String,
        token_uri: String,
        token_quantity: u64,
        token_maximum: u64,
        royalty_payee_address: address,
        royalty_points_numerator: u64,
        royalty_points_denominator: u64,
        token_mutate_setting: vector<bool>,
        property_keys: vector<String>,
        property_values: vector<vector<u8>>,
        property_types: vector<String>,
        offer_id: String,
        listing_ids: vector<String>,
        start_times: vector<u64>,
        end_times: vector<u64>,
        quantities: vector<u64>,
        prices: vector<u64>
    }

    struct AcceptOffersWithoutCreatedTokenAndListingProof has drop {
        seller: address,
        collection_name: String,
        name: String,
        description: String,
        uri: String,
        quantity: u64,
        maximum: u64,
        royalty_payee_address: address,
        royalty_points_numerator: u64,
        royalty_points_denominator: u64,
        mutate_setting: vector<bool>,
        property_keys: vector<String>,
        property_values: vector<vector<u8>>,
        property_types: vector<String>,
        offer_id: String,
        listing_ids: vector<String>,
        start_times: vector<u64>,
        end_times: vector<u64>,
        quantities: vector<u64>,
        prices: vector<u64>
    }

    struct AcceptOffersWithCreatedTokenAndWithoutListingProof has drop {
        seller: address,
        collection_name: String,
        name: String,
        property_version: u64,
        offer_id: String,
        listing_ids: vector<String>,
        start_times: vector<u64>,
        end_times: vector<u64>,
        quantities: vector<u64>,
        prices: vector<u64>
    }

    struct ClaimListingProof has drop {
        seller: address,
        listing_id: String
    }

    struct ClaimAllListingProof has drop {
        seller: address
    }

    struct ClaimOfferingProof has drop {
        buyer: address,
        offer_id: String
    }

    struct ClaimAllOfferingProof has drop {
        buyer: address
    }

    public fun create_new_collection_proof(
        creator: address,
        name: String,
        description: String,
        uri: String,
        maximum: u64,
        mutate_setting: vector<bool>
    ): CreateCollectionProof {
        CreateCollectionProof {
            creator,
            name,
            description,
            uri,
            maximum,
            mutate_setting
        }
    }

    public fun create_new_token_with_created_collection_proof(
        creator: address,
        collection_name: String,
        name: String,
        description: String,
        uri: String,
        quantity: u64,
        maximum: u64,
        royalty_payee_address: address,
        royalty_points_numerator: u64,
        royalty_points_denominator: u64,
        mutate_setting: vector<bool>,
        property_keys: vector<String>,
        property_values: vector<vector<u8>>,
        property_types: vector<String>
    ): CreateTokenWithCreatedCollectionProof {
        CreateTokenWithCreatedCollectionProof {
            creator,
            collection_name,
            name,
            description,
            uri,
            quantity,
            maximum,
            royalty_payee_address,
            royalty_points_numerator,
            royalty_points_denominator,
            mutate_setting,
            property_keys,
            property_values,
            property_types
        }
    }

    public fun create_new_token_without_created_collection_proof(
        creator: address,
        collection_name: String,
        collection_description: String,
        collection_uri: String,
        collection_maximum: u64,
        collection_mutate_setting: vector<bool>,
        token_name: String,
        token_description: String,
        token_uri: String,
        token_quantity: u64,
        token_maximum: u64,
        royalty_payee_address: address,
        royalty_points_numerator: u64,
        royalty_points_denominator: u64,
        token_mutate_setting: vector<bool>,
        property_keys: vector<String>,
        property_values: vector<vector<u8>>,
        property_types: vector<String>
    ): CreateTokenWithoutCreatedCollectionProof {
        CreateTokenWithoutCreatedCollectionProof {
            creator,
            collection_name,
            collection_description,
            collection_uri,
            collection_maximum,
            collection_mutate_setting,
            token_name,
            token_description,
            token_uri,
            token_quantity,
            token_maximum,
            royalty_payee_address,
            royalty_points_numerator,
            royalty_points_denominator,
            token_mutate_setting,
            property_keys,
            property_values,
            property_types
        }
    }

    public fun create_transfer_with_created_token_proof(
        from: address,
        creator: address,
        collection_name: String,
        token_name: String,
        token_property_version: u64,
        to: address,
        quantity: u64
    ): TransferWithCreatedTokenProof {
        TransferWithCreatedTokenProof {
            from,
            creator,
            collection_name,
            token_name,
            token_property_version,
            to,
            quantity
        }
    }

    public fun create_transfer_without_created_collection_proof(
        creator: address,
        collection_name: String,
        collection_description: String,
        collection_uri: String,
        collection_maximum: u64,
        collection_mutate_setting: vector<bool>,
        token_name: String,
        token_description: String,
        token_uri: String,
        token_quantity: u64,
        token_maximum: u64,
        royalty_payee_address: address,
        royalty_points_numerator: u64,
        royalty_points_denominator: u64,
        token_mutate_setting: vector<bool>,
        property_keys: vector<String>,
        property_values: vector<vector<u8>>,
        property_types: vector<String>,
        to: address
    ): TransferWithoutCreatedCollectionProof {
        TransferWithoutCreatedCollectionProof {
            creator,
            collection_name,
            collection_description,
            collection_uri,
            collection_maximum,
            collection_mutate_setting,
            token_name,
            token_description,
            token_uri,
            token_quantity,
            token_maximum,
            royalty_payee_address,
            royalty_points_numerator,
            royalty_points_denominator,
            token_mutate_setting,
            property_keys,
            property_values,
            property_types,
            to
        }
    }

    public fun create_transfer_with_created_collection_proof(
        creator: address,
        collection_name: String,
        token_name: String,
        token_description: String,
        token_uri: String,
        token_quantity: u64,
        token_maximum: u64,
        royalty_payee_address: address,
        royalty_points_numerator: u64,
        royalty_points_denominator: u64,
        token_mutate_setting: vector<bool>,
        property_keys: vector<String>,
        property_values: vector<vector<u8>>,
        property_types: vector<String>,
        to: address
    ): TransferWithCreatedCollectionProof {
        TransferWithCreatedCollectionProof {
            creator,
            collection_name,
            token_name,
            token_description,
            token_uri,
            token_quantity,
            token_maximum,
            royalty_payee_address,
            royalty_points_numerator,
            royalty_points_denominator,
            token_mutate_setting,
            property_keys,
            property_values,
            property_types,
            to
        }
    }

    public fun create_listing_token_fixed_price_proof(
        owner: address,
        listing_id: String,
        creator: address,
        collection_name: String,
        token_name: String,
        property_version: u64,
        quantity: u64,
        price: u64,
        duration: u64
    ): ListingTokenFixedPriceProof {
        ListingTokenFixedPriceProof {
            owner,
            listing_id,
            creator,
            collection_name,
            token_name,
            property_version,
            quantity,
            price,
            duration
        }
    }

    public fun create_listing_token_for_bid_proof(
        owner: address,
        listing_id: String,
        creator: address,
        collection_name: String,
        token_name: String,
        property_version: u64,
        quantity: u64,
        duration: u64
    ): ListingTokenForBidProof {
        ListingTokenForBidProof {
            owner,
            listing_id,
            creator,
            collection_name,
            token_name,
            property_version,
            quantity,
            duration
        }
    }

    public fun create_buy_now_token_proof(
        buyer: address,
        listing_id: String
    ): BuyNowTokenProof {
        BuyNowTokenProof {
            buyer,
            listing_id
        }
    }

    public fun create_edit_price_listing_proof(
        seller: address,
        listing_id: String,
        new_price: u64
    ): EditPriceListingProof {
        EditPriceListingProof {
            seller,
            listing_id,
            new_price
        }
    }

    public fun create_cancel_listing_proof(seller: address, listing_id: String): CancelListingProof {
        CancelListingProof {
            seller,
            listing_id
        }
    }

    public fun create_cancel_all_listing_proof(seller: address): CancelAllListingProof {
        CancelAllListingProof {
            seller
        }
    }

    public fun create_offer_token_proof(
        buyer: address,
        offer_id: String,
        creator: address,
        collection_name: String,
        token_name: String,
        property_version: u64,
        quantity: u64,
        price: u64,
        duration: u64
    ): OfferTokenProof {
        OfferTokenProof {
            buyer,
            offer_id,
            creator,
            collection_name,
            token_name,
            property_version,
            quantity,
            price,
            duration
        }
    }

    public fun create_cancel_offer_proof(buyer: address, offer_id: String): CancelOfferProof {
        CancelOfferProof {
            buyer,
            offer_id
        }
    }

    public fun create_cancel_all_offer_proof(buyer: address): CancelAllOfferProof {
        CancelAllOfferProof {
            buyer
        }
    }

    public fun create_accept_offers_with_created_token_and_listing_proof(
        seller: address,
        offer_id: String,
        listing_ids: vector<String>
    ): AcceptOffersWithCreatedTokenAndListingProof {
        AcceptOffersWithCreatedTokenAndListingProof {
            seller,
            offer_id,
            listing_ids
        }
    }

    public fun create_accept_offers_without_created_collection_and_listing_proof(
        seller: address,
        collection_name: String,
        collection_description: String,
        collection_uri: String,
        collection_maximum: u64,
        collection_mutate_setting: vector<bool>,
        token_name: String,
        token_description: String,
        token_uri: String,
        token_quantity: u64,
        token_maximum: u64,
        royalty_payee_address: address,
        royalty_points_numerator: u64,
        royalty_points_denominator: u64,
        token_mutate_setting: vector<bool>,
        property_keys: vector<String>,
        property_values: vector<vector<u8>>,
        property_types: vector<String>,
        offer_id: String,
        listing_ids: vector<String>,
        start_times: vector<u64>,
        end_times: vector<u64>,
        quantities: vector<u64>,
        prices: vector<u64>
    ): AcceptOffersWithoutCreatedCollectionAndListingProof {
        AcceptOffersWithoutCreatedCollectionAndListingProof {
            seller,
            collection_name,
            collection_description,
            collection_uri,
            collection_maximum,
            collection_mutate_setting,
            token_name,
            token_description,
            token_uri,
            token_quantity,
            token_maximum,
            royalty_payee_address,
            royalty_points_numerator,
            royalty_points_denominator,
            token_mutate_setting,
            property_keys,
            property_values,
            property_types,
            offer_id,
            listing_ids,
            start_times,
            end_times,
            quantities,
            prices
        }
    }

    public fun create_accept_offers_without_created_token_and_listing_proof(
        seller: address,
        collection_name: String,
        name: String,
        description: String,
        uri: String,
        quantity: u64,
        maximum: u64,
        royalty_payee_address: address,
        royalty_points_numerator: u64,
        royalty_points_denominator: u64,
        mutate_setting: vector<bool>,
        property_keys: vector<String>,
        property_values: vector<vector<u8>>,
        property_types: vector<String>,
        offer_id: String,
        listing_ids: vector<String>,
        start_times: vector<u64>,
        end_times: vector<u64>,
        quantities: vector<u64>,
        prices: vector<u64>
    ): AcceptOffersWithoutCreatedTokenAndListingProof {
        AcceptOffersWithoutCreatedTokenAndListingProof {
            seller,
            collection_name,
            name,
            description,
            uri,
            quantity,
            maximum,
            royalty_payee_address,
            royalty_points_numerator,
            royalty_points_denominator,
            mutate_setting,
            property_keys,
            property_values,
            property_types,
            offer_id,
            listing_ids,
            start_times,
            end_times,
            quantities,
            prices
        }
    }

    public fun create_accept_offers_with_created_token_and_without_listing_proof(
        seller: address,
        collection_name: String,
        name: String,
        property_version: u64,
        offer_id: String,
        listing_ids: vector<String>,
        start_times: vector<u64>,
        end_times: vector<u64>,
        quantities: vector<u64>,
        prices: vector<u64>
    ): AcceptOffersWithCreatedTokenAndWithoutListingProof {
        AcceptOffersWithCreatedTokenAndWithoutListingProof {
            seller,
            collection_name,
            name,
            property_version,
            offer_id,
            listing_ids,
            start_times,
            end_times,
            quantities,
            prices
        }
    }

    public fun create_claim_listing_proof(seller: address, listing_id: String): ClaimListingProof {
        ClaimListingProof {
            seller,
            listing_id
        }
    }

    public fun create_claim_all_listing_proof(seller: address): ClaimAllListingProof {
        ClaimAllListingProof {
            seller
        }
    }

    public fun create_claim_offering_proof(buyer: address, offer_id: String): ClaimOfferingProof {
        ClaimOfferingProof {
            buyer,
            offer_id
        }
    }

    public fun create_claim_all_offering_proof(buyer: address): ClaimAllOfferingProof {
        ClaimAllOfferingProof {
            buyer
        }
    }

    public fun create_new_collection_event(
        creator: address,
        name: String,
        description: String,
        uri: String,
        maximum: u64,
        mutability_config: CollectionMutabilityConfig,
        timestamp: u64
    ): CreateCollectionEvent {
        CreateCollectionEvent {
            creator,
            name,
            description,
            uri,
            maximum,
            mutability_config,
            timestamp
        }
    }

    public fun create_new_token_event(
        creator: address,
        collection_name: String,
        name: String,
        description: String,
        uri: String,
        royalty_payee_address: address,
        royalty_points_numerator: u64,
        royalty_points_denominator: u64,
        mutability_config: TokenMutabilityConfig,
        timestamp: u64
    ): CreateTokenEvent {
        CreateTokenEvent {
            creator,
            collection_name,
            name,
            description,
            uri,
            royalty_payee_address,
            royalty_points_numerator,
            royalty_points_denominator,
            mutability_config,
            timestamp
        }
    }

    public fun create_listing_event(
        listing_id: String,
        token_id: TokenId,
        seller: address,
        quantity: u64,
        price: Option<u64>,
        start_time: u64,
        end_time: u64
    ): ListingEvent {
        ListingEvent {
            listing_id,
            token_id,
            seller,
            quantity,
            price,
            start_time,
            end_time
        }
    }

    public fun create_edit_price_listing_event(
        listing_id: String,
        old_price: u64,
        new_price: u64,
        timestamp: u64
    ): EditPriceListingEvent {
        EditPriceListingEvent {
            listing_id,
            old_price,
            new_price,
            timestamp
        }
    }

    public fun create_cancel_listing_event(
        listing_id: String,
        timestamp: u64
    ): CancelListingEvent {
        CancelListingEvent {
            listing_id,
            timestamp
        }
    }

    public fun create_cancel_all_listing_event(
        seller: address,
        listing_ids: vector<String>,
        timestamp: u64
    ): CancelAllListingEvent {
        CancelAllListingEvent {
            seller,
            listing_ids,
            timestamp
        }
    }

    public fun create_offer_event(
        offer_id: String,
        token_id: TokenId,
        buyer: address,
        quantity: u64,
        price: u64,
        start_time: u64,
        end_time: u64
    ): OfferEvent {
        OfferEvent {
            offer_id,
            token_id,
            buyer,
            quantity,
            price,
            start_time,
            end_time
        }
    }

    public fun create_cancel_offer_event(
        offer_id: String,
        timestamp: u64
    ): CancelOfferEvent {
        CancelOfferEvent {
            offer_id,
            timestamp
        }
    }

    public fun create_cancel_all_offer_event(
        buyer: address,
        offer_ids: vector<String>,
        timestamp: u64
    ): CancelAllOfferEvent {
        CancelAllOfferEvent {
            buyer,
            offer_ids,
            timestamp
        }
    }

    public fun create_transfer_event(
        token_id: TokenId,
        from: address,
        to: address,
        quantity: u64,
        timestamp: u64
    ): TransferEvent {
        TransferEvent {
            token_id,
            from,
            to,
            quantity,
            timestamp
        }
    }

    public fun create_buy_now_event(
        listing_id: String,
        seller: address,
        buyer: address,
        quantity: u64,
        price: u64,
        timestamp: u64
    ): BuyNowEvent {
        BuyNowEvent {
            listing_id,
            seller,
            buyer,
            quantity,
            price,
            timestamp
        }
    }

    public fun create_accept_offer_event(
        seller: address,
        buyer: address,
        listing_ids: vector<String>,
        offer_id: String,
        quantity: u64,
        price: u64,
        timestamp: u64
    ): AcceptOfferEvent {
        AcceptOfferEvent {
            seller,
            buyer,
            listing_ids,
            offer_id,
            quantity,
            price,
            timestamp
        }
    }

    public fun create_claim_listing_event(
        listing_id: String,
        quantity: u64,
        timestamp: u64
    ): ClaimListingEvent {
        ClaimListingEvent {
            listing_id,
            quantity,
            timestamp
        }
    }

    public fun create_claim_all_listing_event(
        seller: address,
        listing_ids: vector<String>,
        timestamp: u64
    ): ClaimAllListingEvent {
        ClaimAllListingEvent {
            seller,
            listing_ids,
            timestamp
        }
    }

    public fun create_claim_offer_event(
        offer_id: String,
        quantity: u64,
        timestamp: u64
    ): ClaimOfferEvent {
        ClaimOfferEvent {
            offer_id,
            quantity,
            timestamp
        }
    }

    public fun create_claim_all_offer_event(
        buyer: address,
        offer_ids: vector<String>,
        timestamp: u64
    ): ClaimAllOfferEvent {
        ClaimAllOfferEvent {
            buyer,
            offer_ids,
            timestamp
        }
    }
}
