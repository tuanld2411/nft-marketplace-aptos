module marketplace::nft {
    use std::error;
    use std::vector;
    use std::signer;
    use std::string::String;
    use std::option::{Self, Option};
    use aptos_framework::timestamp;
    use aptos_std::table::{Self, Table};
    use aptos_token::token::{Self, TokenId};

    struct Nft has key {
        exchange_token_ids: vector<TokenId>,
        exchange_tokens: Table<TokenId, ExchangeToken>,
        exchange_collection_ids: vector<CollectionId>,
        exchange_collections: Table<CollectionId, ExchangeCollection>,
        created_token_ids: vector<TokenId>,
        created_tokens: Table<TokenId, CreatedTokenData>,
        created_collection_ids: vector<CollectionId>,
        created_collections: Table<CollectionId, CreatedCollectionData>,
        user_by_listing_id: Table<String, address>,
        user_by_offer_id: Table<String, address>
    }

    struct ExchangeToken has drop, store {
        token_id: TokenId,
        listing_ids: vector<String>,
        offer_ids: vector<String>,
        last_sale_price: Option<u64>,
    }

    struct ExchangeCollection has drop, store {
        volume: u64,
        token_ids: vector<TokenId>
    }

    struct CreatedTokenData has drop, store {
        created_at: u64
    }

    struct CollectionId has copy, drop, store {
        creator: address,
        collection: String
    }

    struct CreatedCollectionData has drop, store {
        created_at: u64
    }

    /// The collection is already existed
    const ENFT_COLLECTION_IS_ALREADY_EXISTED: u64 = 0;

    /// The token is already existed
    const ENFT_TOKEN_IS_ALREADY_EXISTED: u64 = 1;

    /// The listing is already existed
    const ENFT_LISTING_IS_EXISTED: u64 = 2;

    /// The token is not found
    const ENFT_TOKEN_NOT_FOUND: u64 = 3;

    /// The listing is not found
    const ENFT_LISTING_NOT_FOUND: u64 = 4;

    /// The collection is not found
    const ENFT_COLLECTION_NOT_FOUND: u64 = 5;

    /// The user is not authorized
    const ENOT_AUTHORIZED: u64 = 6;

    /// The offer is already existed
    const ENFT_OFFER_IS_EXISTED: u64 = 7;

    /// The offer is not found
    const ENFT_OFFER_IS_NOT_FOUND: u64 = 8;

    public fun initialize_nft(account: &signer) {
        if(!exists<Nft>(signer::address_of(account))) {
            move_to(
                account,
                Nft {
                    exchange_token_ids: vector::empty<TokenId>(),
                    exchange_tokens: table::new(),
                    exchange_collection_ids: vector::empty<CollectionId>(),
                    exchange_collections: table::new(),
                    created_token_ids: vector::empty<TokenId>(),
                    created_tokens: table::new(),
                    created_collection_ids: vector::empty<CollectionId>(),
                    created_collections: table::new(),
                    user_by_listing_id: table::new(),
                    user_by_offer_id: table::new()
                }
            );
        }
    }

    public fun create_collection(creator: address, collection_name: String) acquires Nft {
        let collection_id = create_collection_id_raw(creator, collection_name);
        let nft = borrow_global_mut<Nft>(@marketplace);
        assert!(!vector::contains(&nft.created_collection_ids, &collection_id), error::invalid_argument(ENFT_COLLECTION_IS_ALREADY_EXISTED));
        assert!(!table::contains(&nft.created_collections, collection_id), error::invalid_argument(ENFT_COLLECTION_IS_ALREADY_EXISTED));
        vector::push_back(&mut nft.created_collection_ids, collection_id);
        table::add(&mut nft.created_collections, collection_id, create_collection_data());
    }

    public fun create_token(
        creator: address,
        collection_name: String,
        name: String,
        property_version: u64
    ) acquires Nft {
        let token_id = token::create_token_id_raw(creator, collection_name, name, property_version);
        let nft = borrow_global_mut<Nft>(@marketplace);
        assert!(!vector::contains(&nft.created_token_ids, &token_id), error::invalid_argument(ENFT_TOKEN_IS_ALREADY_EXISTED));
        assert!(!table::contains(&nft.created_tokens, token_id), error::invalid_argument(ENFT_TOKEN_IS_ALREADY_EXISTED));
        vector::push_back(&mut nft.created_token_ids, token_id);
        table::add(&mut nft.created_tokens, token_id, create_token_data());
    }

    public fun checking_listing_id(
        seller_addr: address,
        listing_id: String
    ) acquires Nft {
        let nft = borrow_global<Nft>(@marketplace);
        assert!(table::contains(&nft.user_by_listing_id, listing_id), error::invalid_argument(ENFT_LISTING_NOT_FOUND));
        assert!(seller_addr == *table::borrow(&nft.user_by_listing_id, listing_id), error::permission_denied(ENOT_AUTHORIZED));
    }

    public fun checking_offer_id(
        buyer_addr: address,
        offer_id: String
    ) acquires Nft {
        let nft = borrow_global<Nft>(@marketplace);
        assert!(table::contains(&nft.user_by_offer_id, offer_id), error::invalid_argument(ENFT_OFFER_IS_NOT_FOUND));
        assert!(buyer_addr == *table::borrow(&nft.user_by_offer_id, offer_id), error::permission_denied(ENOT_AUTHORIZED));
    }

    public fun listing_token(
        seller_addr: address,
        listing_id: String,
        creator: address,
        collection_name: String,
        name: String,
        property_version: u64
    ) acquires Nft {
        let nft = borrow_global_mut<Nft>(@marketplace);
        assert!(!table::contains(&nft.user_by_listing_id, listing_id), error::already_exists(ENFT_LISTING_IS_EXISTED));
        table::add(&mut nft.user_by_listing_id, listing_id, seller_addr);
        let token_id = token::create_token_id_raw(creator, collection_name, name, property_version);
        if (vector::contains(&nft.exchange_token_ids, &token_id)) {
            assert!(table::contains(&nft.exchange_tokens, token_id), error::invalid_argument(ENFT_TOKEN_NOT_FOUND));
            let exchange_token = table::borrow_mut(&mut nft.exchange_tokens, token_id);
            vector::push_back(&mut exchange_token.listing_ids, listing_id);
        } else {
            assert!(!table::contains(&nft.exchange_tokens, token_id), error::invalid_argument(ENFT_TOKEN_IS_ALREADY_EXISTED));
            vector::push_back(&mut nft.exchange_token_ids, token_id);
            table::add(&mut nft.exchange_tokens, token_id, create_exchange_token_by_listing_id(token_id, listing_id));
        };
        let collection_id = create_collection_id_raw(creator, collection_name);
        if (vector::contains(&nft.exchange_collection_ids, &collection_id)) {
            assert!(table::contains(&nft.exchange_collections, collection_id), error::invalid_argument(ENFT_COLLECTION_NOT_FOUND));
            let exchange_collection = table::borrow_mut(&mut nft.exchange_collections, collection_id);
            if (!vector::contains(&exchange_collection.token_ids, &token_id)) {
                vector::push_back(&mut exchange_collection.token_ids, token_id);
            };
        } else {
            assert!(!table::contains(&nft.exchange_collections, collection_id), error::invalid_argument(ENFT_COLLECTION_IS_ALREADY_EXISTED));
            vector::push_back(&mut nft.exchange_collection_ids, collection_id);
            table::add(&mut nft.exchange_collections, collection_id, create_exchange_collection(token_id));
        };
    }

    public fun cancel_listing(
        seller_addr: address,
        token_id: TokenId,
        listing_id: String
    ) acquires Nft {
        checking_listing_id(seller_addr, listing_id);
        remove_listing_id(token_id, listing_id);
    }

    public fun claim_listing(
        seller_addr: address,
        token_id: TokenId,
        listing_id: String
    ) acquires Nft {
        checking_listing_id(seller_addr, listing_id);
        remove_listing_id(token_id, listing_id);
    }

    public fun offer_token(
        buyer_addr: address,
        offer_id: String,
        creator: address,
        collection_name: String,
        name: String,
        property_version: u64
    ) acquires Nft {
        let nft = borrow_global_mut<Nft>(@marketplace);
        assert!(!table::contains(&nft.user_by_offer_id, offer_id), error::already_exists(ENFT_OFFER_IS_EXISTED));
        table::add(&mut nft.user_by_offer_id, offer_id, buyer_addr);
        let token_id = token::create_token_id_raw(creator, collection_name, name, property_version);
        if (vector::contains(&nft.exchange_token_ids, &token_id)) {
            assert!(table::contains(&nft.exchange_tokens, token_id), error::invalid_argument(ENFT_TOKEN_NOT_FOUND));
            let exchange_token = table::borrow_mut(&mut nft.exchange_tokens, token_id);
            vector::push_back(&mut exchange_token.offer_ids, offer_id);
        } else {
            assert!(!table::contains(&nft.exchange_tokens, token_id), error::invalid_argument(ENFT_TOKEN_IS_ALREADY_EXISTED));
            vector::push_back(&mut nft.exchange_token_ids, token_id);
            table::add(&mut nft.exchange_tokens, token_id, create_exchange_token_by_offer_id(token_id, offer_id));
        };
    }

    public fun cancel_offer(
        buyer_addr: address,
        token_id: TokenId,
        offer_id: String
    ) acquires Nft {
        checking_offer_id(buyer_addr, offer_id);
        remove_offer_id(token_id, offer_id);
    }

    public fun claim_offer(
        buyer_addr: address,
        token_id: TokenId,
        offer_id: String
    ) acquires Nft {
        checking_offer_id(buyer_addr, offer_id);
        remove_offer_id(token_id, offer_id);
    }

    public fun update_sale_info(
        token_id: TokenId,
        amount: u64,
        price: u64
    ) acquires Nft {
        let nft = borrow_global_mut<Nft>(@marketplace);
        let exchange_token = table::borrow_mut(&mut nft.exchange_tokens, token_id);
        exchange_token.last_sale_price = option::some(price);
        let (creator, collection_name, _, _) = token::get_token_id_fields(&token_id);
        let collection_id = create_collection_id_raw(creator, collection_name);
        assert!(vector::contains(&nft.exchange_collection_ids, &collection_id), error::invalid_argument(ENFT_COLLECTION_NOT_FOUND));
        assert!(table::contains(&nft.exchange_collections, collection_id), error::invalid_argument(ENFT_COLLECTION_NOT_FOUND));
        let exchange_collection = table::borrow_mut(&mut nft.exchange_collections, collection_id);
        exchange_collection.volume = exchange_collection.volume + amount * price;
    }

    public fun get_seller_by_listing_id(listing_id: String): address acquires Nft {
        let nft = borrow_global<Nft>(@marketplace);
        assert!(table::contains(&nft.user_by_listing_id, listing_id), error::invalid_argument(ENFT_LISTING_NOT_FOUND));
        *table::borrow(&nft.user_by_listing_id, listing_id)
    }

    public fun get_buyer_by_offer_id(offer_id: String): address acquires Nft {
        let nft = borrow_global<Nft>(@marketplace);
        assert!(table::contains(&nft.user_by_offer_id, offer_id), error::invalid_argument(ENFT_OFFER_IS_NOT_FOUND));
        *table::borrow(&nft.user_by_offer_id, offer_id)
    }

    fun create_collection_id_raw(creator: address, collection: String): CollectionId {
        CollectionId {
            creator,
            collection
        }
    }

    fun create_collection_data(): CreatedCollectionData {
        CreatedCollectionData {
            created_at: timestamp::now_seconds()
        }
    }

    fun create_token_data(): CreatedTokenData {
        CreatedTokenData {
            created_at: timestamp::now_seconds()
        }
    }

    fun create_exchange_token_by_listing_id(token_id: TokenId, listing_id: String): ExchangeToken {
        ExchangeToken {
            token_id,
            listing_ids: vector::singleton<String>(listing_id),
            offer_ids: vector::empty<String>(),
            last_sale_price: option::none(),
        }
    }

    fun create_exchange_token_by_offer_id(token_id: TokenId, offer_id: String): ExchangeToken {
        ExchangeToken {
            token_id,
            listing_ids: vector::empty<String>(),
            offer_ids: vector::singleton<String>(offer_id),
            last_sale_price: option::none(),
        }
    }

    fun create_exchange_collection(token_id: TokenId): ExchangeCollection {
        ExchangeCollection {
            volume: 0,
            token_ids: vector::singleton(token_id)
        }
    }

    fun remove_listing_id(
        token_id: TokenId,
        listing_id: String
    ) acquires Nft {
        let nft = borrow_global_mut<Nft>(@marketplace);
        let (is_found, _) = vector::index_of(&nft.exchange_token_ids, &token_id);
        assert!(is_found, error::invalid_argument(ENFT_TOKEN_NOT_FOUND));
        assert!(table::contains(&nft.exchange_tokens, token_id), error::invalid_argument(ENFT_TOKEN_NOT_FOUND));
        let (creator, collection_name, _, _) = token::get_token_id_fields(&token_id);
        let collection_id = create_collection_id_raw(creator, collection_name);
        let (is_found, _) = vector::index_of(&nft.exchange_collection_ids, &collection_id);
        assert!(is_found, error::invalid_argument(ENFT_COLLECTION_NOT_FOUND));
        assert!(table::contains(&nft.exchange_collections, collection_id), error::invalid_argument(ENFT_COLLECTION_NOT_FOUND));
        let exchange_token = table::borrow_mut(&mut nft.exchange_tokens, token_id);
        let (is_found, listing_idx) = vector::index_of(&exchange_token.listing_ids, &listing_id);
        assert!(is_found, error::invalid_argument(ENFT_LISTING_NOT_FOUND));
        vector::remove(&mut exchange_token.listing_ids, listing_idx);
        // if (vector::length(&exchange_token.listing_ids) == 0) {
        //     let exchange_collection = table::borrow_mut(&mut nft.exchange_collections, collection_id);
        //     let (is_found, token_idx) = vector::index_of(&exchange_collection.token_ids, &token_id);
        //     assert!(is_found, error::invalid_argument(ENFT_TOKEN_NOT_FOUND));
        //     vector::remove(&mut exchange_collection.token_ids, token_idx);
        //     if (vector::length(&exchange_collection.token_ids) == 0) {
        //         vector::remove(&mut nft.exchange_collection_ids, collection_idx);
        //         table::remove(&mut nft.exchange_collections, collection_id);
        //     };
        //     if (vector::length(&exchange_token.offer_ids) == 0) {
        //         vector::remove(&mut nft.exchange_token_ids, token_idx);
        //         table::remove(&mut nft.exchange_tokens, token_id);
        //     };
        // }
    }

    fun remove_offer_id(
        token_id: TokenId,
        offer_id: String
    ) acquires Nft {
        let nft = borrow_global_mut<Nft>(@marketplace);
        let (is_found, _) = vector::index_of(&nft.exchange_token_ids, &token_id);
        assert!(is_found, error::invalid_argument(ENFT_TOKEN_NOT_FOUND));
        assert!(table::contains(&nft.exchange_tokens, token_id), error::invalid_argument(ENFT_TOKEN_NOT_FOUND));
        let exchange_token = table::borrow_mut(&mut nft.exchange_tokens, token_id);
        let (is_found, offer_idx) = vector::index_of(&exchange_token.offer_ids, &offer_id);
        assert!(is_found, error::invalid_argument(ENFT_OFFER_IS_NOT_FOUND));
        vector::remove(&mut exchange_token.offer_ids, offer_idx);
        // if (vector::length(&exchange_token.listing_ids) == 0 && vector::length(&exchange_token.offer_ids) == 0) {
        //     vector::remove(&mut nft.exchange_token_ids, token_idx);
        //     table::remove(&mut nft.exchange_tokens, token_id);
        // }
    }
}
