module marketplace::seller {
    use std::error;
    use std::signer;
    use std::vector;
    use std::string::String;
    use std::option::{Self, Option};
    use aptos_framework::timestamp;
    use aptos_std::table::{Self, Table};
    use aptos_token::token::{Self, TokenId};

    // Store listing records on the owner's account. The owner is everyone, who listed NFTs
    struct ListingRecords has key {
        all_records: Table<String, Listing>,
        records_fixed_price: Table<String, FixedPrice>,
        records_for_bid: Table<String, OpenForBid>,
        all_listing_ids: vector<String>,
        listed_token_ids: vector<TokenId>,
        listing_ids_by_token_id: Table<TokenId, vector<String>>
    }

    struct Listing has drop, store {
        listing_id: String,
        token_id: TokenId,
        seller: address,
        sold_amount: u64,
        quantity: u64,
        price: Option<u64>,
        start_time: u64,
        end_time: u64,
        is_active: bool,
        is_claim: bool
    }

    struct FixedPrice has drop, store {
        listing_id: String,
        buy_now: Option<BuyNow>,
        accept_offers: vector<AcceptOffer>,
    }

    struct BuyNow has drop, store {
        buyer: address,
        quantity: u64,
        price: u64,
        timestamp: u64
    }

    struct AcceptOffer has copy, drop, store {
        offer_id: String,
        buyer: address,
        quantity: u64,
        price: u64,
        timestamp: u64
    }

    struct OpenForBid has drop, store {
        listing_id: String,
        accept_offers: vector<AcceptOffer>
    }

    /// Start time should be less than end time
    const ESTART_TIME_LARGER_THAN_END_TIME: u64 = 0;

    /// Not enough token to list
    const EOWNER_NOT_HAVING_ENOUGH_TOKEN: u64 = 1;

    /// Listing zero token
    const ELISTING_QUANTITY_ZERO: u64 = 2;

    /// Listing price cannot equal zero
    const ELISTING_PRICE_ZERO: u64 = 3;

    /// Listing id was existed
    const ELISTING_ID_EXISTED: u64 = 4;

    /// Listing id is not found
    const ELISTING_ID_NOT_FOUND: u64 = 5;

    /// The price of listing is not fixed
    const ELISTING_MUST_FIXED_PRICE: u64 = 6;

    /// The amount is not enough for buying
    const ELISTING_AMOUNT_UNVAILABLE: u64 = 7;

    /// Not during listing
    const ELISTING_IS_EXPIRED: u64 = 8;

    /// The new price is invalid
    const ELISTING_NEW_PRICE_INVALID: u64 = 9;

    /// The listing is archived
    const ELISTING_IS_ARCHIVED: u64 = 10;

    /// The token id is not existed
    const ETOKEN_ID_IS_NOT_EXISTED: u64 = 11;

    /// Accept offers arguments is invalid
    const EACCEPT_OFFERS_INVALID_ARGUMENTS: u64 = 12;

    /// The listing cannot claim token
    const ELISTING_CANNOT_CLAIM: u64 = 13;

    /// The listing is already claimed
    const ELISTING_ALREADY_CLAIMED: u64 = 14;

    public fun initialize_listing_records(account: &signer) {
        if(!exists<ListingRecords>(signer::address_of(account))) {
            move_to(
                account,
                ListingRecords {
                    all_records: table::new(),
                    records_fixed_price: table::new(),
                    records_for_bid: table::new(),
                    all_listing_ids: vector::empty<String>(),
                    listed_token_ids: vector::empty<TokenId>(),
                    listing_ids_by_token_id: table::new()
                }
            );
        }
    }

    public fun create_listing(
        owner: &signer,
        listing_id: String,
        token_id: TokenId,
        quantity: u64,
        price: Option<u64>,
        start_time: u64,
        end_time: u64
    ): Listing {
        let owner_addr = signer::address_of(owner);
        assert!(start_time < end_time, error::invalid_argument(ESTART_TIME_LARGER_THAN_END_TIME));
        assert!(token::balance_of(owner_addr, token_id) >= quantity, error::invalid_argument(EOWNER_NOT_HAVING_ENOUGH_TOKEN));
        assert!(quantity > 0, error::invalid_argument(ELISTING_QUANTITY_ZERO));
        if (option::is_some(&price)) {
            assert!(*option::borrow(&price) > 0, error::invalid_argument(ELISTING_PRICE_ZERO));
        };
        Listing {
            listing_id,
            token_id,
            seller: owner_addr,
            sold_amount: 0,
            quantity,
            price,
            start_time,
            end_time,
            is_active: true,
            is_claim: false
        }
    }

    public fun create_listing_under_user_account(
        owner: &signer,
        listing_id: String,
        token_id: TokenId,
        quantity: u64,
        price: Option<u64>,
        start_time: u64,
        end_time: u64
    ) acquires ListingRecords {
        let owner_addr = signer::address_of(owner);
        let listing = create_listing(
            owner,
            listing_id,
            token_id,
            quantity,
            price,
            start_time,
            end_time
        );
        initialize_listing_records(owner);
        let listing_records = borrow_global_mut<ListingRecords>(owner_addr);
        assert!(!table::contains(&listing_records.all_records, listing_id), error::invalid_argument(ELISTING_ID_EXISTED));
        table::add(&mut listing_records.all_records, listing_id, listing);
        vector::push_back(&mut listing_records.all_listing_ids, listing_id);
        if (option::is_some(&price)) {
            assert!(!table::contains(&listing_records.records_fixed_price, listing_id), error::invalid_argument(ELISTING_ID_EXISTED));
            table::add(&mut listing_records.records_fixed_price, listing_id, create_fixed_price(listing_id));
        } else {
            assert!(!table::contains(&listing_records.records_for_bid, listing_id), error::invalid_argument(ELISTING_ID_EXISTED));
            table::add(&mut listing_records.records_for_bid, listing_id, create_open_for_bid(listing_id));
        };
        if (vector::contains(&listing_records.listed_token_ids, &token_id)) {
            assert!(table::contains(&listing_records.listing_ids_by_token_id, token_id), error::invalid_argument(ELISTING_ID_NOT_FOUND));
            assert!(!vector::contains(table::borrow(&listing_records.listing_ids_by_token_id, token_id), &listing_id), error::invalid_argument(ELISTING_ID_NOT_FOUND));
            let listing_ids = table::borrow_mut(&mut listing_records.listing_ids_by_token_id, token_id);
            vector::push_back(listing_ids, listing_id);
        } else {
            assert!(!table::contains(&listing_records.listing_ids_by_token_id, token_id), error::invalid_argument(ELISTING_ID_EXISTED));
            vector::push_back(&mut listing_records.listed_token_ids, token_id);
            table::add(&mut listing_records.listing_ids_by_token_id, token_id, vector::singleton(listing_id));
        };
    }

    public fun edit_price_listing(
        seller_addr: address,
        listing_id: String,
        new_price: u64
    ): (TokenId, u64) acquires ListingRecords {
        let listing_records = borrow_global_mut<ListingRecords>(seller_addr);
        assert!(table::contains(&listing_records.all_records, listing_id), error::invalid_argument(ELISTING_ID_NOT_FOUND));
        assert!(table::contains(&listing_records.records_fixed_price, listing_id), error::invalid_argument(ELISTING_ID_NOT_FOUND));
        let listing = table::borrow_mut(&mut listing_records.all_records, listing_id);
        let now = timestamp::now_seconds();
        assert!(listing.start_time < now && now < listing.end_time, error::unavailable(ELISTING_IS_EXPIRED));
        assert!(listing.is_active, error::unavailable(ELISTING_IS_ARCHIVED));
        assert!(option::is_some(&listing.price), error::unavailable(ELISTING_MUST_FIXED_PRICE));
        let old_price = *option::borrow(&listing.price);
        assert!(0 < new_price && new_price < old_price, error::invalid_argument(ELISTING_NEW_PRICE_INVALID));
        listing.price = option::some(new_price);
        (listing.token_id, old_price)
    }

    public fun cancel_listing(seller_addr: address, listing_id: String): (TokenId, u64) acquires ListingRecords {
        let listing_records = borrow_global_mut<ListingRecords>(seller_addr);
        assert!(table::contains(&listing_records.all_records, listing_id), error::invalid_argument(ELISTING_ID_NOT_FOUND));
        remove_listing_from_token_infos(listing_records, listing_id);
        let listing = table::borrow_mut(&mut listing_records.all_records, listing_id);
        assert!(listing.is_active, error::unavailable(ELISTING_IS_ARCHIVED));
        let now = timestamp::now_seconds();
        assert!(listing.start_time < now && now < listing.end_time, error::unavailable(ELISTING_IS_EXPIRED));
        listing.is_active = false;
        (listing.token_id, listing.quantity - listing.sold_amount)
    }

    public fun cancel_all_listing(seller_addr: address): (vector<TokenId>, vector<u64>, vector<String>) acquires ListingRecords {
        let listing_records = borrow_global_mut<ListingRecords>(seller_addr);
        let now = timestamp::now_seconds();
        let token_ids = vector::empty<TokenId>();
        let remain_tokens = vector::empty<u64>();
        let canceled_listing_ids = vector::empty<String>();
        let idx = 0;
        while (idx < vector::length(&listing_records.all_listing_ids)) {
            let listing = table::borrow_mut(
                &mut listing_records.all_records,
                *vector::borrow(&listing_records.all_listing_ids, idx)
            );
            if (listing.is_active && listing.start_time < now && now < listing.end_time) {
                listing.is_active = false;
                vector::push_back(&mut token_ids, listing.token_id);
                vector::push_back(&mut remain_tokens, listing.quantity - listing.sold_amount);
                vector::push_back(&mut canceled_listing_ids, listing.listing_id);
                remove_listing_from_token_infos(listing_records, listing.listing_id);
            };
            idx = idx + 1;
        };
        (token_ids, remain_tokens, canceled_listing_ids)
    }

    public fun claim_listing(seller_addr: address, listing_id: String): (TokenId, u64) acquires ListingRecords {
        let listing_records = borrow_global_mut<ListingRecords>(seller_addr);
        assert!(table::contains(&listing_records.all_records, listing_id), error::invalid_argument(ELISTING_ID_NOT_FOUND));
        remove_listing_from_token_infos(listing_records, listing_id);
        let listing = table::borrow_mut(&mut listing_records.all_records, listing_id);
        assert!(listing.is_active, error::unavailable(ELISTING_IS_ARCHIVED));
        assert!(!listing.is_claim, error::unavailable(ELISTING_ALREADY_CLAIMED));
        let now = timestamp::now_seconds();
        assert!(now > listing.end_time, error::unavailable(ELISTING_CANNOT_CLAIM));
        listing.is_claim = true;
        (listing.token_id, listing.quantity - listing.sold_amount)
    }

    public fun claim_all_listing(seller_addr: address): (vector<TokenId>, vector<u64>, vector<String>) acquires ListingRecords {
        let listing_records = borrow_global_mut<ListingRecords>(seller_addr);
        let now = timestamp::now_seconds();
        let token_ids = vector::empty<TokenId>();
        let remain_tokens = vector::empty<u64>();
        let claimed_listing_ids = vector::empty<String>();
        let idx = 0;
        while (idx < vector::length(&listing_records.all_listing_ids)) {
            let listing = table::borrow_mut(
                &mut listing_records.all_records,
                *vector::borrow(&listing_records.all_listing_ids, idx)
            );
            if (listing.is_active && !listing.is_claim && now > listing.end_time) {
                listing.is_claim = true;
                vector::push_back(&mut token_ids, listing.token_id);
                vector::push_back(&mut remain_tokens, listing.quantity - listing.sold_amount);
                vector::push_back(&mut claimed_listing_ids, listing.listing_id);
                remove_listing_from_token_infos(listing_records, listing.listing_id);
            };
            idx = idx + 1;
        };
        (token_ids, remain_tokens, claimed_listing_ids)
    }

    public fun buy_now_token(
        seller_addr: address,
        buyer_addr: address,
        listing_id: String,
        timestamp: u64
    ): (TokenId, u64, u64) acquires ListingRecords {
        assert!(seller_addr != buyer_addr, error::invalid_argument(ELISTING_ID_NOT_FOUND));
        let listing_records = borrow_global_mut<ListingRecords>(seller_addr);
        assert!(table::contains(&listing_records.all_records, listing_id), error::invalid_argument(ELISTING_ID_NOT_FOUND));
        assert!(table::contains(&listing_records.records_fixed_price, listing_id), error::invalid_argument(ELISTING_ID_NOT_FOUND));
        let listing = table::borrow_mut(&mut listing_records.all_records, listing_id);
        assert!(listing.start_time <= timestamp && timestamp <= listing.end_time, error::invalid_argument(ELISTING_IS_EXPIRED));
        assert!(listing.is_active, error::unavailable(ELISTING_IS_ARCHIVED));
        assert!(option::is_some(&listing.price), error::unavailable(ELISTING_MUST_FIXED_PRICE));
        assert!(listing.sold_amount < listing.quantity, error::invalid_argument(ELISTING_AMOUNT_UNVAILABLE));
        let amount = listing.quantity - listing.sold_amount;
        let fixed_price = table::borrow_mut(&mut listing_records.records_fixed_price, listing_id);
        listing.sold_amount = listing.quantity;
        fixed_price.buy_now = option::some(
            create_buy_now(
                buyer_addr,
                amount,
                *option::borrow(&listing.price),
                timestamp
            )
        );
        (listing.token_id, amount, *option::borrow(&listing.price))
    }

    public fun match_listing_ids_with_offer_id(
        seller_addr: address,
        buyer_addr: address,
        offer_id: String,
        listing_ids: vector<String>,
        amount: u64,
        price: u64
    ) acquires ListingRecords {
        let listing_records = borrow_global_mut<ListingRecords>(seller_addr);
        let listing_ids_amount = vector::length(&listing_ids);
        assert!(listing_ids_amount > 0, error::invalid_argument(EACCEPT_OFFERS_INVALID_ARGUMENTS));
        assert!(amount > 0, error::invalid_argument(EACCEPT_OFFERS_INVALID_ARGUMENTS));
        let now = timestamp::now_seconds();
        let idx = 0;
        while (idx < listing_ids_amount) {
            let listing_id = *vector::borrow(&listing_ids, idx);
            let listing = table::borrow_mut(&mut listing_records.all_records, listing_id);
            assert!(listing.start_time <= now && now <= listing.end_time, error::invalid_argument(ELISTING_IS_EXPIRED));
            assert!(listing.is_active, error::unavailable(ELISTING_IS_ARCHIVED));
            let amount_matching = amount;
            if (listing_ids_amount == 1) {
                assert!(amount < listing.quantity - listing.sold_amount, error::invalid_argument(EACCEPT_OFFERS_INVALID_ARGUMENTS));
                listing.sold_amount = listing.sold_amount + amount;
            } else {
                assert!(listing.quantity > listing.sold_amount, error::invalid_argument(EACCEPT_OFFERS_INVALID_ARGUMENTS));
                amount_matching = listing.quantity - listing.sold_amount;
                listing.sold_amount = listing.quantity;
            };
            let accept_offer = create_accept_offer(offer_id, buyer_addr, amount_matching, price, now);
            if (table::contains(&listing_records.records_fixed_price, listing_id)) {
                let record_fixed_price = table::borrow_mut(&mut listing_records.records_fixed_price, listing_id);
                vector::push_back(&mut record_fixed_price.accept_offers, copy accept_offer);
            };
            if (table::contains(&listing_records.records_for_bid, listing_id)) {
                let record_for_bid = table::borrow_mut(&mut listing_records.records_for_bid, listing_id);
                vector::push_back(&mut record_for_bid.accept_offers, copy accept_offer);
            };
            if (listing.sold_amount == listing.quantity) {
                remove_listing_from_token_infos(listing_records, listing_id);
            };
            idx = idx + 1;
        };
    }

    public fun get_listing_id_fields(seller_addr: address, listing_id: String): (TokenId, u64) acquires ListingRecords {
        let listing_records = borrow_global<ListingRecords>(seller_addr);
        assert!(vector::contains(&listing_records.all_listing_ids, &listing_id), error::invalid_argument(ELISTING_ID_NOT_FOUND));
        assert!(table::contains(&listing_records.all_records, listing_id), error::invalid_argument(ELISTING_ID_NOT_FOUND));
        let listing = table::borrow(&listing_records.all_records, listing_id);
        let amount_available = listing.quantity - listing.sold_amount;
        (listing.token_id, amount_available)
    }

    fun create_fixed_price(listing_id: String): FixedPrice {
        FixedPrice {
            listing_id,
            buy_now: option::none<BuyNow>(),
            accept_offers: vector::empty<AcceptOffer>()
        }
    }

    fun create_open_for_bid(listing_id: String): OpenForBid {
        OpenForBid {
            listing_id,
            accept_offers: vector::empty<AcceptOffer>()
        }
    }

    fun create_buy_now(
        buyer: address,
        quantity: u64,
        price: u64,
        timestamp: u64
    ): BuyNow {
        BuyNow {
            buyer,
            quantity,
            price,
            timestamp
        }
    }

    fun create_accept_offer(
        offer_id: String,
        buyer: address,
        quantity: u64,
        price: u64,
        timestamp: u64
    ): AcceptOffer {
        AcceptOffer {
            offer_id,
            buyer,
            quantity,
            price,
            timestamp
        }
    }

    fun remove_listing_from_token_infos(
        listing_records: &mut ListingRecords,
        listing_id: String
    ) {
        let token_id = table::borrow(&listing_records.all_records, listing_id).token_id;
        let (is_found, token_idx) = vector::index_of(&listing_records.listed_token_ids, &token_id);
        assert!(is_found, error::invalid_argument(ETOKEN_ID_IS_NOT_EXISTED));
        let (is_found, listing_idx) = vector::index_of(table::borrow(&listing_records.listing_ids_by_token_id, token_id), &listing_id);
        let listing_ids = table::borrow_mut(&mut listing_records.listing_ids_by_token_id, token_id);
        assert!(is_found, error::invalid_argument(ELISTING_ID_NOT_FOUND));
        vector::remove(listing_ids, listing_idx);
        if (vector::length(table::borrow(&listing_records.listing_ids_by_token_id, token_id)) == 0) {
            table::remove(&mut listing_records.listing_ids_by_token_id, token_id);
            vector::remove(&mut listing_records.listed_token_ids, token_idx);
        };
    }
}
