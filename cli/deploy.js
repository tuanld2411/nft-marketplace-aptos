const { HexString, TxnBuilderTypes, AptosAccount, BCS } = require("aptos")
const { readFileSync } = require("fs")
const path = require("path")
const yargs = require("yargs")
const { createClient, getAccounts } = require("./common")
require("dotenv").config()

yargs
    .usage("Usage: npm run setup CLIs for marketplace package. For help: npm run cli -h")
    .help("h")
    .alias("h", "help")
    .command(
        "publish",
        "Publish this package",
        (yargs) => {
            yargs.positional("seed", {
                type: "string",
            })
            yargs.positional("node-url", {
                default: process.env.NODE_URL,
                type: "string",
            })
        },
        async (args) => {
            let { seed, nodeUrl } = args
            const client = createClient(nodeUrl)
            const admin = new AptosAccount(HexString.ensure(process.env.DEPLOYER_PRIVATE_KEY).toUint8Array())
            console.log("\x1b[32m%s\x1b[0m", "Publishing...")

            const modulePath = "./"
            const packageMetadata = readFileSync(path.join(modulePath, "build", "MarketPlace", "package-metadata.bcs"))
            const moduleData = readFileSync(
                path.join(modulePath, "build", "MarketPlace", "bytecode_modules", "marketplace_exchange.mv")
            )
            seed = new HexString(seed.toString("hex")).toUint8Array()
            const metadataSerialized = BCS.bcsSerializeBytes(
                new HexString(packageMetadata.toString("hex")).toUint8Array()
            )
            const codeSerializer = new BCS.Serializer()
            BCS.serializeVector(
                [new TxnBuilderTypes.Module(new HexString(moduleData.toString("hex")).toUint8Array())],
                codeSerializer
            )
            const rawTxn = await client.generateTransaction(admin.address(), {
                function: "0x1::resource_account::create_resource_account_and_publish_package",
                type_arguments: [],
                arguments: [seed, metadataSerialized, codeSerializer.getBytes()],
            })
            const bcsTxn = await client.signTransaction(admin, rawTxn)
            const pendingTxn = await client.submitTransaction(bcsTxn)
            await client.waitForTransaction(pendingTxn.hash, { checkSuccess: true })

            console.log("\x1b[32m%s\x1b[0m", "Completed!")
        }
    ).argv
