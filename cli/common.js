const { AptosClient } = require("aptos")
require("dotenv").config()

const APTOS_COIN_TYPE = "0x1::aptos_coin::AptosCoin"

const createClient = (nodeUrl = process.env.NODE_URL) => new AptosClient(nodeUrl)

const getBalance = async (client, accountAddress, coinType = APTOS_COIN_TYPE) => {
    try {
        const resource = await client.getAccountResource(accountAddress, `0x1::coin::CoinStore<${coinType}>`)
        return parseInt(resource["coin"]["value"])
    } catch (_) {
        return 0
    }
}

module.exports = {
    APTOS_COIN_TYPE,
    createClient,
    getBalance,
}
