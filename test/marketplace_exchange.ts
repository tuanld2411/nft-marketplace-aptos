import { AptosAccount, BCS, HexString, TokenClient } from "aptos"
import { assert } from "chai"
import { MarketplaceClient, Sandbox } from "./common"
import {
    AcceptOffersWithCreatedTokenAndListingProof,
    AcceptOffersWithCreatedTokenAndWithoutListingProof,
    AcceptOffersWithoutCreatedCollectionAndListingProof,
    AcceptOffersWithoutCreatedTokenAndListingProof,
    BuyNowTokenProof,
    CancelAllListingProof,
    CancelAllOfferProof,
    CancelListingProof,
    CancelOfferProof,
    ClaimAllListingProof,
    ClaimAllOfferingProof,
    CreateCollectionProof,
    CreateTokenWithCreatedCollectionProof,
    CreateTokenWithoutCreatedCollectionProof,
    EditPriceListingProof,
    ListingTokenProof,
    OfferTokenProof,
    TransferWithCreatedCollectionProof,
    TransferWithCreatedTokenProof,
    TransferWithoutCreatedCollectionProof,
} from "./signature"

const MARKETPLACE = "152945a2f4f9089cd4e9453fa6d4f32d44292881c58f4320b3ce4945d629bf33"

type ConfigResource = {
    type: string
    data: {
        fee_numerator: number | bigint
        fee_denominator: number | bigint
        fee_address: string
        signer_cap: {
            account: string
        }
        payment_coin: {
            account_address: string
            module_name: number[]
            struct_name: number[]
        }
        verifier_pk: {
            vec: { bytes: string }[]
        }
        paused: boolean
        operators: string[]
    }
}

type ExchangeResource = {
    type: string
    data: {
        all_exchange_tokens: {
            handle: string
        }
        all_lists: { handle: string }
        all_offers: { handle: string }
        all_token_ids: {
            token_data_id: { creator: { account: string }; collection: string; name: string }
            property_version: number
        }[]
    }
}

type ListingRecordsResource = {
    type: string
    data: {
        all_listing_ids: number[]
        all_records: {
            handle: string
        }
        records_fixed_price: {
            handle: string
        }
        records_for_bid: {
            handle: string
        }
    }
}

describe("Marketplace exchange", () => {
    let client: MarketplaceClient
    let tokenClient: TokenClient
    const { admin, seller, buyer, payee, verifier } = new Sandbox()
    let random = Math.floor(Math.random() * 10000)
    let now = Math.floor(Date.now() / 1000)
    before(async () => {
        client = new MarketplaceClient(MARKETPLACE)
        tokenClient = new TokenClient(client)
        await client.optInDirectTransfer(buyer, true)
    })

    it("Init exchange config failed because user is not authorized", async () => {
        try {
            const txn = await client.initConfig(seller, 1, 2, payee, verifier.pubKey().hex())
            await client.waitForTransactionWithResult(txn, { checkSuccess: true })
        } catch (error) {
            assert.ok(error.transaction.vm_status.includes("ENOT_AUTHORIZED"))
        }
    })

    // it("Init exchange config failed because arguments is invalid", async () => {
    //     try {
    //         const txn = await client.initConfig(admin, 2, 1, payee, verifier.pubKey().hex())
    //         await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    //     } catch (error) {
    //         assert.ok(error.transaction.vm_status.includes("EINVALID_FEE_NUMERATOR_DENOMINATOR"))
    //     }
    // })

    it("Init exchange config should be right", async () => {
        const txn = await client.initConfig(admin, 1, 2, payee, verifier.pubKey().hex())
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    })

    // it("Add operator should be right", async () => {
    //     const txn = await client.addOperator(admin, seller.address().toString())
    //     await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    //     const {
    //         data: { operators },
    //     } = (await client.getAccountResource(
    //         MARKETPLACE,
    //         `0x${MARKETPLACE}::marketplace_exchange::Config`
    //     )) as ConfigResource
    //     assert.ok(operators.includes(seller.address().toString()))
    // })

    // it("Add operator should be failed because the operator is already existed", async () => {
    //     try {
    //         const txn = await client.addOperator(admin, seller.address().toString())
    //         await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    //     } catch (error) {
    //         assert.ok(error.transaction.vm_status.includes("EELEMENT_IS_EXISTED"))
    //     }
    // })

    // it("Remove operator should be right", async () => {
    //     const txn = await client.removeOperator(admin, seller.address().toString())
    //     await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    //     const {
    //         data: { operators },
    //     } = (await client.getAccountResource(
    //         MARKETPLACE,
    //         `0x${MARKETPLACE}::marketplace_exchange::Config`
    //     )) as ConfigResource
    //     assert.ok(!operators.includes(seller.address().toString()))
    // })

    // it("Remove operator should be failed because the operator is not existed", async () => {
    //     try {
    //         const txn = await client.removeOperator(admin, seller.address().toString())
    //         await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    //     } catch (error) {
    //         assert.ok(error.transaction.vm_status.includes("EELEMENT_IS_NOT_EXISTED"))
    //     }
    // })

    // it("Set fee parameters should be right", async () => {
    //     let txn = await client.setFee(admin, 1, 3, seller.address().toString())
    //     await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    //     txn = await client.setFee(admin, 1, 2, payee)
    //     await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    // })

    it("Set verifier public key should be right", async () => {
        let txn = await client.setVerifierPk(admin, seller.pubKey().toString())
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })
        txn = await client.setVerifierPk(admin, verifier.pubKey().hex())
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    })

    it("Create collection should be right", async () => {
        const name = `collection ${random}`
        const description = `description ${random}`
        const uri = `uri ${random}`
        const maximum = 100
        const mutateSetting = [false, false, false]
        const data = new CreateCollectionProof(
            MARKETPLACE,
            seller.address().hex(),
            name,
            description,
            uri,
            maximum,
            mutateSetting
        )
        const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
        let txn = await client.createCollection(seller, name, description, uri, maximum, mutateSetting, signature)
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    })

    // it("Create collection should be failed because the collection is existed", async () => {
    //     const name = `collection ${random}`
    //     const description = `description ${random}`
    //     const uri = ""
    //     const maximum = 100
    //     const mutateSetting = [false, false, false]
    //     const data = new CreateCollectionProof(
    //         MARKETPLACE,
    //         seller.address().hex(),
    //         name,
    //         description,
    //         uri,
    //         maximum,
    //         mutateSetting
    //     )
    //     const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
    //     let txn = await client.createCollection(seller, name, description, uri, maximum, mutateSetting, signature)
    //     try {
    //         await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    //     } catch (error) {
    //         assert.ok(error.transaction.vm_status.includes("ECOLLECTION_ALREADY_EXISTS"))
    //     }
    // })

    it("Create token with created collection should be right", async () => {
        const collectionName = `collection ${random}`
        const name = `token ${random}`
        const description = "token description"
        const uri = "token uri"
        const quantity = 1000
        const maximum = 1000
        const royaltyPayeeAddress = admin.address().hex()
        const royaltyPointsNumerator = 1
        const royaltyPointsDenominator = 2
        const mutateSetting = [false, false, false, false, false]
        const propertyKeys = ["Property", "Stat"]
        let propertyValues = ["Hello", 0].map((value) => {
            const serializer = new BCS.Serializer()
            typeof value === "number" ? serializer.serializeU64(value) : serializer.serializeStr(value)
            return Array.from(serializer.getBytes())
        })
        const propertyTypes = ["string", "number"]
        const data = new CreateTokenWithCreatedCollectionProof(
            MARKETPLACE,
            seller.address().hex(),
            collectionName,
            name,
            description,
            uri,
            quantity,
            maximum,
            royaltyPayeeAddress,
            royaltyPointsNumerator,
            royaltyPointsDenominator,
            mutateSetting,
            propertyKeys,
            propertyValues,
            propertyTypes
        )
        const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
        let txn = await client.createTokenWithCreatedCollection(
            seller,
            collectionName,
            name,
            description,
            uri,
            quantity,
            maximum,
            royaltyPayeeAddress,
            royaltyPointsNumerator,
            royaltyPointsDenominator,
            mutateSetting,
            propertyKeys,
            propertyValues,
            propertyTypes,
            signature
        )
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    })

    it("Create token without created collection should be right", async () => {
        const collectionName = `collection ${random} ${random}`
        const collectionDescription = "collection description"
        const collectionUri = "collection uri"
        const collectionMaximum = 10
        const collectionMutateSetting = [false, false, false]
        const tokenName = `token ${random}`
        const tokenDescription = "token description"
        const tokenUri = "token uri"
        const tokenQuantity = 100
        const tokenMaximum = 100
        const royaltyPayeeAddress = admin.address().hex()
        const royaltyPointsNumerator = 1
        const royaltyPointsDenominator = 2
        const tokenMutateSetting = [false, false, false, false, false]
        const propertyKeys = ["Property", "Stats"]
        let propertyValues = ["Hello", 0].map((value) => {
            const serializer = new BCS.Serializer()
            typeof value === "number" ? serializer.serializeU64(value) : serializer.serializeStr(value)
            return Array.from(serializer.getBytes())
        })
        const propertyTypes = ["string", "number"]
        const data = new CreateTokenWithoutCreatedCollectionProof(
            MARKETPLACE,
            seller.address().hex(),
            collectionName,
            collectionDescription,
            collectionUri,
            collectionMaximum,
            collectionMutateSetting,
            tokenName,
            tokenDescription,
            tokenUri,
            tokenQuantity,
            tokenMaximum,
            royaltyPayeeAddress,
            royaltyPointsNumerator,
            royaltyPointsDenominator,
            tokenMutateSetting,
            propertyKeys,
            propertyValues,
            propertyTypes
        )
        const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
        const txn = await client.createTokenWithoutCreatedCollection(
            seller,
            collectionName,
            collectionDescription,
            collectionUri,
            collectionMaximum,
            collectionMutateSetting,
            tokenName,
            tokenDescription,
            tokenUri,
            tokenQuantity,
            tokenMaximum,
            royaltyPayeeAddress,
            royaltyPointsNumerator,
            royaltyPointsDenominator,
            tokenMutateSetting,
            propertyKeys,
            propertyValues,
            propertyTypes,
            signature
        )
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    })

    it("Listing token fixed price", async () => {
        let listingId = "1"
        const collectionName = `collection ${random}`
        const tokenName = `token ${random}`
        const propertyVersion = 0
        const quantity = 100
        const price = 1000
        const duration = 1000
        let data = new ListingTokenProof(
            MARKETPLACE,
            seller.address().hex(),
            listingId,
            seller.address().hex(),
            collectionName,
            tokenName,
            propertyVersion,
            quantity,
            price,
            duration
        )
        let signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
        let txn = await client.listingToken(
            seller,
            listingId,
            seller.address().hex(),
            collectionName,
            tokenName,
            propertyVersion,
            quantity,
            price,
            duration,
            signature
        )
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })
        listingId = "3"
        data = new ListingTokenProof(
            MARKETPLACE,
            seller.address().hex(),
            listingId,
            seller.address().hex(),
            collectionName,
            tokenName,
            propertyVersion,
            quantity,
            price,
            duration
        )
        signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
        txn = await client.listingToken(
            seller,
            listingId,
            seller.address().hex(),
            collectionName,
            tokenName,
            propertyVersion,
            quantity,
            price,
            duration,
            signature
        )
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    })

    it("Listing token opening for bid", async () => {
        const listingId = "2"
        const collectionName = `collection ${random}`
        const tokenName = `token ${random}`
        const propertyVersion = 0
        const quantity = 100
        const duration = 1000
        const data = new ListingTokenProof(
            MARKETPLACE,
            seller.address().hex(),
            listingId,
            seller.address().hex(),
            collectionName,
            tokenName,
            propertyVersion,
            quantity,
            null,
            duration
        )
        const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
        const txn = await client.listingToken(
            seller,
            listingId,
            seller.address().hex(),
            collectionName,
            tokenName,
            propertyVersion,
            quantity,
            null,
            duration,
            signature
        )
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    })

    // it("Edit price of listing should be right", async () => {
    //     const listingId = "1"
    //     const newPrice = 999
    //     const data = new EditPriceListingProof(MARKETPLACE, seller.address().hex(), listingId, newPrice)
    //     const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
    //     const txn = await client.editPriceListing(seller, listingId, newPrice, signature)
    //     await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    // })

    // it("Edit price of listing should be failed", async() => {
    //     let listingId = "1"
    //     let newPrice = 1000
    //     let data = new EditPriceListingProof(MARKETPLACE, seller.address().hex(), listingId, newPrice)
    //     let signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
    //     let txn = await client.editPriceListing(seller, listingId, newPrice, signature)
    //     try {
    //         await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    //     } catch (error) {
    //         assert.ok(error.transaction.vm_status.includes("ELISTING_NEW_PRICE_INVALID"))
    //     }
    //     try {
    //         listingId = "ghk"
    //         newPrice = 1000
    //         data = new EditPriceListingProof(MARKETPLACE, seller.address().hex(), listingId, newPrice)
    //         signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
    //         txn = await client.editPriceListing(seller, listingId, newPrice, signature)
    //     } catch (error) {
    //         assert.ok(error.transaction.vm_status.includes("ENFT_LISTING_NOT_FOUND"))
    //     }
    //     try {
    //         listingId = "1"
    //         newPrice = 1000
    //         data = new EditPriceListingProof(MARKETPLACE, verifier.address().hex(), listingId, newPrice)
    //         signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
    //         txn = await client.editPriceListing(verifier, listingId, newPrice, signature)
    //     } catch (error) {
    //         assert.ok(error.transaction.vm_status.includes("ENOT_AUTHORIZED"))
    //     }
    //     try {
    //         listingId = "2"
    //         newPrice = 1000
    //         data = new EditPriceListingProof(MARKETPLACE, seller.address().hex(), listingId, newPrice)
    //         signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
    //         txn = await client.editPriceListing(seller, listingId, newPrice, signature)
    //     } catch (error) {
    //         assert.ok(error.transaction.vm_status.includes("ELISTING_NEW_PRICE_INVALID"))
    //     }
    // })

    it("Buy now token should be right", async () => {
        const listingId = "1"
        const data = new BuyNowTokenProof(MARKETPLACE, buyer.address().hex(), listingId)
        const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
        const txn = await client.buyNowToken(buyer, listingId, signature)
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    })

    // it("Buy now token should be failed because the listing is not fixed price", async () => {
    //     const listingId = "2"
    //     const data = new BuyNowTokenProof(MARKETPLACE, buyer.address().hex(), listingId)
    //     const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
    //     const txn = await client.buyNowToken(buyer, listingId, signature)
    //     try {
    //         await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    //     } catch (error) {
    //         assert.ok(error.transaction.vm_status.includes("ELISTING_ID_NOT_FOUND"))
    //     }
    // })

    // it("Cancel listing should be right", async () => {
    //     const listingId = "1"
    //     const data = new CancelListingProof(MARKETPLACE, seller.address().hex(), listingId)
    //     const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
    //     const txn = await client.cancelListing(seller, listingId, signature)
    //     await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    //     const tokenBalance = await tokenClient.getToken(
    //         seller.address().hex(),
    //         `collection ${random}`,
    //         `token ${random}`,
    //         "0"
    //     )
    // })

    // it("Cancel listing should be failed because the listing is archived", async () => {
    //     const listingId = "2"
    //     const data = new CancelListingProof(MARKETPLACE, seller.address().hex(), listingId)
    //     const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
    //     const txn = await client.cancelListing(seller, listingId, signature)
    //     try {
    //         await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    //     } catch (error) {
    //         assert.ok(error.transaction.vm_status.includes("ELISTING_IS_ARCHIVED"))
    //     }
    // })

    // it("Cancel all listing should be right", async () => {
    //     const data = new CancelAllListingProof(MARKETPLACE, seller.address().hex())
    //     const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
    //     const txn = await client.cancelAllListing(seller, signature)
    //     await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    //     const events = await client.getEventsByEventHandle(
    //         MARKETPLACE,
    //         `0x${new HexString(MARKETPLACE).noPrefix()}::marketplace_exchange::Exchange`,
    //         "cancel_all_listing_event"
    //     )
    //     console.log({ events })
    // })

    // it("Query resources by account", async () => {
    //     let listingRecordsResource = (await client.getAccountResource(
    //         seller.address().hex(),
    //         `0x${new HexString(MARKETPLACE).noPrefix()}::marketplace_seller::ListingRecords`
    //     )) as ListingRecordsResource
    //     let exchangeResource = (await client.getAccountResource(
    //         MARKETPLACE,
    //         `0x${new HexString(MARKETPLACE).noPrefix()}::marketplace_exchange::Exchange`
    //     )) as ExchangeResource
    //     console.log(listingRecordsResource.data.all_listing_ids)
    //     console.log(exchangeResource.data.all_token_ids)
    // })

    it("Offer token should be right", async () => {
        const offerId = "1"
        const collectionName = `collection ${random}`
        const tokenName = `token ${random}`
        const propertyVersion = 0
        const quantity = 200
        const price = 1000
        const duration = 1000
        const data = new OfferTokenProof(
            MARKETPLACE,
            buyer.address().hex(),
            offerId,
            seller.address().hex(),
            collectionName,
            tokenName,
            propertyVersion,
            quantity,
            price,
            duration
        )
        const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
        const txn = await client.offerToken(
            buyer,
            offerId,
            seller.address().hex(),
            collectionName,
            tokenName,
            propertyVersion,
            quantity,
            price,
            duration,
            signature
        )
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    })

    it("Offer token with token off-chain", async () => {
        let offerId = "3"
        let collectionName = `_collection ${random}`
        let tokenName = `token ${random}`
        let propertyVersion = 0
        let quantity = 500
        let price = 1000
        let duration = 1000
        let data = new OfferTokenProof(
            MARKETPLACE,
            buyer.address().hex(),
            offerId,
            seller.address().hex(),
            collectionName,
            tokenName,
            propertyVersion,
            quantity,
            price,
            duration
        )
        let signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
        let txn = await client.offerToken(
            buyer,
            offerId,
            seller.address().hex(),
            collectionName,
            tokenName,
            propertyVersion,
            quantity,
            price,
            duration,
            signature
        )
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })

        offerId = "4"
        collectionName = `collection ${random}`
        tokenName = `_token ${random}`
        data = new OfferTokenProof(
            MARKETPLACE,
            buyer.address().hex(),
            offerId,
            seller.address().hex(),
            collectionName,
            tokenName,
            propertyVersion,
            quantity,
            price,
            duration
        )
        signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
        txn = await client.offerToken(
            buyer,
            offerId,
            seller.address().hex(),
            collectionName,
            tokenName,
            propertyVersion,
            quantity,
            price,
            duration,
            signature
        )
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    })

    // it("Cancel offer should be right", async () => {
    //     const offerId = "1"
    //     const data = new CancelOfferProof(MARKETPLACE, buyer.address().hex(), offerId)
    //     const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
    //     const txn = await client.cancelOffer(buyer, offerId, signature)
    //     await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    // })

    // it("Cancel all offer should be right", async () => {
    //     const offerId = "2"
    //     const collectionName = `collection ${random} ${random}`
    //     const tokenName = `token ${random}`
    //     const propertyVersion = 0
    //     const quantity = 100
    //     const price = 1000
    //     const duration = 1000
    //     let data = new OfferTokenProof(
    //         MARKETPLACE,
    //         buyer.address().hex(),
    //         offerId,
    //         seller.address().hex(),
    //         collectionName,
    //         tokenName,
    //         propertyVersion,
    //         quantity,
    //         price,
    //         duration
    //     )
    //     let signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
    //     let txn = await client.offerToken(
    //         buyer,
    //         offerId,
    //         seller.address().hex(),
    //         collectionName,
    //         tokenName,
    //         propertyVersion,
    //         quantity,
    //         price,
    //         duration,
    //         signature
    //     )
    //     await client.waitForTransactionWithResult(txn, { checkSuccess: true })

    //     let dataCancelAllOffer = new CancelAllOfferProof(MARKETPLACE, buyer.address().hex())
    //     signature = dataCancelAllOffer.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
    //     txn = await client.cancelAllOffer(buyer, signature)
    //     await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    // })

    it("Transfer token with created collection should be right", async () => {
        const collectionName = `collection ${random}`
        const tokenName = `token ${random}`
        const tokenPropertyVersion = 0
        const to = buyer.address().hex()
        const quantity = 1
        const data = new TransferWithCreatedTokenProof(
            MARKETPLACE,
            seller.address().hex(),
            seller.address().hex(),
            collectionName,
            tokenName,
            tokenPropertyVersion,
            to,
            quantity
        )
        const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
        let txn = await client.transferWithCreatedToken(
            seller,
            seller.address().hex(),
            collectionName,
            tokenName,
            tokenPropertyVersion,
            to,
            quantity,
            signature
        )
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    })

    it("Transfer token without created collection should be right", async () => {
        const collectionName = `collection ${random} ${random} ${random}`
        const collectionDescription = "collection description"
        const collectionUri = "collection uri"
        const collectionMaximum = 10
        const collectionMutateSetting = [false, false, false]
        const tokenName = `token ${random}`
        const tokenDescription = "token description"
        const tokenUri = "token uri"
        const tokenQuantity = 100
        const tokenMaximum = 100
        const royaltyPayeeAddress = admin.address().hex()
        const royaltyPointsNumerator = 1
        const royaltyPointsDenominator = 2
        const tokenMutateSetting = [false, false, false, false, false]
        const propertyKeys = ["Property", "Stats"]
        let propertyValues = ["Hello", 0].map((value) => {
            const serializer = new BCS.Serializer()
            typeof value === "number" ? serializer.serializeU64(value) : serializer.serializeStr(value)
            return Array.from(serializer.getBytes())
        })
        const propertyTypes = ["string", "number"]
        const to = buyer.address().hex()
        const data = new TransferWithoutCreatedCollectionProof(
            MARKETPLACE,
            seller.address().hex(),
            collectionName,
            collectionDescription,
            collectionUri,
            collectionMaximum,
            collectionMutateSetting,
            tokenName,
            tokenDescription,
            tokenUri,
            tokenQuantity,
            tokenMaximum,
            royaltyPayeeAddress,
            royaltyPointsNumerator,
            royaltyPointsDenominator,
            tokenMutateSetting,
            propertyKeys,
            propertyValues,
            propertyTypes,
            to
        )
        const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
        let txn = await client.transferWithoutCreatedCollection(
            seller,
            collectionName,
            collectionDescription,
            collectionUri,
            collectionMaximum,
            collectionMutateSetting,
            tokenName,
            tokenDescription,
            tokenUri,
            tokenQuantity,
            tokenMaximum,
            royaltyPayeeAddress,
            royaltyPointsNumerator,
            royaltyPointsDenominator,
            tokenMutateSetting,
            propertyKeys,
            propertyValues,
            propertyTypes,
            to,
            signature
        )
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    })

    it("Transfer token with created collection should be right", async () => {
        const collectionName = `collection ${random}`
        const tokenName = `token ${random} new`
        const tokenDescription = "token description"
        const tokenUri = "token uri"
        const tokenQuantity = 100
        const tokenMaximum = 100
        const royaltyPayeeAddress = admin.address().hex()
        const royaltyPointsNumerator = 1
        const royaltyPointsDenominator = 2
        const tokenMutateSetting = [false, false, false, false, false]
        const propertyKeys = ["Property", "Stats"]
        let propertyValues = ["Hello", 0].map((value) => {
            const serializer = new BCS.Serializer()
            typeof value === "number" ? serializer.serializeU64(value) : serializer.serializeStr(value)
            return Array.from(serializer.getBytes())
        })
        const propertyTypes = ["string", "number"]
        const to = buyer.address().hex()
        const data = new TransferWithCreatedCollectionProof(
            MARKETPLACE,
            seller.address().hex(),
            collectionName,
            tokenName,
            tokenDescription,
            tokenUri,
            tokenQuantity,
            tokenMaximum,
            royaltyPayeeAddress,
            royaltyPointsNumerator,
            royaltyPointsDenominator,
            tokenMutateSetting,
            propertyKeys,
            propertyValues,
            propertyTypes,
            to
        )
        const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
        let txn = await client.transferWithCreatedCollection(
            seller,
            collectionName,
            tokenName,
            tokenDescription,
            tokenUri,
            tokenQuantity,
            tokenMaximum,
            royaltyPayeeAddress,
            royaltyPointsNumerator,
            royaltyPointsDenominator,
            tokenMutateSetting,
            propertyKeys,
            propertyValues,
            propertyTypes,
            to,
            signature
        )
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    })

    it("Accept offers with created token and listing on-chain", async () => {
        let orderId = "1"
        let listingIds = ["2", "3"]
        let data = new AcceptOffersWithCreatedTokenAndListingProof(
            MARKETPLACE,
            seller.address().hex(),
            orderId,
            listingIds
        )
        const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
        const txn = await client.acceptOffersWithCreatedTokenAndListing(seller, orderId, listingIds, signature)
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    })

    it("Accept offers without created collection and listing off-chain", async () => {
        const collectionName = `_collection ${random}`
        const collectionDescription = "collection description"
        const collectionUri = "collection uri"
        const collectionMaximum = 10
        const collectionMutateSetting = [false, false, false]
        const tokenName = `token ${random}`
        const tokenDescription = "token description"
        const tokenUri = "token uri"
        const tokenQuantity = 100
        const tokenMaximum = 1000
        const royaltyPayeeAddress = admin.address().hex()
        const royaltyPointsNumerator = 1
        const royaltyPointsDenominator = 2
        const tokenMutateSetting = [false, false, false, false, false]
        const propertyKeys = ["Property", "Stats"]
        let propertyValues = ["Hello", 0].map((value) => {
            const serializer = new BCS.Serializer()
            typeof value === "number" ? serializer.serializeU64(value) : serializer.serializeStr(value)
            return Array.from(serializer.getBytes())
        })
        const propertyTypes = ["string", "number"]
        const offerId = "3"
        const listingIds = ["4", "5"]
        const quantities = [50, 50]
        const startTimes = [now - 24 * 3600, now - 24 * 3600]
        const endTimes = [now + 24 * 3600, now + 24 * 3600]
        const prices = [1000, 0]
        const data = new AcceptOffersWithoutCreatedCollectionAndListingProof(
            MARKETPLACE,
            seller.address().hex(),
            collectionName,
            collectionDescription,
            collectionUri,
            collectionMaximum,
            collectionMutateSetting,
            tokenName,
            tokenDescription,
            tokenUri,
            tokenQuantity,
            tokenMaximum,
            royaltyPayeeAddress,
            royaltyPointsNumerator,
            royaltyPointsDenominator,
            tokenMutateSetting,
            propertyKeys,
            propertyValues,
            propertyTypes,
            offerId,
            listingIds,
            startTimes,
            endTimes,
            quantities,
            prices
        )
        const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
        const txn = await client.acceptOffersWithoutCreatedCollectionAndListing(
            seller,
            collectionName,
            collectionDescription,
            collectionUri,
            collectionMaximum,
            collectionMutateSetting,
            tokenName,
            tokenDescription,
            tokenUri,
            tokenQuantity,
            tokenMaximum,
            royaltyPayeeAddress,
            royaltyPointsNumerator,
            royaltyPointsDenominator,
            tokenMutateSetting,
            propertyKeys,
            propertyValues,
            propertyTypes,
            offerId,
            listingIds,
            startTimes,
            endTimes,
            quantities,
            prices,
            signature
        )
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    })

    it("Accept offers without created token and listing off-chain", async () => {
        const collectionName = `collection ${random}`
        const name = `_token ${random}`
        const description = "token description"
        const uri = "token uri"
        const quantity = 100
        const maximum = 1000
        const royaltyPayeeAddress = admin.address().hex()
        const royaltyPointsNumerator = 1
        const royaltyPointsDenominator = 2
        const mutateSetting = [false, false, false, false, false]
        const propertyKeys = ["Property", "Stat"]
        let propertyValues = ["Hello", 0].map((value) => {
            const serializer = new BCS.Serializer()
            typeof value === "number" ? serializer.serializeU64(value) : serializer.serializeStr(value)
            return Array.from(serializer.getBytes())
        })
        const propertyTypes = ["string", "number"]
        const offerId = "4"
        const listingIds = ["6", "7"]
        const quantities = [50, 50]
        const startTimes = [now - 24 * 3600, now - 24 * 3600]
        const endTimes = [now + 24 * 3600, now + 24 * 3600]
        const prices = [1000, 0]
        const data = new AcceptOffersWithoutCreatedTokenAndListingProof(
            MARKETPLACE,
            seller.address().hex(),
            collectionName,
            name,
            description,
            uri,
            quantity,
            maximum,
            royaltyPayeeAddress,
            royaltyPointsNumerator,
            royaltyPointsDenominator,
            mutateSetting,
            propertyKeys,
            propertyValues,
            propertyTypes,
            offerId,
            listingIds,
            startTimes,
            endTimes,
            quantities,
            prices
        )
        const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
        let txn = await client.acceptOffersWithoutCreatedTokenAndListing(
            seller,
            collectionName,
            name,
            description,
            uri,
            quantity,
            maximum,
            royaltyPayeeAddress,
            royaltyPointsNumerator,
            royaltyPointsDenominator,
            mutateSetting,
            propertyKeys,
            propertyValues,
            propertyTypes,
            offerId,
            listingIds,
            startTimes,
            endTimes,
            quantities,
            prices,
            signature
        )
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    })

    it("Accept offers with created token and listing off-chain", async () => {
        const collectionName = `collection ${random}`
        const name = `token ${random}`
        const propertyVersion = 0
        const offerId = "1"
        const listingIds = ["8", "9"]
        const quantities = [50, 50]
        const startTimes = [now - 24 * 3600, now - 24 * 3600]
        const endTimes = [now + 24 * 3600, now + 24 * 3600]
        const prices = [1000, 0]
        const data = new AcceptOffersWithCreatedTokenAndWithoutListingProof(
            MARKETPLACE,
            seller.address().hex(),
            collectionName,
            name,
            propertyVersion,
            offerId,
            listingIds,
            startTimes,
            endTimes,
            quantities,
            prices
        )
        const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
        let txn = await client.acceptOffersWithCreatedTokenAndWithoutListing(
            seller,
            collectionName,
            name,
            propertyVersion,
            offerId,
            listingIds,
            startTimes,
            endTimes,
            quantities,
            prices,
            signature
        )
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    })

    it("Claim all listing should be right", async () => {
        const data = new ClaimAllListingProof(MARKETPLACE, seller.address().hex())
        const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
        const txn = await client.claimAllListing(seller, signature)
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    })

    it("Claim all offering should be right", async () => {
        const data = new ClaimAllOfferingProof(MARKETPLACE, buyer.address().hex())
        const signature = data.generateSignature(verifier.toPrivateKeyObject().privateKeyHex)
        const txn = await client.claimAllOffer(buyer, signature)
        await client.waitForTransactionWithResult(txn, { checkSuccess: true })
    })
})
