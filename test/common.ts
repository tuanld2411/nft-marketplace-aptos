import { AptosAccount, AptosClient, BCS, HexString } from "aptos"
import { readFileSync } from "fs"
import { load } from "js-yaml"

export const NODE_URL = "https://fullnode.testnet.aptoslabs.com"

export const APTOS_COIN_TYPE = "0x1::aptos_coin::AptosCoin"

type ConfigYaml = {
    profiles: {
        default: {
            private_key: string
        }
        seller: {
            private_key: string
        }
        buyer: {
            private_key: string
        }
        payee: {
            private_key: string
        }
        verifier: {
            private_key: string
        }
    }
}

export const sleep = (second: number) => {
    return new Promise((resolve) => setTimeout(resolve, second * 1000))
}

export class CommonClient extends AptosClient {
    constructor() {
        super(NODE_URL)
    }

    async registerCoin(account: AptosAccount, coinType: string = APTOS_COIN_TYPE): Promise<void> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: "0x1::managed_coin::register",
            type_arguments: [coinType],
            arguments: [],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        await this.waitForTransaction(pendingTxn.hash, { checkSuccess: true })
    }

    async optInDirectTransfer(account: AptosAccount, optIn: boolean): Promise<void> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: "0x3::token::opt_in_direct_transfer",
            type_arguments: [],
            arguments: [optIn],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        await this.waitForTransaction(pendingTxn.hash, { checkSuccess: true })
    }

    async getBalance(accountAddress: HexString, coinType: string = APTOS_COIN_TYPE): Promise<string | number> {
        try {
            const resource = await this.getAccountResource(accountAddress, `0x1::coin::CoinStore<${coinType}>`)

            return parseInt((resource.data as any)["coin"]["value"])
        } catch (_) {
            return 0
        }
    }
}

export class Sandbox {
    admin: AptosAccount
    seller: AptosAccount
    buyer: AptosAccount
    payee: string
    verifier: AptosAccount

    constructor(aptosConfigPath: string = "/home/dds-019/marketplace-contract/.aptos/config.yaml") {
        const phrases = load(readFileSync(aptosConfigPath, "utf-8")) as ConfigYaml
        this.admin = new AptosAccount(HexString.ensure(phrases.profiles.default.private_key).toUint8Array())
        this.seller = new AptosAccount(HexString.ensure(phrases.profiles.seller.private_key).toUint8Array())
        this.buyer = new AptosAccount(HexString.ensure(phrases.profiles.buyer.private_key).toUint8Array())
        this.payee = new AptosAccount(HexString.ensure(phrases.profiles.payee.private_key).toUint8Array())
            .address()
            .hex()
        this.verifier = new AptosAccount(HexString.ensure(phrases.profiles.verifier.private_key).toUint8Array())
    }
}

export class MarketplaceClient extends CommonClient {
    marketplace: HexString
    constructor(marketplace: string) {
        super()
        this.marketplace = new HexString(marketplace)
    }

    async initConfig(
        account: AptosAccount,
        feeNumerator: number | bigint,
        feeDenominator: number | bigint,
        feeAddress: string,
        verifierPk: string
    ): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::init_exchange_config`,
            type_arguments: [],
            arguments: [feeNumerator, feeDenominator, feeAddress, new HexString(verifierPk).toUint8Array()],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async addOperator(account: AptosAccount, operator: string): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::add_operator`,
            type_arguments: [],
            arguments: [operator],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async removeOperator(account: AptosAccount, operator: string): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::remove_operator`,
            type_arguments: [],
            arguments: [operator],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async setFee(
        account: AptosAccount,
        feeNumerator: number | bigint,
        feeDenominator: number | bigint,
        feeAddress: string
    ): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::set_fee`,
            type_arguments: [],
            arguments: [feeNumerator, feeDenominator, feeAddress],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async setPaymentCoin(account: AptosAccount, coinType: string = APTOS_COIN_TYPE): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::set_payment_coin`,
            type_arguments: [coinType],
            arguments: [],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async setVerifierPk(account: AptosAccount, verifierPublicKey: string): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::set_verifier_pk`,
            type_arguments: [],
            arguments: [new HexString(verifierPublicKey).toUint8Array()],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async setPaused(account: AptosAccount, isPaused: boolean): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::set_paused`,
            type_arguments: [],
            arguments: [isPaused],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async createCollection(
        account: AptosAccount,
        name: string,
        description: string,
        uri: string,
        maximum: number | bigint,
        mutateSetting: boolean[],
        signature: string
    ): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::create_collection`,
            type_arguments: [],
            arguments: [name, description, uri, maximum, mutateSetting, new HexString(signature).toUint8Array()],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async createTokenWithCreatedCollection(
        account: AptosAccount,
        collectionName: string,
        name: string,
        description: string,
        uri: string,
        quantity: number | bigint,
        maximum: number | bigint,
        royaltyPayeeAddress: string,
        royaltyPointsNumerator: number | bigint,
        royaltyPointsDenominator: number | bigint,
        mutateSetting: boolean[],
        propertyKeys: string[],
        propertyValues: number[][],
        propertyTypes: string[],
        signature: string
    ): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::create_token_with_created_collection`,
            type_arguments: [],
            arguments: [
                collectionName,
                name,
                description,
                uri,
                quantity,
                maximum,
                new HexString(royaltyPayeeAddress).hex(),
                royaltyPointsNumerator,
                royaltyPointsDenominator,
                mutateSetting,
                propertyKeys,
                propertyValues,
                propertyTypes,
                new HexString(signature).toUint8Array(),
            ],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async createTokenWithoutCreatedCollection(
        account: AptosAccount,
        collectionName: string,
        collectionDescription: string,
        collectionUri: string,
        collectionMaximum: number | bigint,
        collectionMutateSetting: boolean[],
        tokenName: string,
        tokenDescription: string,
        tokenUri: string,
        tokenQuantity: number | bigint,
        tokenMaximum: number | bigint,
        royaltyPayeeAddress: string,
        royaltyPointsNumerator: number | bigint,
        royaltyPointsDenominator: number | bigint,
        tokenMutateSetting: boolean[],
        propertyKeys: string[],
        propertyValues: number[][],
        propertyTypes: string[],
        signature: string
    ): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::create_token_without_created_collection`,
            type_arguments: [],
            arguments: [
                collectionName,
                collectionDescription,
                collectionUri,
                collectionMaximum,
                collectionMutateSetting,
                tokenName,
                tokenDescription,
                tokenUri,
                tokenQuantity,
                tokenMaximum,
                new HexString(royaltyPayeeAddress).hex(),
                royaltyPointsNumerator,
                royaltyPointsDenominator,
                tokenMutateSetting,
                propertyKeys,
                propertyValues,
                propertyTypes,
                new HexString(signature).toUint8Array(),
            ],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async listingToken(
        account: AptosAccount,
        listingId: string,
        creator: string,
        collectionName: string,
        tokenName: string,
        propertyVersion: number | bigint,
        quantity: number | bigint,
        price: number | bigint | null,
        duration: number | bigint,
        signature: string,
        coinType: string = APTOS_COIN_TYPE
    ): Promise<string> {
        const argumentsFixedPrice = [
            listingId,
            creator,
            collectionName,
            tokenName,
            propertyVersion,
            quantity,
            price,
            duration,
            new HexString(signature).toUint8Array(),
        ]
        const argumentsForBid = [
            listingId,
            creator,
            collectionName,
            tokenName,
            propertyVersion,
            quantity,
            duration,
            new HexString(signature).toUint8Array(),
        ]
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::${
                price ? "listing_token_fixed_price" : "listing_token_for_bid"
            }`,
            type_arguments: [coinType],
            arguments: price ? argumentsFixedPrice : argumentsForBid,
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async buyNowToken(
        account: AptosAccount,
        listingId: string,
        signature: string,
        coinType: string = APTOS_COIN_TYPE
    ): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::buy_now_token`,
            type_arguments: [coinType],
            arguments: [listingId, new HexString(signature).toUint8Array()],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async editPriceListing(
        account: AptosAccount,
        listingId: string,
        newPrice: number | bigint,
        signature: string
    ): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::edit_price_listing`,
            type_arguments: [],
            arguments: [listingId, newPrice, new HexString(signature).toUint8Array()],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async cancelListing(account: AptosAccount, listingId: string, signature: string): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::cancel_listing`,
            type_arguments: [],
            arguments: [listingId, new HexString(signature).toUint8Array()],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async cancelAllListing(account: AptosAccount, signature: string): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::cancel_all_listing`,
            type_arguments: [],
            arguments: [new HexString(signature).toUint8Array()],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async offerToken(
        account: AptosAccount,
        offerId: string,
        creator: string,
        collectionName: string,
        tokenName: string,
        propertyVersion: number | bigint,
        quantity: number | bigint,
        price: number | bigint,
        duration: number | bigint,
        signature: string,
        coinType: string = APTOS_COIN_TYPE
    ): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::offer_token`,
            type_arguments: [coinType],
            arguments: [
                offerId,
                creator,
                collectionName,
                tokenName,
                propertyVersion,
                quantity,
                price,
                duration,
                new HexString(signature).toUint8Array(),
            ],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async cancelOffer(
        account: AptosAccount,
        offerId: string,
        signature: string,
        coinType: string = APTOS_COIN_TYPE
    ): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::cancel_offer`,
            type_arguments: [coinType],
            arguments: [offerId, new HexString(signature).toUint8Array()],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async cancelAllOffer(
        account: AptosAccount,
        signature: string,
        coinType: string = APTOS_COIN_TYPE
    ): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::cancel_all_offer`,
            type_arguments: [coinType],
            arguments: [new HexString(signature).toUint8Array()],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async transferWithCreatedToken(
        account: AptosAccount,
        creator: string,
        collectionName: string,
        tokenName: string,
        tokenPropertyVersion: number | bigint,
        to: string,
        quantity: number | bigint,
        signature: string
    ): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::transfer_with_created_token`,
            type_arguments: [],
            arguments: [
                creator,
                collectionName,
                tokenName,
                tokenPropertyVersion,
                to,
                quantity,
                new HexString(signature).toUint8Array(),
            ],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async transferWithoutCreatedCollection(
        account: AptosAccount,
        collectionName: string,
        collectionDescription: string,
        collectionUri: string,
        collectionMaximum: number | bigint,
        collectionMutateSetting: boolean[],
        tokenName: string,
        tokenDescription: string,
        tokenUri: string,
        tokenQuantity: number | bigint,
        tokenMaximum: number | bigint,
        royaltyPayeeAddress: string,
        royaltyPointsNumerator: number | bigint,
        royaltyPointsDenominator: number | bigint,
        tokenMutateSetting: boolean[],
        propertyKeys: string[],
        propertyValues: number[][],
        propertyTypes: string[],
        to: string,
        signature: string
    ): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::transfer_without_created_collection`,
            type_arguments: [],
            arguments: [
                collectionName,
                collectionDescription,
                collectionUri,
                collectionMaximum,
                collectionMutateSetting,
                tokenName,
                tokenDescription,
                tokenUri,
                tokenQuantity,
                tokenMaximum,
                new HexString(royaltyPayeeAddress).hex(),
                royaltyPointsNumerator,
                royaltyPointsDenominator,
                tokenMutateSetting,
                propertyKeys,
                propertyValues,
                propertyTypes,
                new HexString(to).hex(),
                new HexString(signature).toUint8Array(),
            ],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async transferWithCreatedCollection(
        account: AptosAccount,
        collectionName: string,
        tokenName: string,
        tokenDescription: string,
        tokenUri: string,
        tokenQuantity: number | bigint,
        tokenMaximum: number | bigint,
        royaltyPayeeAddress: string,
        royaltyPointsNumerator: number | bigint,
        royaltyPointsDenominator: number | bigint,
        tokenMutateSetting: boolean[],
        propertyKeys: string[],
        propertyValues: number[][],
        propertyTypes: string[],
        to: string,
        signature: string
    ): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::transfer_with_created_collection`,
            type_arguments: [],
            arguments: [
                collectionName,
                tokenName,
                tokenDescription,
                tokenUri,
                tokenQuantity,
                tokenMaximum,
                new HexString(royaltyPayeeAddress).hex(),
                royaltyPointsNumerator,
                royaltyPointsDenominator,
                tokenMutateSetting,
                propertyKeys,
                propertyValues,
                propertyTypes,
                new HexString(to).hex(),
                new HexString(signature).toUint8Array(),
            ],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async acceptOffersWithCreatedTokenAndListing(
        account: AptosAccount,
        orderId: string,
        listingIds: string[],
        signature: string,
        coinType: string = APTOS_COIN_TYPE
    ): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::accept_offers_with_created_token_and_listing`,
            type_arguments: [coinType],
            arguments: [orderId, listingIds, new HexString(signature).toUint8Array()],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async acceptOffersWithoutCreatedCollectionAndListing(
        account: AptosAccount,
        collectionName: string,
        collectionDescription: string,
        collectionUri: string,
        collectionMaximum: number | bigint,
        collectionMutateSetting: boolean[],
        tokenName: string,
        tokenDescription: string,
        tokenUri: string,
        tokenQuantity: number | bigint,
        tokenMaximum: number | bigint,
        royaltyPayeeAddress: string,
        royaltyPointsNumerator: number | bigint,
        royaltyPointsDenominator: number | bigint,
        tokenMutateSetting: boolean[],
        propertyKeys: string[],
        propertyValues: number[][],
        propertyTypes: string[],
        offerId: string,
        listingIds: string[],
        startTimes: (number | bigint)[],
        endTimes: (number | bigint)[],
        quantities: (number | bigint)[],
        prices: (number | bigint)[],
        signature: string,
        coinType: string = APTOS_COIN_TYPE
    ): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::accept_offers_without_created_collection_and_listing`,
            type_arguments: [coinType],
            arguments: [
                collectionName,
                collectionDescription,
                collectionUri,
                collectionMaximum,
                collectionMutateSetting,
                tokenName,
                tokenDescription,
                tokenUri,
                tokenQuantity,
                tokenMaximum,
                new HexString(royaltyPayeeAddress).hex(),
                royaltyPointsNumerator,
                royaltyPointsDenominator,
                tokenMutateSetting,
                propertyKeys,
                propertyValues,
                propertyTypes,
                offerId,
                listingIds,
                startTimes,
                endTimes,
                quantities,
                prices,
                new HexString(signature).toUint8Array(),
            ],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async acceptOffersWithoutCreatedTokenAndListing(
        account: AptosAccount,
        collectionName: string,
        name: string,
        description: string,
        uri: string,
        quantity: number | bigint,
        maximum: number | bigint,
        royaltyPayeeAddress: string,
        royaltyPointsNumerator: number | bigint,
        royaltyPointsDenominator: number | bigint,
        mutateSetting: boolean[],
        propertyKeys: string[],
        propertyValues: number[][],
        propertyTypes: string[],
        offerId: string,
        listingIds: string[],
        startTimes: (number | bigint)[],
        endTimes: (number | bigint)[],
        quantities: (number | bigint)[],
        prices: (number | bigint)[],
        signature: string,
        coinType: string = APTOS_COIN_TYPE
    ): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::accept_offers_without_created_token_and_listing`,
            type_arguments: [coinType],
            arguments: [
                collectionName,
                name,
                description,
                uri,
                quantity,
                maximum,
                new HexString(royaltyPayeeAddress).hex(),
                royaltyPointsNumerator,
                royaltyPointsDenominator,
                mutateSetting,
                propertyKeys,
                propertyValues,
                propertyTypes,
                offerId,
                listingIds,
                startTimes,
                endTimes,
                quantities,
                prices,
                new HexString(signature).toUint8Array(),
            ],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async acceptOffersWithCreatedTokenAndWithoutListing(
        account: AptosAccount,
        collectionName: string,
        name: string,
        propertyVersion: number | bigint,
        offerId: string,
        listingIds: string[],
        startTimes: (number | bigint)[],
        endTimes: (number | bigint)[],
        quantities: (number | bigint)[],
        prices: (number | bigint)[],
        signature: string,
        coinType: string = APTOS_COIN_TYPE
    ): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::accept_offers_with_created_token_and_without_listing`,
            type_arguments: [coinType],
            arguments: [
                collectionName,
                name,
                propertyVersion,
                offerId,
                listingIds,
                startTimes,
                endTimes,
                quantities,
                prices,
                new HexString(signature).toUint8Array(),
            ],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async claimListing(account: AptosAccount, listingId: string, signature: string): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::claim_listing`,
            type_arguments: [],
            arguments: [listingId, new HexString(signature).toUint8Array()],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async claimAllListing(account: AptosAccount, signature: string): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::claim_all_listing`,
            type_arguments: [],
            arguments: [new HexString(signature).toUint8Array()],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async claimOffer(
        account: AptosAccount,
        offerId: string,
        signature: string,
        coinType: string = APTOS_COIN_TYPE
    ): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::claim_offer`,
            type_arguments: [coinType],
            arguments: [offerId, new HexString(signature).toUint8Array()],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }

    async claimAllOffer(account: AptosAccount, signature: string, coinType: string = APTOS_COIN_TYPE): Promise<string> {
        const rawTxn = await this.generateTransaction(account.address(), {
            function: `${this.marketplace.hex()}::exchange_entry::claim_all_offer`,
            type_arguments: [coinType],
            arguments: [new HexString(signature).toUint8Array()],
        })
        const bcsTxn = await this.signTransaction(account, rawTxn)
        const pendingTxn = await this.submitTransaction(bcsTxn)
        return pendingTxn.hash
    }
}
