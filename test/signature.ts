import { AptosAccount, BCS, HexString, TxnBuilderTypes } from "aptos"

class Proof {
    moduleAddress: string
    moduleName: string
    structName: string

    constructor(moduleAddress: string, moduleName: string, structName: string) {
        this.moduleAddress = moduleAddress
        this.moduleName = moduleName
        this.structName = structName
    }

    generateSignature(signer_private_key: string) {
        const message = BCS.bcsToBytes(this)
        const signer = new AptosAccount(HexString.ensure(signer_private_key).toUint8Array())
        const signature = signer.signBuffer(message)
        return signature.noPrefix()
    }

    serialize(serializer: BCS.Serializer) {}
}

export class CreateCollectionProof extends Proof {
    creator: string
    name: string
    description: string
    uri: string
    maximum: number | bigint
    mutateSetting: boolean[]

    constructor(
        moduleAddress: string,
        creator: string,
        name: string,
        description: string,
        uri: string,
        maximum: number,
        mutateSetting: boolean[]
    ) {
        super(moduleAddress, "utils", "CreateCollectionProof")
        this.creator = creator
        this.name = name
        this.description = description
        this.uri = uri
        this.maximum = maximum
        this.mutateSetting = mutateSetting
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.creator).serialize(serializer)
        serializer.serializeStr(this.name)
        serializer.serializeStr(this.description)
        serializer.serializeStr(this.uri)
        serializer.serializeU64(this.maximum)
        serializer.serializeU32AsUleb128(this.mutateSetting.length)
        this.mutateSetting.forEach((setting: boolean) => {
            serializer.serializeBool(setting)
        })
    }
}

export class CreateTokenWithCreatedCollectionProof extends Proof {
    creator: string
    collectionName: string
    name: string
    description: string
    uri: string
    quantity: number | bigint
    maximum: number | bigint
    royaltyPayeeAddress: string
    royaltyPointsNumerator: number | bigint
    royaltyPointsDenominator: number | bigint
    mutateSetting: boolean[]
    propertyKeys: string[]
    propertyValues: number[][]
    propertyTypes: string[]

    constructor(
        moduleAddress: string,
        creator: string,
        collectionName: string,
        name: string,
        description: string,
        uri: string,
        quantity: number | bigint,
        maximum: number | bigint,
        royaltyPayeeAddress: string,
        royaltyPointsNumerator: number | bigint,
        royaltyPointsDenominator: number | bigint,
        mutateSetting: boolean[],
        propertyKeys: string[],
        propertyValues: number[][],
        propertyTypes: string[]
    ) {
        super(moduleAddress, "utils", "CreateTokenWithCreatedCollectionProof")
        this.creator = creator
        this.collectionName = collectionName
        this.name = name
        this.description = description
        this.uri = uri
        this.quantity = quantity
        this.maximum = maximum
        this.royaltyPayeeAddress = royaltyPayeeAddress
        this.royaltyPointsNumerator = royaltyPointsNumerator
        this.royaltyPointsDenominator = royaltyPointsDenominator
        this.mutateSetting = mutateSetting
        this.propertyKeys = propertyKeys
        this.propertyValues = propertyValues
        this.propertyTypes = propertyTypes
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.creator).serialize(serializer)
        serializer.serializeStr(this.collectionName)
        serializer.serializeStr(this.name)
        serializer.serializeStr(this.description)
        serializer.serializeStr(this.uri)
        serializer.serializeU64(this.quantity)
        serializer.serializeU64(this.maximum)
        TxnBuilderTypes.AccountAddress.fromHex(this.royaltyPayeeAddress).serialize(serializer)
        serializer.serializeU64(this.royaltyPointsNumerator)
        serializer.serializeU64(this.royaltyPointsDenominator)
        serializer.serializeU32AsUleb128(this.mutateSetting.length)
        this.mutateSetting.forEach((setting: boolean) => {
            serializer.serializeBool(setting)
        })
        serializer.serializeU32AsUleb128(this.propertyKeys.length)
        this.propertyKeys.forEach((key: string) => {
            serializer.serializeStr(key)
        })
        serializer.serializeU32AsUleb128(this.propertyValues.length)
        this.propertyValues.forEach((value: number[]) => {
            serializer.serializeU32AsUleb128(value.length)
            value.forEach((num: number) => {
                serializer.serializeU8(num)
            })
        })
        serializer.serializeU32AsUleb128(this.propertyTypes.length)
        this.propertyTypes.forEach((type: string) => {
            serializer.serializeStr(type)
        })
    }
}

export class CreateTokenWithoutCreatedCollectionProof extends Proof {
    creator: string
    collectionName: string
    collectionDescription: string
    collectionUri: string
    collectionMaximum: number | bigint
    collectionMutateSetting: boolean[]
    tokenName: string
    tokenDescription: string
    tokenUri: string
    tokenQuantity: number | bigint
    tokenMaximum: number | bigint
    royaltyPayeeAddress: string
    royaltyPointsNumerator: number | bigint
    royaltyPointsDenominator: number | bigint
    tokenMutateSetting: boolean[]
    propertyKeys: string[]
    propertyValues: number[][]
    propertyTypes: string[]

    constructor(
        moduleAddress: string,
        creator: string,
        collectionName: string,
        collectionDescription: string,
        collectionUri: string,
        collectionMaximum: number | bigint,
        collectionMutateSetting: boolean[],
        tokenName: string,
        tokenDescription: string,
        tokenUri: string,
        tokenQuantity: number | bigint,
        tokenMaximum: number | bigint,
        royaltyPayeeAddress: string,
        royaltyPointsNumerator: number | bigint,
        royaltyPointsDenominator: number | bigint,
        tokenMutateSetting: boolean[],
        propertyKeys: string[],
        propertyValues: number[][],
        propertyTypes: string[]
    ) {
        super(moduleAddress, "utils", "CreateTokenWithoutCreatedCollectionProof")
        this.creator = creator
        this.collectionName = collectionName
        this.collectionDescription = collectionDescription
        this.collectionUri = collectionUri
        this.collectionMaximum = collectionMaximum
        this.collectionMutateSetting = collectionMutateSetting
        this.tokenName = tokenName
        this.tokenDescription = tokenDescription
        this.tokenUri = tokenUri
        this.tokenQuantity = tokenQuantity
        this.tokenMaximum = tokenMaximum
        this.royaltyPayeeAddress = royaltyPayeeAddress
        this.royaltyPointsNumerator = royaltyPointsNumerator
        this.royaltyPointsDenominator = royaltyPointsDenominator
        this.tokenMutateSetting = tokenMutateSetting
        this.propertyKeys = propertyKeys
        this.propertyValues = propertyValues
        this.propertyTypes = propertyTypes
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.creator).serialize(serializer)
        serializer.serializeStr(this.collectionName)
        serializer.serializeStr(this.collectionDescription)
        serializer.serializeStr(this.collectionUri)
        serializer.serializeU64(this.collectionMaximum)
        serializer.serializeU32AsUleb128(this.collectionMutateSetting.length)
        this.collectionMutateSetting.forEach((setting: boolean) => {
            serializer.serializeBool(setting)
        })
        serializer.serializeStr(this.tokenName)
        serializer.serializeStr(this.tokenDescription)
        serializer.serializeStr(this.tokenUri)
        serializer.serializeU64(this.tokenQuantity)
        serializer.serializeU64(this.tokenMaximum)
        TxnBuilderTypes.AccountAddress.fromHex(this.royaltyPayeeAddress).serialize(serializer)
        serializer.serializeU64(this.royaltyPointsNumerator)
        serializer.serializeU64(this.royaltyPointsDenominator)
        serializer.serializeU32AsUleb128(this.tokenMutateSetting.length)
        this.tokenMutateSetting.forEach((setting: boolean) => {
            serializer.serializeBool(setting)
        })
        serializer.serializeU32AsUleb128(this.propertyKeys.length)
        this.propertyKeys.forEach((key: string) => {
            serializer.serializeStr(key)
        })
        serializer.serializeU32AsUleb128(this.propertyValues.length)
        this.propertyValues.forEach((value: number[]) => {
            serializer.serializeU32AsUleb128(value.length)
            value.forEach((num: number) => {
                serializer.serializeU8(num)
            })
        })
        serializer.serializeU32AsUleb128(this.propertyTypes.length)
        this.propertyTypes.forEach((type: string) => {
            serializer.serializeStr(type)
        })
    }
}

export class ListingTokenProof extends Proof {
    owner: string
    listingId: string
    creator: string
    collectionName: string
    tokenName: string
    propertyVersion: number | bigint
    quantity: number | bigint
    price: number | bigint | null
    duration: number | bigint

    constructor(
        moduleAddress: string,
        owner: string,
        listingId: string,
        creator: string,
        collectionName: string,
        tokenName: string,
        propertyVersion: number | bigint,
        quantity: number | bigint,
        price: number | bigint | null,
        duration: number | bigint
    ) {
        super(moduleAddress, "utils", price ? "ListingTokenFixedPriceProof" : "ListingTokenForBidProof")
        this.owner = owner
        this.listingId = listingId
        this.creator = creator
        this.collectionName = collectionName
        this.tokenName = tokenName
        this.propertyVersion = propertyVersion
        this.quantity = quantity
        this.price = price
        this.duration = duration
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.owner).serialize(serializer)
        serializer.serializeStr(this.listingId)
        TxnBuilderTypes.AccountAddress.fromHex(this.creator).serialize(serializer)
        serializer.serializeStr(this.collectionName)
        serializer.serializeStr(this.tokenName)
        serializer.serializeU64(this.propertyVersion)
        serializer.serializeU64(this.quantity)
        if (this.price) {
            serializer.serializeU64(this.price)
        }
        serializer.serializeU64(this.duration)
    }
}

export class BuyNowTokenProof extends Proof {
    buyer: string
    listingId: string

    constructor(moduleAddress: string, buyer: string, listingId: string) {
        super(moduleAddress, "utils", "BuyNowTokenProof")
        this.buyer = buyer
        this.listingId = listingId
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.buyer).serialize(serializer)
        serializer.serializeStr(this.listingId)
    }
}

export class EditPriceListingProof extends Proof {
    seller: string
    listingId: string
    newPrice: number | bigint

    constructor(moduleAddress: string, seller: string, listingId: string, newPrice: number | bigint) {
        super(moduleAddress, "utils", "EditPriceListingProof")
        this.seller = seller
        this.listingId = listingId
        this.newPrice = newPrice
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.seller).serialize(serializer)
        serializer.serializeStr(this.listingId)
        serializer.serializeU64(this.newPrice)
    }
}

export class CancelListingProof extends Proof {
    seller: string
    listingId: string

    constructor(moduleAddress: string, seller: string, listingId: string) {
        super(moduleAddress, "utils", "CancelListingProof")
        this.seller = seller
        this.listingId = listingId
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.seller).serialize(serializer)
        serializer.serializeStr(this.listingId)
    }
}

export class CancelAllListingProof extends Proof {
    seller: string

    constructor(moduleAddress: string, seller: string) {
        super(moduleAddress, "utils", "CancelAllListingProof")
        this.seller = seller
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.seller).serialize(serializer)
    }
}

export class OfferTokenProof extends Proof {
    buyer: string
    offerId: string
    creator: string
    collectionName: string
    tokenName: string
    propertyVersion: number | bigint
    quantity: number | bigint
    price: number | bigint
    duration: number | bigint

    constructor(
        moduleAddress: string,
        buyer: string,
        offerId: string,
        creator: string,
        collectionName: string,
        tokenName: string,
        propertyVersion: number | bigint,
        quantity: number | bigint,
        price: number | bigint,
        duration: number | bigint
    ) {
        super(moduleAddress, "utils", "OfferTokenProof")
        this.buyer = buyer
        this.offerId = offerId
        this.creator = creator
        this.collectionName = collectionName
        this.tokenName = tokenName
        this.propertyVersion = propertyVersion
        this.quantity = quantity
        this.price = price
        this.duration = duration
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.buyer).serialize(serializer)
        serializer.serializeStr(this.offerId)
        TxnBuilderTypes.AccountAddress.fromHex(this.creator).serialize(serializer)
        serializer.serializeStr(this.collectionName)
        serializer.serializeStr(this.tokenName)
        serializer.serializeU64(this.propertyVersion)
        serializer.serializeU64(this.quantity)
        serializer.serializeU64(this.price)
        serializer.serializeU64(this.duration)
    }
}

export class CancelOfferProof extends Proof {
    buyer: string
    offerId: string

    constructor(moduleAddress: string, buyer: string, offerId: string) {
        super(moduleAddress, "utils", "CancelOfferProof")
        this.buyer = buyer
        this.offerId = offerId
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.buyer).serialize(serializer)
        serializer.serializeStr(this.offerId)
    }
}

export class CancelAllOfferProof extends Proof {
    buyer: string

    constructor(moduleAddress: string, buyer: string) {
        super(moduleAddress, "utils", "CancelAllOfferProof")
        this.buyer = buyer
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.buyer).serialize(serializer)
    }
}

export class TransferWithCreatedTokenProof extends Proof {
    from: string
    creator: string
    collectionName: string
    tokenName: string
    tokenPropertyVersion: number | bigint
    to: string
    quantity: number | bigint

    constructor(
        moduleAddress: string,
        from: string,
        creator: string,
        collectionName: string,
        tokenName: string,
        tokenPropertyVersion: number | bigint,
        to: string,
        quantity: number | bigint
    ) {
        super(moduleAddress, "utils", "TransferWithCreatedTokenProof")
        this.from = from
        this.creator = creator
        this.collectionName = collectionName
        this.tokenName = tokenName
        this.tokenPropertyVersion = tokenPropertyVersion
        this.to = to
        this.quantity = quantity
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.from).serialize(serializer)
        TxnBuilderTypes.AccountAddress.fromHex(this.creator).serialize(serializer)
        serializer.serializeStr(this.collectionName)
        serializer.serializeStr(this.tokenName)
        serializer.serializeU64(this.tokenPropertyVersion)
        TxnBuilderTypes.AccountAddress.fromHex(this.to).serialize(serializer)
        serializer.serializeU64(this.quantity)
    }
}

export class TransferWithoutCreatedCollectionProof extends Proof {
    creator: string
    collectionName: string
    collectionDescription: string
    collectionUri: string
    collectionMaximum: number | bigint
    collectionMutateSetting: boolean[]
    tokenName: string
    tokenDescription: string
    tokenUri: string
    tokenQuantity: number | bigint
    tokenMaximum: number | bigint
    royaltyPayeeAddress: string
    royaltyPointsNumerator: number | bigint
    royaltyPointsDenominator: number | bigint
    tokenMutateSetting: boolean[]
    propertyKeys: string[]
    propertyValues: number[][]
    propertyTypes: string[]
    to: string

    constructor(
        moduleAddress: string,
        creator: string,
        collectionName: string,
        collectionDescription: string,
        collectionUri: string,
        collectionMaximum: number | bigint,
        collectionMutateSetting: boolean[],
        tokenName: string,
        tokenDescription: string,
        tokenUri: string,
        tokenQuantity: number | bigint,
        tokenMaximum: number | bigint,
        royaltyPayeeAddress: string,
        royaltyPointsNumerator: number | bigint,
        royaltyPointsDenominator: number | bigint,
        tokenMutateSetting: boolean[],
        propertyKeys: string[],
        propertyValues: number[][],
        propertyTypes: string[],
        to: string
    ) {
        super(moduleAddress, "utils", "TransferWithoutCreatedCollectionProof")
        this.creator = creator
        this.collectionName = collectionName
        this.collectionDescription = collectionDescription
        this.collectionUri = collectionUri
        this.collectionMaximum = collectionMaximum
        this.collectionMutateSetting = collectionMutateSetting
        this.tokenName = tokenName
        this.tokenDescription = tokenDescription
        this.tokenUri = tokenUri
        this.tokenQuantity = tokenQuantity
        this.tokenMaximum = tokenMaximum
        this.royaltyPayeeAddress = royaltyPayeeAddress
        this.royaltyPointsNumerator = royaltyPointsNumerator
        this.royaltyPointsDenominator = royaltyPointsDenominator
        this.tokenMutateSetting = tokenMutateSetting
        this.propertyKeys = propertyKeys
        this.propertyValues = propertyValues
        this.propertyTypes = propertyTypes
        this.to = to
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.creator).serialize(serializer)
        serializer.serializeStr(this.collectionName)
        serializer.serializeStr(this.collectionDescription)
        serializer.serializeStr(this.collectionUri)
        serializer.serializeU64(this.collectionMaximum)
        serializer.serializeU32AsUleb128(this.collectionMutateSetting.length)
        this.collectionMutateSetting.forEach((setting: boolean) => {
            serializer.serializeBool(setting)
        })
        serializer.serializeStr(this.tokenName)
        serializer.serializeStr(this.tokenDescription)
        serializer.serializeStr(this.tokenUri)
        serializer.serializeU64(this.tokenQuantity)
        serializer.serializeU64(this.tokenMaximum)
        TxnBuilderTypes.AccountAddress.fromHex(this.royaltyPayeeAddress).serialize(serializer)
        serializer.serializeU64(this.royaltyPointsNumerator)
        serializer.serializeU64(this.royaltyPointsDenominator)
        serializer.serializeU32AsUleb128(this.tokenMutateSetting.length)
        this.tokenMutateSetting.forEach((setting: boolean) => {
            serializer.serializeBool(setting)
        })
        serializer.serializeU32AsUleb128(this.propertyKeys.length)
        this.propertyKeys.forEach((key: string) => {
            serializer.serializeStr(key)
        })
        serializer.serializeU32AsUleb128(this.propertyValues.length)
        this.propertyValues.forEach((value: number[]) => {
            serializer.serializeU32AsUleb128(value.length)
            value.forEach((num: number) => {
                serializer.serializeU8(num)
            })
        })
        serializer.serializeU32AsUleb128(this.propertyTypes.length)
        this.propertyTypes.forEach((type: string) => {
            serializer.serializeStr(type)
        })
        TxnBuilderTypes.AccountAddress.fromHex(this.to).serialize(serializer)
    }
}

export class TransferWithCreatedCollectionProof extends Proof {
    creator: string
    collectionName: string
    tokenName: string
    tokenDescription: string
    tokenUri: string
    tokenQuantity: number | bigint
    tokenMaximum: number | bigint
    royaltyPayeeAddress: string
    royaltyPointsNumerator: number | bigint
    royaltyPointsDenominator: number | bigint
    tokenMutateSetting: boolean[]
    propertyKeys: string[]
    propertyValues: number[][]
    propertyTypes: string[]
    to: string

    constructor(
        moduleAddress: string,
        creator: string,
        collectionName: string,
        tokenName: string,
        tokenDescription: string,
        tokenUri: string,
        tokenQuantity: number | bigint,
        tokenMaximum: number | bigint,
        royaltyPayeeAddress: string,
        royaltyPointsNumerator: number | bigint,
        royaltyPointsDenominator: number | bigint,
        tokenMutateSetting: boolean[],
        propertyKeys: string[],
        propertyValues: number[][],
        propertyTypes: string[],
        to: string
    ) {
        super(moduleAddress, "utils", "TransferWithCreatedCollectionProof")
        this.creator = creator
        this.collectionName = collectionName
        this.tokenName = tokenName
        this.tokenDescription = tokenDescription
        this.tokenUri = tokenUri
        this.tokenQuantity = tokenQuantity
        this.tokenMaximum = tokenMaximum
        this.royaltyPayeeAddress = royaltyPayeeAddress
        this.royaltyPointsNumerator = royaltyPointsNumerator
        this.royaltyPointsDenominator = royaltyPointsDenominator
        this.tokenMutateSetting = tokenMutateSetting
        this.propertyKeys = propertyKeys
        this.propertyValues = propertyValues
        this.propertyTypes = propertyTypes
        this.to = to
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.creator).serialize(serializer)
        serializer.serializeStr(this.collectionName)
        serializer.serializeStr(this.tokenName)
        serializer.serializeStr(this.tokenDescription)
        serializer.serializeStr(this.tokenUri)
        serializer.serializeU64(this.tokenQuantity)
        serializer.serializeU64(this.tokenMaximum)
        TxnBuilderTypes.AccountAddress.fromHex(this.royaltyPayeeAddress).serialize(serializer)
        serializer.serializeU64(this.royaltyPointsNumerator)
        serializer.serializeU64(this.royaltyPointsDenominator)
        serializer.serializeU32AsUleb128(this.tokenMutateSetting.length)
        this.tokenMutateSetting.forEach((setting: boolean) => {
            serializer.serializeBool(setting)
        })
        serializer.serializeU32AsUleb128(this.propertyKeys.length)
        this.propertyKeys.forEach((key: string) => {
            serializer.serializeStr(key)
        })
        serializer.serializeU32AsUleb128(this.propertyValues.length)
        this.propertyValues.forEach((value: number[]) => {
            serializer.serializeU32AsUleb128(value.length)
            value.forEach((num: number) => {
                serializer.serializeU8(num)
            })
        })
        serializer.serializeU32AsUleb128(this.propertyTypes.length)
        this.propertyTypes.forEach((type: string) => {
            serializer.serializeStr(type)
        })
        TxnBuilderTypes.AccountAddress.fromHex(this.to).serialize(serializer)
    }
}

export class AcceptOffersWithCreatedTokenAndListingProof extends Proof {
    seller: string
    offerId: string
    listingIds: string[]

    constructor(moduleAddress: string, seller: string, offerId: string, listingIds: string[]) {
        super(moduleAddress, "utils", "AcceptOffersWithCreatedTokenAndListingProof")
        this.seller = seller
        this.offerId = offerId
        this.listingIds = listingIds
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.seller).serialize(serializer)
        serializer.serializeStr(this.offerId)
        serializer.serializeU32AsUleb128(this.listingIds.length)
        this.listingIds.forEach((type: string) => {
            serializer.serializeStr(type)
        })
    }
}

export class AcceptOffersWithoutCreatedCollectionAndListingProof extends Proof {
    seller: string
    collectionName: string
    collectionDescription: string
    collectionUri: string
    collectionMaximum: number | bigint
    collectionMutateSetting: boolean[]
    tokenName: string
    tokenDescription: string
    tokenUri: string
    tokenQuantity: number | bigint
    tokenMaximum: number | bigint
    royaltyPayeeAddress: string
    royaltyPointsNumerator: number | bigint
    royaltyPointsDenominator: number | bigint
    tokenMutateSetting: boolean[]
    propertyKeys: string[]
    propertyValues: number[][]
    propertyTypes: string[]
    offerId: string
    listingIds: string[]
    startTimes: (number | bigint)[]
    endTimes: (number | bigint)[]
    quantities: (number | bigint)[]
    prices: (number | bigint)[]

    constructor(
        moduleAddress: string,
        seller: string,
        collectionName: string,
        collectionDescription: string,
        collectionUri: string,
        collectionMaximum: number | bigint,
        collectionMutateSetting: boolean[],
        tokenName: string,
        tokenDescription: string,
        tokenUri: string,
        tokenQuantity: number | bigint,
        tokenMaximum: number | bigint,
        royaltyPayeeAddress: string,
        royaltyPointsNumerator: number | bigint,
        royaltyPointsDenominator: number | bigint,
        tokenMutateSetting: boolean[],
        propertyKeys: string[],
        propertyValues: number[][],
        propertyTypes: string[],
        offerId: string,
        listingIds: string[],
        startTimes: (number | bigint)[],
        endTimes: (number | bigint)[],
        quantities: (number | bigint)[],
        prices: (number | bigint)[]
    ) {
        super(moduleAddress, "utils", "AcceptOffersWithoutCreatedCollectionAndListingProof")
        this.seller = seller
        this.collectionName = collectionName
        this.collectionDescription = collectionDescription
        this.collectionUri = collectionUri
        this.collectionMaximum = collectionMaximum
        this.collectionMutateSetting = collectionMutateSetting
        this.tokenName = tokenName
        this.tokenDescription = tokenDescription
        this.tokenUri = tokenUri
        this.tokenQuantity = tokenQuantity
        this.tokenMaximum = tokenMaximum
        this.royaltyPayeeAddress = royaltyPayeeAddress
        this.royaltyPointsNumerator = royaltyPointsNumerator
        this.royaltyPointsDenominator = royaltyPointsDenominator
        this.tokenMutateSetting = tokenMutateSetting
        this.propertyKeys = propertyKeys
        this.propertyValues = propertyValues
        this.propertyTypes = propertyTypes
        this.offerId = offerId
        this.listingIds = listingIds
        this.startTimes = startTimes
        this.endTimes = endTimes
        this.quantities = quantities
        this.prices = prices
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.seller).serialize(serializer)
        serializer.serializeStr(this.collectionName)
        serializer.serializeStr(this.collectionDescription)
        serializer.serializeStr(this.collectionUri)
        serializer.serializeU64(this.collectionMaximum)
        serializer.serializeU32AsUleb128(this.collectionMutateSetting.length)
        this.collectionMutateSetting.forEach((setting: boolean) => {
            serializer.serializeBool(setting)
        })
        serializer.serializeStr(this.tokenName)
        serializer.serializeStr(this.tokenDescription)
        serializer.serializeStr(this.tokenUri)
        serializer.serializeU64(this.tokenQuantity)
        serializer.serializeU64(this.tokenMaximum)
        TxnBuilderTypes.AccountAddress.fromHex(this.royaltyPayeeAddress).serialize(serializer)
        serializer.serializeU64(this.royaltyPointsNumerator)
        serializer.serializeU64(this.royaltyPointsDenominator)
        serializer.serializeU32AsUleb128(this.tokenMutateSetting.length)
        this.tokenMutateSetting.forEach((setting: boolean) => {
            serializer.serializeBool(setting)
        })
        serializer.serializeU32AsUleb128(this.propertyKeys.length)
        this.propertyKeys.forEach((key: string) => {
            serializer.serializeStr(key)
        })
        serializer.serializeU32AsUleb128(this.propertyValues.length)
        this.propertyValues.forEach((value: number[]) => {
            serializer.serializeU32AsUleb128(value.length)
            value.forEach((num: number) => {
                serializer.serializeU8(num)
            })
        })
        serializer.serializeU32AsUleb128(this.propertyTypes.length)
        this.propertyTypes.forEach((type: string) => {
            serializer.serializeStr(type)
        })
        serializer.serializeStr(this.offerId)
        serializer.serializeU32AsUleb128(this.listingIds.length)
        this.listingIds.forEach((listingId: string) => {
            serializer.serializeStr(listingId)
        })
        serializer.serializeU32AsUleb128(this.startTimes.length)
        this.startTimes.forEach((startTime: number | bigint) => {
            serializer.serializeU64(startTime)
        })
        serializer.serializeU32AsUleb128(this.endTimes.length)
        this.endTimes.forEach((endTime: number | bigint) => {
            serializer.serializeU64(endTime)
        })
        serializer.serializeU32AsUleb128(this.quantities.length)
        this.quantities.forEach((quantity: number | bigint) => {
            serializer.serializeU64(quantity)
        })
        serializer.serializeU32AsUleb128(this.prices.length)
        this.prices.forEach((price: number | bigint) => {
            serializer.serializeU64(price)
        })
    }
}

export class AcceptOffersWithoutCreatedTokenAndListingProof extends Proof {
    creator: string
    collectionName: string
    name: string
    description: string
    uri: string
    quantity: number | bigint
    maximum: number | bigint
    royaltyPayeeAddress: string
    royaltyPointsNumerator: number | bigint
    royaltyPointsDenominator: number | bigint
    mutateSetting: boolean[]
    propertyKeys: string[]
    propertyValues: number[][]
    propertyTypes: string[]
    offerId: string
    listingIds: string[]
    startTimes: (number | bigint)[]
    endTimes: (number | bigint)[]
    quantities: (number | bigint)[]
    prices: (number | bigint)[]

    constructor(
        moduleAddress: string,
        creator: string,
        collectionName: string,
        name: string,
        description: string,
        uri: string,
        quantity: number | bigint,
        maximum: number | bigint,
        royaltyPayeeAddress: string,
        royaltyPointsNumerator: number | bigint,
        royaltyPointsDenominator: number | bigint,
        mutateSetting: boolean[],
        propertyKeys: string[],
        propertyValues: number[][],
        propertyTypes: string[],
        offerId: string,
        listingIds: string[],
        startTimes: (number | bigint)[],
        endTimes: (number | bigint)[],
        quantities: (number | bigint)[],
        prices: (number | bigint)[]
    ) {
        super(moduleAddress, "utils", "AcceptOffersWithoutCreatedTokenAndListingProof")
        this.creator = creator
        this.collectionName = collectionName
        this.name = name
        this.description = description
        this.uri = uri
        this.quantity = quantity
        this.maximum = maximum
        this.royaltyPayeeAddress = royaltyPayeeAddress
        this.royaltyPointsNumerator = royaltyPointsNumerator
        this.royaltyPointsDenominator = royaltyPointsDenominator
        this.mutateSetting = mutateSetting
        this.propertyKeys = propertyKeys
        this.propertyValues = propertyValues
        this.propertyTypes = propertyTypes
        this.offerId = offerId
        this.listingIds = listingIds
        this.startTimes = startTimes
        this.endTimes = endTimes
        this.quantities = quantities
        this.prices = prices
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.creator).serialize(serializer)
        serializer.serializeStr(this.collectionName)
        serializer.serializeStr(this.name)
        serializer.serializeStr(this.description)
        serializer.serializeStr(this.uri)
        serializer.serializeU64(this.quantity)
        serializer.serializeU64(this.maximum)
        TxnBuilderTypes.AccountAddress.fromHex(this.royaltyPayeeAddress).serialize(serializer)
        serializer.serializeU64(this.royaltyPointsNumerator)
        serializer.serializeU64(this.royaltyPointsDenominator)
        serializer.serializeU32AsUleb128(this.mutateSetting.length)
        this.mutateSetting.forEach((setting: boolean) => {
            serializer.serializeBool(setting)
        })
        serializer.serializeU32AsUleb128(this.propertyKeys.length)
        this.propertyKeys.forEach((key: string) => {
            serializer.serializeStr(key)
        })
        serializer.serializeU32AsUleb128(this.propertyValues.length)
        this.propertyValues.forEach((value: number[]) => {
            serializer.serializeU32AsUleb128(value.length)
            value.forEach((num: number) => {
                serializer.serializeU8(num)
            })
        })
        serializer.serializeU32AsUleb128(this.propertyTypes.length)
        this.propertyTypes.forEach((type: string) => {
            serializer.serializeStr(type)
        })
        serializer.serializeStr(this.offerId)
        serializer.serializeU32AsUleb128(this.listingIds.length)
        this.listingIds.forEach((listingId: string) => {
            serializer.serializeStr(listingId)
        })
        serializer.serializeU32AsUleb128(this.startTimes.length)
        this.startTimes.forEach((startTime: number | bigint) => {
            serializer.serializeU64(startTime)
        })
        serializer.serializeU32AsUleb128(this.endTimes.length)
        this.endTimes.forEach((endTime: number | bigint) => {
            serializer.serializeU64(endTime)
        })
        serializer.serializeU32AsUleb128(this.quantities.length)
        this.quantities.forEach((quantity: number | bigint) => {
            serializer.serializeU64(quantity)
        })
        serializer.serializeU32AsUleb128(this.prices.length)
        this.prices.forEach((price: number | bigint) => {
            serializer.serializeU64(price)
        })
    }
}

export class AcceptOffersWithCreatedTokenAndWithoutListingProof extends Proof {
    creator: string
    collectionName: string
    name: string
    propertyVersion: number | bigint
    offerId: string
    listingIds: string[]
    startTimes: (number | bigint)[]
    endTimes: (number | bigint)[]
    quantities: (number | bigint)[]
    prices: (number | bigint)[]

    constructor(
        moduleAddress: string,
        creator: string,
        collectionName: string,
        name: string,
        propertyVersion: number | bigint,
        offerId: string,
        listingIds: string[],
        startTimes: (number | bigint)[],
        endTimes: (number | bigint)[],
        quantities: (number | bigint)[],
        prices: (number | bigint)[]
    ) {
        super(moduleAddress, "utils", "AcceptOffersWithCreatedTokenAndWithoutListingProof")
        this.creator = creator
        this.collectionName = collectionName
        this.name = name
        this.propertyVersion = propertyVersion
        this.offerId = offerId
        this.listingIds = listingIds
        this.startTimes = startTimes
        this.endTimes = endTimes
        this.quantities = quantities
        this.prices = prices
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.creator).serialize(serializer)
        serializer.serializeStr(this.collectionName)
        serializer.serializeStr(this.name)
        serializer.serializeU64(this.propertyVersion)
        serializer.serializeStr(this.offerId)
        serializer.serializeU32AsUleb128(this.listingIds.length)
        this.listingIds.forEach((listingId: string) => {
            serializer.serializeStr(listingId)
        })
        serializer.serializeU32AsUleb128(this.startTimes.length)
        this.startTimes.forEach((startTime: number | bigint) => {
            serializer.serializeU64(startTime)
        })
        serializer.serializeU32AsUleb128(this.endTimes.length)
        this.endTimes.forEach((endTime: number | bigint) => {
            serializer.serializeU64(endTime)
        })
        serializer.serializeU32AsUleb128(this.quantities.length)
        this.quantities.forEach((quantity: number | bigint) => {
            serializer.serializeU64(quantity)
        })
        serializer.serializeU32AsUleb128(this.prices.length)
        this.prices.forEach((price: number | bigint) => {
            serializer.serializeU64(price)
        })
    }
}

export class ClaimListingProof extends Proof {
    seller: string
    listingId: string

    constructor(moduleAddress: string, seller: string, listingId: string) {
        super(moduleAddress, "utils", "ClaimListingProof")
        this.seller = seller
        this.listingId = listingId
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.seller).serialize(serializer)
        serializer.serializeStr(this.listingId)
    }
}

export class ClaimAllListingProof extends Proof {
    seller: string

    constructor(moduleAddress: string, seller: string) {
        super(moduleAddress, "utils", "ClaimAllListingProof")
        this.seller = seller
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.seller).serialize(serializer)
    }
}

export class ClaimOfferingProof extends Proof {
    buyer: string
    offerId: string

    constructor(moduleAddress: string, buyer: string, offerId: string) {
        super(moduleAddress, "utils", "ClaimOfferingProof")
        this.buyer = buyer
        this.offerId = offerId
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.buyer).serialize(serializer)
        serializer.serializeStr(this.offerId)
    }
}

export class ClaimAllOfferingProof extends Proof {
    buyer: string

    constructor(moduleAddress: string, buyer: string) {
        super(moduleAddress, "utils", "ClaimAllOfferingProof")
        this.buyer = buyer
    }

    serialize(serializer: BCS.Serializer) {
        TxnBuilderTypes.AccountAddress.fromHex(this.moduleAddress).serialize(serializer)
        serializer.serializeStr(this.moduleName)
        serializer.serializeStr(this.structName)
        TxnBuilderTypes.AccountAddress.fromHex(this.buyer).serialize(serializer)
    }
}
