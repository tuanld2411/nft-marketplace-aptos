## Install Aptos

-   Following [this link](https://aptos.dev/cli-tools/build-aptos-cli) to install Aptos
-   Following [this link](https://aptos.dev/cli-tools/install-move-prover) to install Move prove
-   For the convenience of coding, install [this extension ](https://marketplace.visualstudio.com/items?itemName=damirka.move-syntax)and run the following plugin in VS code:

    ```
    cargo install --path language/move-analyzer
    ```

-   If successful, the result of running the command `aptos --version` will be:

    ```
    aptos 1.0.1
    Aptos Labs <opensource@aptoslabs.com>
    Command Line Interface (CLI) for developing and interacting with the Aptos blockchain
    ```

## Setup environment

-   Following this command:

    ```
    https://gitlab.pandoinfinity.com/nft-marketplace/blockchain/marketplace-contract
    cd marketplace-contract
    npm install
    aptos init
    aptos init --profile payee
    aptos init --profile user
    aptos init --profile verifier
    ```

-   The above aptos init command lines will create a folder called .aptos, with the file config.yaml in it. The config.yaml file includes the address, private key information of default, user, payee, verifier
-   Run the following command to get the address of default and user respectively:
    ```
    aptos account lookup-address
    aptos account lookup-address --profile user
    ```
-   Transfer the APT token to the above 2 addresses

## Publish

-   Following this command, then type `yes` continue:
    ```
    aptos move create-resource-account-and-publish-package --seed 0x123456789 --address-name marketplace --named-addresses admin_addr=default
    ```

## Run unit test

-   Replace the resource address in the output screen on line 4, file `test/marketplace_exchange.ts`
-   Run command: `npm run test` and look testing result
